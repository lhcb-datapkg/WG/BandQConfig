# This is the CMakeCache file.
# For build in directory: /afs/cern.ch/user/i/ibelyaev/cmtuser/RELEASE/BandQConfig
# It was generated by CMake: /cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.11.0/Linux-x86_64/bin/cmake
# You can edit this file to change values found and used by cmake.
# If you do not want to change any of the values, simply exit the editor.
# If you do want to change a value, simply edit, save, and exit the editor.
# The syntax for the file is as follows:
# KEY:TYPE=VALUE
# KEY is the name of a variable in the cache.
# TYPE is a hint to GUIs for the type of VALUE, DO NOT EDIT TYPE!.
# VALUE is the current value for the KEY.

########################
# EXTERNAL cache entries
########################

//Set to OFF to build static libraries.
BUILD_SHARED_LIBS:BOOL=ON

//Path to a program.
CMAKE_AR:FILEPATH=/usr/bin/ar

//Choose the type of build, options are: Release, Debug, Coverage,
// Profile, RelWithDebInfo, MinSizeRel.
CMAKE_BUILD_TYPE:STRING=Release

//Enable/Disable color output during build.
CMAKE_COLOR_MAKEFILE:BOOL=ON

//Single build output directory for all generated config files
CMAKE_CONFIG_OUTPUT_DIRECTORY:STRING=/afs/cern.ch/user/i/ibelyaev/cmtuser/RELEASE/BandQConfig/config

//CXX compiler
CMAKE_CXX_COMPILER:FILEPATH=/usr/bin/c++

//A wrapper around 'ar' adding the appropriate '--plugin' option
// for the GCC compiler
CMAKE_CXX_COMPILER_AR:FILEPATH=CMAKE_CXX_COMPILER_AR-NOTFOUND

//A wrapper around 'ranlib' adding the appropriate '--plugin' option
// for the GCC compiler
CMAKE_CXX_COMPILER_RANLIB:FILEPATH=CMAKE_CXX_COMPILER_RANLIB-NOTFOUND

//Flags used by the compiler during all build types.
CMAKE_CXX_FLAGS:STRING= -msse4.2 -fmessage-length=0 -pipe -Wall -Wextra -Werror=return-type -pthread -pedantic -Wwrite-strings -Wpointer-arith -Woverloaded-virtual -Wno-long-long -Wsuggest-override

//Flags used by the compiler during coverage builds.
CMAKE_CXX_FLAGS_COVERAGE:STRING=--coverage

//Flags used by the compiler during Debug builds.
CMAKE_CXX_FLAGS_DEBUG:STRING=-Og -g

//Flags used by the CXX compiler during MINSIZEREL builds.
CMAKE_CXX_FLAGS_MINSIZEREL:STRING=-Os -DNDEBUG

//Flags used by the compiler during profile builds.
CMAKE_CXX_FLAGS_PROFILE:STRING=-pg

//Flags used by the compiler during release builds.
CMAKE_CXX_FLAGS_RELEASE:STRING=-O3 -DNDEBUG

//Flags used by the compiler during Release with Debug Info builds.
CMAKE_CXX_FLAGS_RELWITHDEBINFO:STRING=-O3 -DNDEBUG -g

//C compiler
CMAKE_C_COMPILER:FILEPATH=/usr/bin/cc

//A wrapper around 'ar' adding the appropriate '--plugin' option
// for the GCC compiler
CMAKE_C_COMPILER_AR:FILEPATH=CMAKE_C_COMPILER_AR-NOTFOUND

//A wrapper around 'ranlib' adding the appropriate '--plugin' option
// for the GCC compiler
CMAKE_C_COMPILER_RANLIB:FILEPATH=CMAKE_C_COMPILER_RANLIB-NOTFOUND

//Flags used by the compiler during all build types.
CMAKE_C_FLAGS:STRING= -msse4.2 -fmessage-length=0 -pipe -Wall -Wextra -Werror=return-type -pthread -pedantic -Wwrite-strings -Wpointer-arith -Wno-long-long

//Flags used by the compiler during coverage builds.
CMAKE_C_FLAGS_COVERAGE:STRING=--coverage

//Flags used by the compiler during Debug builds.
CMAKE_C_FLAGS_DEBUG:STRING=-Og -g

//Flags used by the C compiler during MINSIZEREL builds.
CMAKE_C_FLAGS_MINSIZEREL:STRING=-Os -DNDEBUG

//Flags used by the compiler during profile builds.
CMAKE_C_FLAGS_PROFILE:STRING=-pg

//Flags used by the compiler during release builds.
CMAKE_C_FLAGS_RELEASE:STRING=-O3 -DNDEBUG

//Flags used by the compiler during Release with Debug Info builds.
CMAKE_C_FLAGS_RELWITHDEBINFO:STRING=-O3 -DNDEBUG -g

//Flags used by the linker during the creation of executables.
CMAKE_EXE_LINKER_FLAGS:STRING=-Wl,--as-needed -Wl,--no-undefined  -Wl,-z,max-page-size=0x1000

//Flags used by the linker during DEBUG builds.
CMAKE_EXE_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during MINSIZEREL builds.
CMAKE_EXE_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during RELEASE builds.
CMAKE_EXE_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during RELWITHDEBINFO builds.
CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//Generate compile_commands.json file
CMAKE_EXPORT_COMPILE_COMMANDS:BOOL=OFF

//Flags used by the compiler during all build types.
CMAKE_Fortran_FLAGS:STRING= -msse4.2 -fmessage-length=0 -pipe -Wall -Wextra -Werror=return-type -pthread -pedantic -fsecond-underscore

//Flags used by the compiler during coverage builds.
CMAKE_Fortran_FLAGS_COVERAGE:STRING=--coverage

//Flags used by the compiler during Debug builds.
CMAKE_Fortran_FLAGS_DEBUG:STRING=-Og -g

//Flags used by the compiler during profile builds.
CMAKE_Fortran_FLAGS_PROFILE:STRING=-pg

//Flags used by the compiler during release builds.
CMAKE_Fortran_FLAGS_RELEASE:STRING=-O3 -DNDEBUG

//Flags used by the compiler during Release with Debug Info builds.
CMAKE_Fortran_FLAGS_RELWITHDEBINFO:STRING=-O3 -DNDEBUG -g

//Install path prefix, prepended onto install directories.
CMAKE_INSTALL_PREFIX:PATH=/afs/cern.ch/user/i/ibelyaev/cmtuser/RELEASE/BandQConfig/InstallArea

//Single build output directory for all libraries
CMAKE_LIBRARY_OUTPUT_DIRECTORY:STRING=/afs/cern.ch/user/i/ibelyaev/cmtuser/RELEASE/BandQConfig/lib

//Path to a program.
CMAKE_LINKER:FILEPATH=/usr/bin/ld

//Path to a program.
CMAKE_MAKE_PROGRAM:FILEPATH=/usr/bin/gmake

//Flags used by the linker during the creation of modules.
CMAKE_MODULE_LINKER_FLAGS:STRING=-Wl,--as-needed -Wl,--no-undefined  -Wl,-z,max-page-size=0x1000

//Flags used by the linker during the creation of modules during
// DEBUG builds.
CMAKE_MODULE_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during the creation of modules during
// MINSIZEREL builds.
CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during the creation of modules during
// RELEASE builds.
CMAKE_MODULE_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during the creation of modules during
// RELWITHDEBINFO builds.
CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//Path to a program.
CMAKE_NM:FILEPATH=/usr/bin/nm

//Path to a program.
CMAKE_OBJCOPY:FILEPATH=/usr/bin/objcopy

//Path to a program.
CMAKE_OBJDUMP:FILEPATH=/usr/bin/objdump

//Value Computed by CMake
CMAKE_PROJECT_NAME:STATIC=WG/BandQConfig

//Version of the project
CMAKE_PROJECT_VERSION:STRING=v15r2

//Path to a program.
CMAKE_RANLIB:FILEPATH=/usr/bin/ranlib

//Single build output directory for all executables
CMAKE_RUNTIME_OUTPUT_DIRECTORY:STRING=/afs/cern.ch/user/i/ibelyaev/cmtuser/RELEASE/BandQConfig/bin

//Flags used by the linker during the creation of dll's.
CMAKE_SHARED_LINKER_FLAGS:STRING=-Wl,--as-needed -Wl,--no-undefined  -Wl,-z,max-page-size=0x1000

//Flags used by the linker during the creation of shared libraries
// during DEBUG builds.
CMAKE_SHARED_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during the creation of shared libraries
// during MINSIZEREL builds.
CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during the creation of shared libraries
// during RELEASE builds.
CMAKE_SHARED_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during the creation of shared libraries
// during RELWITHDEBINFO builds.
CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//If set, runtime paths are not added when installing shared libraries,
// but are added when building.
CMAKE_SKIP_INSTALL_RPATH:BOOL=NO

//If set, runtime paths are not added when using shared libraries.
CMAKE_SKIP_RPATH:BOOL=NO

//Flags used by the linker during the creation of static libraries
// during all build types.
CMAKE_STATIC_LINKER_FLAGS:STRING=

//Flags used by the linker during the creation of static libraries
// during DEBUG builds.
CMAKE_STATIC_LINKER_FLAGS_DEBUG:STRING=

//Flags used by the linker during the creation of static libraries
// during MINSIZEREL builds.
CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL:STRING=

//Flags used by the linker during the creation of static libraries
// during RELEASE builds.
CMAKE_STATIC_LINKER_FLAGS_RELEASE:STRING=

//Flags used by the linker during the creation of static libraries
// during RELWITHDEBINFO builds.
CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO:STRING=

//Path to a program.
CMAKE_STRIP:FILEPATH=/usr/bin/strip

//Use ccache to speed up compilation.
CMAKE_USE_CCACHE:BOOL=OFF

//If this value is on, makefiles will be generated without the
// .SILENT directive, and all commands will be echoed to the console
// during the make.  This is useful for debugging only. With Visual
// Studio IDE projects all commands are done without /nologo.
CMAKE_VERBOSE_MAKEFILE:BOOL=FALSE

//enable explicit symbol visibility on gcc-4
G21_HIDE_SYMBOLS:BOOL=OFF

//disable backward-compatibility hacks in IInterface and InterfaceID
G21_NEW_INTERFACES:BOOL=OFF

//remove deprecated methods and functions
G21_NO_DEPRECATED:BOOL=OFF

//use (only) the new interface of the ServiceLocator
G22_NEW_SVCLOCATOR:BOOL=OFF

//Which architecture-specific optimizations to use
GAUDI_ARCH:STRING=sse4.2

//Set to OFF to disable the build of the tests (libraries and executables).
GAUDI_BUILD_TESTS:BOOL=ON

//style to use for clang-format (apply-formatting command and target)
GAUDI_CLANG_STYLE:STRING=google

//Version of the C++ standard to be used.
GAUDI_CXX_STANDARD:STRING=c++14

//List of (suffix) directories where to look for data packages.
GAUDI_DATA_SUFFIXES:STRING=DBASE;PARAM;EXTRAPACKAGES

//When CMAKE_BUILD_TYPE is RelWithDebInfo, save the debug information
// on a different file.
GAUDI_DETACHED_DEBINFO:BOOL=ON

//enable colors in compiler diagnostics
GAUDI_DIAGNOSTICS_COLOR:BOOL=OFF

//Turn on or off options that are used to hide warning messages.
GAUDI_HIDE_WARNINGS:BOOL=ON

//turn off all optimizations in debug builds
GAUDI_SLOW_DEBUG:BOOL=OFF

//Require that the version of used projects is exactly the what
// specified
GAUDI_STRICT_VERSION_CHECK:BOOL=OFF

//enable warnings for missing override keyword
GAUDI_SUGGEST_OVERRIDE:BOOL=ON

//Use CTest launchers to record details about warnings and errors.
GAUDI_USE_CTEST_LAUNCHERS:BOOL=OFF

//Add the .exe suffix to executables on Unix systems (like CMT
// does).
GAUDI_USE_EXE_SUFFIX:BOOL=ON

//disable backward compatibility hacks (implies all G21_* options)
GAUDI_V21:BOOL=OFF

//enable some API extensions
GAUDI_V22:BOOL=OFF

//The directory containing a CMake configuration file for GaudiProject.
GaudiProject_DIR:PATH=/cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/LBSCRIPTS_v9r2p6/LbUtils/cmake

//BINARY_TAG of the host
HOST_BINARY_TAG:STRING=x86_64-slc6-gcc44-opt

//Path to a program.
PYTHON_EXECUTABLE:FILEPATH=/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_88/Python/2.7.13/x86_64-slc6-gcc62-opt/bin/python

//Value Computed by CMake
Project_BINARY_DIR:STATIC=/afs/cern.ch/user/i/ibelyaev/cmtuser/RELEASE/BandQConfig

//Value Computed by CMake
Project_SOURCE_DIR:STATIC=/afs/cern.ch/user/i/ibelyaev/cmtuser/RELEASE/BandQConfig

//Path to a library.
ROOT_Core_LIBRARY:FILEPATH=ROOT_Core_LIBRARY-NOTFOUND

//Path to a file.
ROOT_INCLUDE_DIR:PATH=ROOT_INCLUDE_DIR-NOTFOUND

//Path to a program.
ROOT_genmap_CMD:FILEPATH=ROOT_genmap_CMD-NOTFOUND

//Path to a program.
ROOT_genreflex_CMD:FILEPATH=ROOT_genreflex_CMD-NOTFOUND

//Path to a program.
ROOT_root_CMD:FILEPATH=ROOT_root_CMD-NOTFOUND

//Path to a program.
ROOT_rootcint_CMD:FILEPATH=ROOT_rootcint_CMD-NOTFOUND

//Value Computed by CMake
WG/BandQConfig_BINARY_DIR:STATIC=/afs/cern.ch/user/i/ibelyaev/cmtuser/RELEASE/BandQConfig

//Value Computed by CMake
WG/BandQConfig_SOURCE_DIR:STATIC=/afs/cern.ch/user/i/ibelyaev/cmtuser/RELEASE/BandQConfig

//Path to a program.
autopep8_cmd:FILEPATH=autopep8_cmd-NOTFOUND

//Path to a program.
ccache_cmd:FILEPATH=/usr/bin/ccache-swig

//Path to a program.
clang_format_cmd:FILEPATH=/cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/LBSCRIPTS_v9r2p6/InstallArea/scripts/lcg-clang-format-3.9

//Path to a program.
default_merge_cmd:FILEPATH=/cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/LBSCRIPTS_v9r2p6/LbUtils/cmake/quick-merge

//Path to a program.
distcc_cmd:FILEPATH=distcc_cmd-NOTFOUND

//Path to a program.
env_cmd:FILEPATH=/cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/LBSCRIPTS_v9r2p6/LbUtils/cmake/xenv

//path to the XML file for the environment to be used once the
// project is installed
env_release_xml:STRING=/afs/cern.ch/user/i/ibelyaev/cmtuser/RELEASE/BandQConfig/config/WG/BandQConfig.xenv

//path to the XML file for the environment to be used in building
// and testing
env_xml:STRING=/afs/cern.ch/user/i/ibelyaev/cmtuser/RELEASE/BandQConfig/config/WG/BandQConfig-build.xenv

//Path to a program.
gaudirun_cmd:FILEPATH=gaudirun_cmd-NOTFOUND

//Path to a program.
genconfuser_cmd:FILEPATH=genconfuser_cmd-NOTFOUND

//Path to a program.
qmtest_metadata_cmd:FILEPATH=/cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/LBSCRIPTS_v9r2p6/LbUtils/cmake/extract_qmtest_metadata.py

//The directory containing a CMake configuration file for vera++.
vera++_DIR:PATH=vera++_DIR-NOTFOUND

//Path to a program.
versheader_cmd:FILEPATH=versheader_cmd-NOTFOUND

//Path to a program.
zippythondir_cmd:FILEPATH=zippythondir_cmd-NOTFOUND


########################
# INTERNAL cache entries
########################

//ADVANCED property for variable: CMAKE_AR
CMAKE_AR-ADVANCED:INTERNAL=1
//This is the directory where this CMakeCache.txt was created
CMAKE_CACHEFILE_DIR:INTERNAL=/afs/cern.ch/user/i/ibelyaev/cmtuser/RELEASE/BandQConfig
//Major version of cmake used to create the current loaded cache
CMAKE_CACHE_MAJOR_VERSION:INTERNAL=3
//Minor version of cmake used to create the current loaded cache
CMAKE_CACHE_MINOR_VERSION:INTERNAL=11
//Patch version of cmake used to create the current loaded cache
CMAKE_CACHE_PATCH_VERSION:INTERNAL=0
//ADVANCED property for variable: CMAKE_COLOR_MAKEFILE
CMAKE_COLOR_MAKEFILE-ADVANCED:INTERNAL=1
//Path to CMake executable.
CMAKE_COMMAND:INTERNAL=/cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.11.0/Linux-x86_64/bin/cmake
//Path to cpack program executable.
CMAKE_CPACK_COMMAND:INTERNAL=/cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.11.0/Linux-x86_64/bin/cpack
//Path to ctest program executable.
CMAKE_CTEST_COMMAND:INTERNAL=/cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.11.0/Linux-x86_64/bin/ctest
//ADVANCED property for variable: CMAKE_CXX_COMPILER
CMAKE_CXX_COMPILER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_COMPILER_AR
CMAKE_CXX_COMPILER_AR-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_COMPILER_RANLIB
CMAKE_CXX_COMPILER_RANLIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS
CMAKE_CXX_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_COVERAGE
CMAKE_CXX_FLAGS_COVERAGE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_DEBUG
CMAKE_CXX_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_MINSIZEREL
CMAKE_CXX_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_PROFILE
CMAKE_CXX_FLAGS_PROFILE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_RELEASE
CMAKE_CXX_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_CXX_FLAGS_RELWITHDEBINFO
CMAKE_CXX_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_COMPILER
CMAKE_C_COMPILER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_COMPILER_AR
CMAKE_C_COMPILER_AR-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_COMPILER_RANLIB
CMAKE_C_COMPILER_RANLIB-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS
CMAKE_C_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_COVERAGE
CMAKE_C_FLAGS_COVERAGE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_DEBUG
CMAKE_C_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_MINSIZEREL
CMAKE_C_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_PROFILE
CMAKE_C_FLAGS_PROFILE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_RELEASE
CMAKE_C_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_C_FLAGS_RELWITHDEBINFO
CMAKE_C_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//Path to cache edit program executable.
CMAKE_EDIT_COMMAND:INTERNAL=/cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.11.0/Linux-x86_64/bin/ccmake
//Executable file format
CMAKE_EXECUTABLE_FORMAT:INTERNAL=ELF
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS
CMAKE_EXE_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_DEBUG
CMAKE_EXE_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_MINSIZEREL
CMAKE_EXE_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_RELEASE
CMAKE_EXE_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_EXPORT_COMPILE_COMMANDS
CMAKE_EXPORT_COMPILE_COMMANDS-ADVANCED:INTERNAL=1
//Name of external makefile project generator.
CMAKE_EXTRA_GENERATOR:INTERNAL=
//ADVANCED property for variable: CMAKE_Fortran_FLAGS
CMAKE_Fortran_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_Fortran_FLAGS_COVERAGE
CMAKE_Fortran_FLAGS_COVERAGE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_Fortran_FLAGS_PROFILE
CMAKE_Fortran_FLAGS_PROFILE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_Fortran_FLAGS_RELEASE
CMAKE_Fortran_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_Fortran_FLAGS_RELWITHDEBINFO
CMAKE_Fortran_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//Name of generator.
CMAKE_GENERATOR:INTERNAL=Unix Makefiles
//Generator instance identifier.
CMAKE_GENERATOR_INSTANCE:INTERNAL=
//Name of generator platform.
CMAKE_GENERATOR_PLATFORM:INTERNAL=
//Name of generator toolset.
CMAKE_GENERATOR_TOOLSET:INTERNAL=
//Source directory with the top level CMakeLists.txt file for this
// project
CMAKE_HOME_DIRECTORY:INTERNAL=/afs/cern.ch/user/i/ibelyaev/cmtuser/RELEASE/BandQConfig
//Install .so files without execute permission.
CMAKE_INSTALL_SO_NO_EXE:INTERNAL=0
//ADVANCED property for variable: CMAKE_LIBRARY_OUTPUT_DIRECTORY
CMAKE_LIBRARY_OUTPUT_DIRECTORY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_LINKER
CMAKE_LINKER-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MAKE_PROGRAM
CMAKE_MAKE_PROGRAM-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS
CMAKE_MODULE_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_DEBUG
CMAKE_MODULE_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL
CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_RELEASE
CMAKE_MODULE_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_NM
CMAKE_NM-ADVANCED:INTERNAL=1
//number of local generators
CMAKE_NUMBER_OF_MAKEFILES:INTERNAL=1
//ADVANCED property for variable: CMAKE_OBJCOPY
CMAKE_OBJCOPY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_OBJDUMP
CMAKE_OBJDUMP-ADVANCED:INTERNAL=1
//Platform information initialized
CMAKE_PLATFORM_INFO_INITIALIZED:INTERNAL=1
//Major version of project
CMAKE_PROJECT_VERSION_MAJOR:INTERNAL=15
//Minor version of project
CMAKE_PROJECT_VERSION_MINOR:INTERNAL=2
//Patch version of project
CMAKE_PROJECT_VERSION_PATCH:INTERNAL=
//Tweak version of project
CMAKE_PROJECT_VERSION_TWEAK:INTERNAL=
//ADVANCED property for variable: CMAKE_RANLIB
CMAKE_RANLIB-ADVANCED:INTERNAL=1
//Path to CMake installation.
CMAKE_ROOT:INTERNAL=/cvmfs/lhcb.cern.ch/lib/contrib/CMake/3.11.0/Linux-x86_64/share/cmake-3.11
//ADVANCED property for variable: CMAKE_RUNTIME_OUTPUT_DIRECTORY
CMAKE_RUNTIME_OUTPUT_DIRECTORY-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS
CMAKE_SHARED_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_DEBUG
CMAKE_SHARED_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL
CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_RELEASE
CMAKE_SHARED_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SKIP_INSTALL_RPATH
CMAKE_SKIP_INSTALL_RPATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_SKIP_RPATH
CMAKE_SKIP_RPATH-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS
CMAKE_STATIC_LINKER_FLAGS-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_DEBUG
CMAKE_STATIC_LINKER_FLAGS_DEBUG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL
CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_RELEASE
CMAKE_STATIC_LINKER_FLAGS_RELEASE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO
CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO-ADVANCED:INTERNAL=1
//ADVANCED property for variable: CMAKE_STRIP
CMAKE_STRIP-ADVANCED:INTERNAL=1
//uname command
CMAKE_UNAME:INTERNAL=/bin/uname
//ADVANCED property for variable: CMAKE_VERBOSE_MAKEFILE
CMAKE_VERBOSE_MAKEFILE-ADVANCED:INTERNAL=1
//Details about finding PythonInterp
FIND_PACKAGE_MESSAGE_DETAILS_PythonInterp:INTERNAL=[/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_88/Python/2.7.13/x86_64-slc6-gcc62-opt/bin/python][v2.7.13()]
//flag to check if the compilation flags have already been set
GAUDI_FLAGS_SET:INTERNAL=x86_64-slc6-gcc62-opt;sse4.2;OFF;ON
//ADVANCED property for variable: HOST_BINARY_TAG
HOST_BINARY_TAG-ADVANCED:INTERNAL=1
//ADVANCED property for variable: PYTHON_EXECUTABLE
PYTHON_EXECUTABLE-ADVANCED:INTERNAL=1
//ADVANCED property for variable: ROOT_INCLUDE_DIR
ROOT_INCLUDE_DIR-ADVANCED:INTERNAL=1
_GET_HOST_BINARY_TAG_SCRIPT:INTERNAL=/cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/LBSCRIPTS_v9r2p6/LbUtils/cmake/get_host_binary_tag.py
//ADVANCED property for variable: ccache_cmd
ccache_cmd-ADVANCED:INTERNAL=1
//ADVANCED property for variable: clang_format_cmd
clang_format_cmd-ADVANCED:INTERNAL=1
//ADVANCED property for variable: default_merge_cmd
default_merge_cmd-ADVANCED:INTERNAL=1
//ADVANCED property for variable: distcc_cmd
distcc_cmd-ADVANCED:INTERNAL=1
//ADVANCED property for variable: env_cmd
env_cmd-ADVANCED:INTERNAL=1
//ADVANCED property for variable: env_release_xml
env_release_xml-ADVANCED:INTERNAL=1
//ADVANCED property for variable: env_xml
env_xml-ADVANCED:INTERNAL=1
//ADVANCED property for variable: gaudirun_cmd
gaudirun_cmd-ADVANCED:INTERNAL=1
//ADVANCED property for variable: genconfuser_cmd
genconfuser_cmd-ADVANCED:INTERNAL=1
//ADVANCED property for variable: qmtest_metadata_cmd
qmtest_metadata_cmd-ADVANCED:INTERNAL=1
//ADVANCED property for variable: versheader_cmd
versheader_cmd-ADVANCED:INTERNAL=1
//ADVANCED property for variable: zippythondir_cmd
zippythondir_cmd-ADVANCED:INTERNAL=1

