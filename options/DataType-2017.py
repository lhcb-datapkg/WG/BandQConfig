#!/usr/bin/env python
# =============================================================================
## @file
#  Helper file to define the data type for davinci 
# =============================================================================
the_year = '2017'

# =============================================================================
from Configurables import DaVinci, CondDB, TurboConf, DstConf
DaVinci   ( DataType                  = the_year )
TurboConf ( DataType                  = the_year )
CondDB    ( LatestGlobalTagByDataType = the_year )


