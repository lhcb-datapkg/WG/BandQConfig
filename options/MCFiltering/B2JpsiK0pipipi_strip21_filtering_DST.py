"""
Stripping filtering file for B2JpsipipiK0pi MC
@author: Sujuan Wu (sujuanwu@cern.ch)
@data 2024-10-11
DaVinci Version : DaVinciDev_v36r1p2
"""

stripping='stripping21'

#from CommonParticlesArchive import CommonParticlesArchiveConf
#CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

from CommonParticles.Utils import *
from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 5 ],
                    "CloneDistCut" : [5000, 9e+99 ] }

# Build the streams and stripping object
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive

#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)
streams = buildStreams(stripping = config, archive = archive)

AllStreams = StrippingStream("B2JpsipipiK0pi.Strip")


linesToAdd = []
for stream in streams:
    if 'Dimuon' in stream.name():
        for line in stream.lines:
            if 'FullDSTDiMuonJpsi2MuMuDetachedLine' in line.name():
                line._prescale = 1.0
                linesToAdd.append(line)
AllStreams.appendLines(linesToAdd)


sc = StrippingConf(Streams=[AllStreams],
                   MaxCandidates=2000,
                   TESPrefix='Strip')

AllStreams.sequence().IgnoreFilterPassed = False # if True, we get all events written out


#Now apply selection
from Gaudi.Configuration import *
from Configurables import DaVinci, FilterDesktop, CombineParticles, OfflineVertexFitter
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, DataOnDemand, MergedSelection, MultiSelectionSequence
from Configurables          import FilterDesktop
from Configurables          import CombineParticles, CheckPV
from Configurables import LoKi__Hybrid__TupleTool
from Configurables import LoKiSvc
from Configurables import FilterInTrees, FilterDecays
from CommonMCParticles import StandardMCPions
from CommonMCParticles import StandardMCKaons


# Jpsi->mu+ mu-
_MyJpsiline= DataOnDemand(Location="/Event/Phys/FullDSTDiMuonJpsi2MuMuDetachedLine/Particles")
#_MyJpsiline.outputLocation()
_selJpsi = FilterDesktop('selJpsi')
_selJpsi.Code = ("P > 0.*MeV")
MyJpsiSel = Selection("MyJpsiSel", Algorithm=_selJpsi, RequiredSelections=[_MyJpsiline])
MyJpsiSequence = SelectionSequence("MyJpsiSequence", TopSelection = MyJpsiSel)

# pions
_stdPi = DataOnDemand(Location = "Phys/StdAllNoPIDsPions/Particles")
_selPi = FilterDesktop('selPi')
_selPi.Preambulo = ["from LoKiPhysMC.decorators import *", "from LoKiPhysMC.functions import mcMatch"]
_selPi.Code = (" (mcMatch('[pi-]cc') & (PT>50) & (PROBNNpi>0.05) & (TRGHOSTPROB<0.4) & (BPVIPCHI2()>2))")
MyPiSel = Selection("MyPiSel", Algorithm=_selPi, RequiredSelections = [_stdPi])
MyPiSequence = SelectionSequence("MyPiSequence", TopSelection = MyPiSel)

# kaons
_stdKLL = DataOnDemand(Location = "Phys/StdLooseKsLL/Particles")
_selKLL = FilterDesktop('selKLL')
_selKLL.Preambulo = ["from LoKiPhysMC.decorators import *", "from LoKiPhysMC.functions import mcMatch"]
_selKLL.Code = (" (mcMatch('[KS0]cc')) & (PT>50) ")
MyKLLSel = Selection("MyKLLSel", Algorithm=_selKLL, RequiredSelections = [_stdKLL])
MyKLLSequence = SelectionSequence("MyKLLSequence", TopSelection = MyKLLSel)

# kaons
_stdKDD = DataOnDemand(Location = "Phys/StdLooseKsDD/Particles")
_selKDD = FilterDesktop('selKDD')
_selKDD.Preambulo = ["from LoKiPhysMC.decorators import *", "from LoKiPhysMC.functions import mcMatch"]
_selKDD.Code = (" (mcMatch('[KS0]cc')) & (PT>50) ")
MyKDDSel = Selection("MyKDDSel", Algorithm=_selKDD, RequiredSelections = [_stdKDD])
MyKDDSequence = SelectionSequence("MyKDDSequence", TopSelection = MyKDDSel)






#combine B+
_makeB = CombineParticles("make_B",
     DecayDescriptor = "[B+ -> J/psi(1S) KS0 pi+ pi- pi+]cc",
     CombinationCut =  " (AM<7000*MeV) & (AM>4000*MeV)",
     MotherCut = "( (BPVDIRA>0.999) & (VFASPF(VCHI2/VDOF)<16) & (BPVIPCHI2()<30) & (PT>500) )" )
    # MotherCut = "((mcMatch('[B+ ==> psi(2S)  KS0 pi+ pi- pi+]CC')) & (BPVDIRA>0.999) & (VFASPF(VCHI2/VDOF)<16) & (BPVIPCHI2()<30) & (PT>500) )" )


#include intermediate resonances


# B+->J/psi(1S) KS0 pi+ pi- pi+

selBLL = Selection("SelBLL",
          Algorithm = _makeB,
          RequiredSelections = [MyJpsiSel, MyKLLSel, MyPiSel] )
seqLL = SelectionSequence("MCTrueR", TopSelection=selBLL )


selBDD = Selection("SelBDD",
          Algorithm = _makeB,
          RequiredSelections = [MyJpsiSel, MyKDDSel, MyPiSel] )
seqDD = SelectionSequence("MCTrueM", TopSelection=selBDD )

AllK0 = MultiSelectionSequence("B2JpsipipiK0pi.MCTruth", Sequences = [seqLL, seqDD])


# Configuration of SelDSTWriter
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import SelDSTWriter, stripDSTStreamConf, stripDSTElements

SelDSTWriterElements = {'default' : stripDSTElements(pack=enablePacking)}

SelDSTWriterConf = {'default': stripDSTStreamConf(pack=enablePacking,selectiveRawEvent=False,fileExtension='.dst')}

caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs


dstWriter = SelDSTWriter("MyDSTWriter",
                         StreamConf=SelDSTWriterConf,
                         MicroDSTElements=SelDSTWriterElements,
                         OutputFileSuffix='Filtered',
                         SelectionSequences=[AllK0])

# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x36122100)
#0xVVVVSSSS, VVVV DaVinci version, SSSS Stripping number

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().appendToMainSequence([sc.sequence() ])
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence([MyJpsiSequence.sequence()])
DaVinci().appendToMainSequence([MyPiSequence.sequence()])
DaVinci().appendToMainSequence([MyKLLSequence.sequence()])
DaVinci().appendToMainSequence([MyKDDSequence.sequence()])
DaVinci().appendToMainSequence([seqLL.sequence()])
DaVinci().appendToMainSequence([seqDD.sequence()])
DaVinci().appendToMainSequence([dstWriter.sequence()])
DaVinci().ProductionType = "Stripping"


#options
DaVinci().Simulation = True
DaVinci().InputType = 'DST'
DaVinci().DataType = "2012"
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool, name="TIMER")
TimingAuditor().TIMER.NameSize = 60
'''
############local test###########
from Configurables import DumpFSR
DumpFSR().OutputLevel = 3
DumpFSR().AsciiFileName = "dumpfsr.check.output_2012.txt"
DaVinci().MoniSequence += [DumpFSR()]
#################################

from GaudiConf import IOHelper
IOHelper().inputFiles([
'00118995_00000051_7.AllStreams.dst',
'00118999_00000009_7.AllStreams.dst'], clear=True)
'''












