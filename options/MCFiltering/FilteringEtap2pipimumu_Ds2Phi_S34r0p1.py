#!/usr/bin/env gaudirun.py
#
# Alexandre Brea
# Filter for the stripping line:
# -Etap2pipimumuDs2PhiLine
# 2018
#



#stripping version
stripping='stripping34r0p1'


#use CommonParticlesArchive
from CommonParticlesArchive import CommonParticlesArchiveConf
CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

#
# Disable the cache in Tr/TrackExtrapolators
#
from Configurables import TrackStateProvider
TrackStateProvider().CacheStatesOnDemand = False

#
#Fix for TrackEff lines
#
from Configurables import DecodeRawEvent
DecodeRawEvent().setProp("OverrideInputs",4.3)

#
# Build the streams and stripping object
#
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStreams
from StrippingArchive import strippingArchive

#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)

streams = buildStreams(stripping = config, archive = archive)


#
# Merge into one stream and run in filter mode
#
AllStreams = StrippingStream("Etap2pipimumu.Strip")


linesToAdd = []
for stream in streams:
    if 'Charm' in stream.name():
        for line in stream.lines:
            if 'StrippingEtap2pipimumuDs2PhiLine' in line.name(): 
                print line.name()
                line._prescale = 1.0
                linesToAdd.append(line)
AllStreams.appendLines(linesToAdd)


                                                

sc = StrippingConf( Streams = [ AllStreams ],
                    MaxCandidates = -1, #should be 2000?
                    TESPrefix = 'Strip',
                    AcceptBadEvents = False,
                    ActiveMDSTStream = True,
                    Verbose = True)



AllStreams.sequence().IgnoreFilterPassed = False # so we filter events by stripping selected lines


#
# Configure the dst writers for the output
#
enablePacking = True

from DSTWriters.microdstelements import *

from DSTWriters.Configuration import ( SelDSTWriter,
                                       stripDSTStreamConf,
                                       stripDSTElements,
                                       stripMicroDSTStreamConf,
                                       stripMicroDSTElements,
                                       stripCalibMicroDSTStreamConf )


SelDSTWriterElements = {
    'default'               : stripDSTElements(pack=enablePacking),
    }

SelDSTWriterConf = {
    'default'                : stripDSTStreamConf(pack=enablePacking,
                                                  selectiveRawEvent=False),
    }

dstWriter = SelDSTWriter( "MyDSTWriter",
                          StreamConf = SelDSTWriterConf,
                          MicroDSTElements = SelDSTWriterElements,
                          OutputFileSuffix ='Filtered',
                          SelectionSequences = sc.activeStreams()
                          )

# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x44103401)
#0xVVVVSSSS, VVVV DaVinci version, SSSS Stripping number


#
# DaVinci Configuration
#

from Configurables import DaVinci
DaVinci().EvtMax = -1 # Number of events 
DaVinci().Simulation = True
DaVinci().HistogramFile = "DVHistos.root"
DaVinci().appendToMainSequence( [ sc.sequence() ] )
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence( [ dstWriter.sequence() ] )
DaVinci().ProductionType = "Stripping"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool,name="TIMER")
TimingAuditor().TIMER.NameSize = 60

# Save info of rejection factor in the filtering
#from Configurables import DumpFSR
#DumpFSR().OutputLevel = 3
#DumpFSR().AsciiFileName = "dumpfsr.check.output.txt"
#DaVinci().MoniSequence += [ DumpFSR() ]


##REMOVE##  <==================== 

#DaVinci().DataType='2018'
#DaVinci().InputType='LDST'


#from commands import * 
#def nsls(dir):
#                """
#                list dir in castor
#                @param dir you want to list
#                @return list of files in dir
#                """
#                so=getstatusoutput("nsls "+dir)
#                if so[0]: return []
#                return so[1].split("\n")#


#CHOME = "/castor/cern.ch/user/j/jcidvida/dsts/23175001/merged/"
#allfiles = ["root://castorlhcb.cern.ch//"+CHOME+x for x in nsls(CHOME)]


#DaVinci().Input = allfiles



