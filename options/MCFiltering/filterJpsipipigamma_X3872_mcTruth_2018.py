from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"


# truth matching standard particle 

mcdecay  = "chi_c2(1P) ==>  ( chi_c1(1P) =>  ( J/psi(1S) =>  mu+  mu-)  gamma )  pi+  pi-"
mc_chic2 = "chi_c2(1P) ==>  ( chi_c1(1P) =>  ( J/psi(1S) =>  mu+  mu-)  gamma )  pi+  pi-"
mc_chic1 = "chi_c2(1P) ==> ^( chi_c1(1P) =>  ( J/psi(1S) =>  mu+  mu-)  gamma )  pi+  pi-"
mc_jpsi  = "chi_c2(1P) ==>  ( chi_c1(1P) => ^( J/psi(1S) =>  mu+  mu-)  gamma )  pi+  pi-"
mc_muons = "chi_c2(1P) ==>  ( chi_c1(1P) =>  ( J/psi(1S) => ^mu+ ^mu-)  gamma )  pi+  pi-"
mc_pions = "chi_c2(1P) ==>  ( chi_c1(1P) =>  ( J/psi(1S) =>  mu+  mu-)  gamma ) ^pi+ ^pi-"
mc_gamma = "chi_c2(1P) ==>  ( chi_c1(1P) =>  ( J/psi(1S) =>  mu+  mu-) ^gamma )  pi+  pi-"


from StandardParticles import StdAllLoosePions   as pions
from StandardParticles import StdLooseJpsi2MuMu  as jpsi 
from StandardParticles import StdAllLooseGammaDD as gammaDD 
from StandardParticles import StdAllLooseGammaLL as gammaLL


from PhysConf.Selections import MCMatchSelection

## jpsi = MCMatchSelection ( jpsi , mc_jpsi , charged_only = True )

from PhysConf.Selections import PrintSelection 

from PhysConf.Selections import FilterSelection
pions = FilterSelection ( 'Good_pions' ,
                          pions        , 
                          Code    = """
                          in_range ( 2.0, ETA, 4.9 )          &
                          in_range ( 3 * GeV , P, 150 * GeV ) &
                          HASRICH                             &
                          ( TRGHP     < 0.4 )                 &
                          ( TRCHI2DOF < 3.0 )                                                 
                          """
                          )

pions = MCMatchSelection ( pions , mc_pions , charged_only = True )


gammaDD = FilterSelection  ( "GammaDD" , gammaDD  , Code = " PT > 400 * MeV " )
gammaLL = FilterSelection  ( "GammaLL" , gammaLL  , Code = " PT > 400 * MeV " )
gammaDD = MCMatchSelection (  gammaDD  , mc_gamma , charged_only = False      )
gammaLL = MCMatchSelection (  gammaLL  , mc_gamma , charged_only = False      )


from PhysConf.Selections import MergedSelection
gamma    = MergedSelection ( "Gamma" , [ gammaDD , gammaLL ] )

from PhysConf.Selections import Combine4BodySelection

chi  = Combine4BodySelection ( "PsiPiPiGamma" ,
                               [ gamma , jpsi , pions ] , 
                               DecayDescriptor   = " chi_c2(1P) -> J/psi(1S) pi+ pi- gamma" ,
                               Combination12Cut  = """
                               ( AM < 6.1 * GeV       ) & 
                               ( ACHI2DOCA(1,2)  < 16 ) 
                               """ ,                            
                               Combination123Cut = """
                               ( AM < 6.1 * GeV       ) & 
                               ( ACHI2DOCA(1,3) <  16 ) &  
                               ( ACHI2DOCA(2,3) <  16 ) 
                               """ , 
                               CombinationCut  = "in_range ( 2.9 * GeV , AM , 6.1 * GeV )",
                               MotherCut       = """
                               in_range ( 2.95 * GeV , M, 6.05 * GeV ) &
                               ( PT           > 1*GeV  ) &                           
                               ( CHI2VXNDF    < 9      ) &
                               ( BPVIPCHI2 () < 4      ) &
                               ( BPVDIRA      > 0.9995 ) 
                               """
                               )

chi = MCMatchSelection ( chi , mc_chic2 , charged_only = False )


## build sequence
from PhysConf.Selections import SelectionSequence 
seq = SelectionSequence("MCTrue", TopSelection = chi )
			   

#
# Configuration of SelDSTWriter
#

enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration    import SelDSTWriter, stripDSTStreamConf, stripDSTElements

SelDSTWriterElements = { 'default' : stripDSTElements   ( pack = enablePacking ) }
SelDSTWriterConf     = { 'default' : stripDSTStreamConf ( pack = enablePacking ) }

dstWriter             = SelDSTWriter (
    "MyDSTWriter"                             ,
    StreamConf         = SelDSTWriterConf     ,
    MicroDSTElements   = SelDSTWriterElements ,
    OutputFileSuffix   = 'PromptExotics'      ,
    SelectionSequences = [ seq ]
    )

#Add stripping TCK 
from Configurables import StrippingTCK
stck = StrippingTCK ( HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x45603400 )

#DaVinci configuration

from Configurables import DaVinci
dv = DaVinci( Simulation     = True                ,
              Lumi           = False               , 
              EvtMax         = -1                  , # Number of events
              HistogramFile  = "DVHistosDDLL.root" ,
              ProductionType = "Stripping"         ) 


dv.appendToMainSequence( [ dstWriter.sequence() ] )
