"""
Stripping filtering file for B2JpsiKpipipi0 MC
@author: Sujuan Wu (sujuanwu@cern.ch)
@data 2024-10-11
DaVinci Version : DaVinciDev_v44r10p5
"""

stripping='stripping24r2'

#from CommonParticlesArchive import CommonParticlesArchiveConf
#CommonParticlesArchiveConf().redirect(stripping)

from Gaudi.Configuration import *
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

from CommonParticles.Utils import *
from CommonParticles.Utils import DefaultTrackingCuts
DefaultTrackingCuts().Cuts  = { "Chi2Cut" : [ 0, 5 ],
                    "CloneDistCut" : [5000, 9e+99 ] }

# Build the streams and stripping object
from StrippingConf.Configuration import StrippingConf, StrippingStream
from StrippingSettings.Utils import strippingConfiguration
from StrippingArchive.Utils import buildStream, buildStreams, cloneLinesFromStream
from StrippingArchive import strippingArchive

#get the configuration dictionary from the database
config  = strippingConfiguration(stripping)
#get the line builders from the archive
archive = strippingArchive(stripping)
streams = buildStreams(stripping = config, archive = archive)

AllStreams = StrippingStream("B2JpsiKpipipi0.Strip")


linesToAdd = []
for stream in streams:
    if 'Dimuon' in stream.name():
        for line in stream.lines:
            if 'FullDSTDiMuonJpsi2MuMuDetachedLine' in line.name():
                line._prescale = 1.0
                linesToAdd.append(line)
AllStreams.appendLines(linesToAdd)


sc = StrippingConf(Streams=[AllStreams],
                   MaxCandidates=2000,
                   TESPrefix='Strip')

AllStreams.sequence().IgnoreFilterPassed = False # if True, we get all events written out


#Now apply selection
from Gaudi.Configuration import *
from Configurables import DaVinci, FilterDesktop, CombineParticles, OfflineVertexFitter
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence, DataOnDemand, MergedSelection, MultiSelectionSequence
from Configurables          import FilterDesktop
from Configurables          import CombineParticles, CheckPV
from Configurables import LoKi__Hybrid__TupleTool
from Configurables import LoKiSvc
from Configurables import FilterInTrees, FilterDecays
from CommonMCParticles import StandardMCPions
from CommonMCParticles import StandardMCKaons


# Jpsi->mu+ mu-
_MyJpsiline= DataOnDemand(Location="/Event/Phys/FullDSTDiMuonJpsi2MuMuDetachedLine/Particles")
#_MyJpsiline.outputLocation()
_selJpsi = FilterDesktop('selJpsi')
_selJpsi.Code = ("P > 0.*MeV")
MyJpsiSel = Selection("MyJpsiSel", Algorithm=_selJpsi, RequiredSelections=[_MyJpsiline])
MyJpsiSequence = SelectionSequence("MyJpsiSequence", TopSelection = MyJpsiSel)

# pions
_stdPi = DataOnDemand(Location = "Phys/StdAllNoPIDsPions/Particles")
_selPi = FilterDesktop('selPi')
_selPi.Preambulo = ["from LoKiPhysMC.decorators import *", "from LoKiPhysMC.functions import mcMatch"]
_selPi.Code = (" (mcMatch('[pi-]cc') & (PT>50) & (PROBNNpi>0.05) & (TRGHOSTPROB<0.4) & (BPVIPCHI2()>2))")
MyPiSel = Selection("MyPiSel", Algorithm=_selPi, RequiredSelections = [_stdPi])
MyPiSequence = SelectionSequence("MyPiSequence", TopSelection = MyPiSel)

# kaons
_stdK = DataOnDemand(Location = "Phys/StdAllNoPIDsKaons/Particles")
_selK = FilterDesktop('selK')
_selK.Preambulo = ["from LoKiPhysMC.decorators import *", "from LoKiPhysMC.functions import mcMatch"]
_selK.Code = (" (mcMatch('[K+]cc')) & (PT>50) & (PROBNNk>0.05) & (TRGHOSTPROB<0.4) & (BPVIPCHI2()>2)")
MyKSel = Selection("MyKSel", Algorithm=_selK, RequiredSelections = [_stdK])
MyKSequence = SelectionSequence("MyKSequence", TopSelection = MyKSel)


# resolved pi0
_stdrepi0 = DataOnDemand(Location = "Phys/StdLooseResolvedPi0/Particles")
_selrePi0 = FilterDesktop('selrePi0')
_selrePi0.Preambulo = ["from LoKiPhysMC.decorators import *", "from LoKiPhysMC.functions import mcMatch"]
_selrePi0.Code = ("(mcMatch('[pi0]cc')) & (PT > 300*MeV)")
MyrePi0Sel = Selection("MyrePi0Sel", Algorithm=_selrePi0, RequiredSelections = [_stdrepi0])
MyrePi0Sequence = SelectionSequence("MyrePi0Sequence", TopSelection = MyrePi0Sel)

# merged pi0
_stdmePi0 = DataOnDemand(Location = "Phys/StdLooseMergedPi0/Particles")
_selmePi0 = FilterDesktop('selmePi0')
_selmePi0.Preambulo = ["from LoKiPhysMC.decorators import *", "from LoKiPhysMC.functions import mcMatch"]
_selmePi0.Code = ("(mcMatch('[pi0]cc')) & (PT > 300*MeV) & (M > 75*MeV) & (M < 195*MeV)")
MymePi0Sel = Selection("MymePi0Sel", Algorithm = _selmePi0, RequiredSelections = [ _stdmePi0 ])
MymePi0Sequence = SelectionSequence("MymePi0Sequence", TopSelection = MymePi0Sel)


#combine B+
_makeB = CombineParticles("make_B",
     DecayDescriptor = "[B+ -> J/psi(1S) K+ pi+ pi- pi0]cc",
     CombinationCut =  " (AM<5600*MeV) & (AM>5000*MeV)",
     MotherCut = "((mcMatch('[B+ ==> J/psi(1S)  K+ pi+ pi- pi0]CC')) & (BPVDIRA>0.999) & (VFASPF(VCHI2/VDOF)<16) & (BPVIPCHI2()<30) & (PT>500) )" )


#include intermediate resonances


# B+->J/psi(1S) K+ pi+ pi- pi0

selBR = Selection("SelBR",
          Algorithm = _makeB,
          RequiredSelections = [MyJpsiSel, MyPiSel, MyKSel, MyrePi0Sel] )
seqR = SelectionSequence("MCTrueR", TopSelection=selBR )


selBM = Selection("SelBM",
          Algorithm = _makeB,
          RequiredSelections = [MyJpsiSel, MyPiSel, MyKSel, MymePi0Sel] )
seqM = SelectionSequence("MCTrueM", TopSelection=selBM )

AllPi0 = MultiSelectionSequence("B2JpsiKpipipi0.MCTruth", Sequences = [seqR, seqM])


# Configuration of SelDSTWriter
enablePacking = True

from DSTWriters.microdstelements import *
from DSTWriters.Configuration import SelDSTWriter, stripDSTStreamConf, stripDSTElements

SelDSTWriterElements = {'default' : stripDSTElements(pack=enablePacking)}

SelDSTWriterConf = {'default': stripDSTStreamConf(pack=enablePacking,selectiveRawEvent=False,fileExtension='.dst')}

caloProtoReprocessLocs = [ "/Event/pRec/ProtoP#99", "/Event/pRec/Calo#99" ]
SelDSTWriterConf['default'].extraItems += caloProtoReprocessLocs


dstWriter = SelDSTWriter("MyDSTWriter",
                         StreamConf=SelDSTWriterConf,
                         MicroDSTElements=SelDSTWriterElements,
                         OutputFileSuffix='Filtered',
                         SelectionSequences=[AllPi0])

# Add stripping TCK
from Configurables import StrippingTCK
stck = StrippingTCK(HDRLocation = '/Event/Strip/Phys/DecReports', TCK=0x44a52420)
#0xVVVVSSSS, VVVV DaVinci version, SSSS Stripping number

#
# DaVinci Configuration
#
from Configurables import DaVinci
DaVinci().appendToMainSequence([sc.sequence() ])
DaVinci().appendToMainSequence( [ stck ] )
DaVinci().appendToMainSequence([MyJpsiSequence.sequence()])
DaVinci().appendToMainSequence([MyPiSequence.sequence()])
DaVinci().appendToMainSequence([MyKSequence.sequence()])
DaVinci().appendToMainSequence([MyrePi0Sequence.sequence()])
DaVinci().appendToMainSequence([MymePi0Sequence.sequence()])
DaVinci().appendToMainSequence([seqR.sequence()])
DaVinci().appendToMainSequence([seqM.sequence()])
DaVinci().appendToMainSequence([dstWriter.sequence()])
DaVinci().ProductionType = "Stripping"


#options
DaVinci().Simulation = True
DaVinci().InputType = 'DST'
DaVinci().DataType = "2015"
DaVinci().EvtMax = -1
DaVinci().HistogramFile = "DVHistos.root"

# Change the column size of Timing table
from Configurables import TimingAuditor, SequencerTimerTool
TimingAuditor().addTool(SequencerTimerTool, name="TIMER")
TimingAuditor().TIMER.NameSize = 60
'''
############local test###########
from Configurables import DumpFSR
DumpFSR().OutputLevel = 3
DumpFSR().AsciiFileName = "dumpfsr.check.output_2015.txt"
DaVinci().MoniSequence += [DumpFSR()]
#################################

from GaudiConf import IOHelper
IOHelper().inputFiles([
'/afs/cern.ch/user/s/sujuan/00109288_00000074_7.AllStreams.dst',
'/afs/cern.ch/user/s/sujuan/00109290_00000132_7.AllStreams.dst',
'/afs/cern.ch/user/s/sujuan/00092194_00000083_7.AllStreams.dst',
'/afs/cern.ch/user/s/sujuan/00092198_00000261_7.AllStreams.dst'], clear=True)

'''












