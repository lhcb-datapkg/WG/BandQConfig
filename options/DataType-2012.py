#!/usr/bin/env python
# =============================================================================
## @file
#  Helper file to define the data type for davinci 
# =============================================================================
the_year = '2012'

# =============================================================================
from Configurables import DaVinci, CondDB, DstConf
DaVinci   ( DataType                  = the_year )
CondDB    ( LatestGlobalTagByDataType = the_year )
