## produced by the script get-files-from-BK:
## get-files-from-BK /LHCb/Collision11/Beam3500GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21r1/90000000/DIMUON.DST -S CERN-SWTEST -m 40 -a DV
from GaudiConf import IOHelper
IOHelper().inputFiles([
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision11/DIMUON.DST/00041840/0000/00041840_00000007_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision11/DIMUON.DST/00041840/0000/00041840_00000249_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision11/DIMUON.DST/00041840/0000/00041840_00000292_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision11/DIMUON.DST/00041840/0000/00041840_00000660_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision11/DIMUON.DST/00041840/0000/00041840_00000718_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision11/DIMUON.DST/00041840/0000/00041840_00000894_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision11/DIMUON.DST/00041840/0000/00041840_00000942_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision11/DIMUON.DST/00041840/0000/00041840_00001022_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision11/DIMUON.DST/00041840/0000/00041840_00001084_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision11/DIMUON.DST/00041840/0000/00041840_00001186_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision11/DIMUON.DST/00041840/0000/00041840_00001209_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision11/DIMUON.DST/00041840/0000/00041840_00001210_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision11/DIMUON.DST/00041840/0000/00041840_00001242_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision11/DIMUON.DST/00041840/0000/00041840_00001243_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision11/DIMUON.DST/00041840/0000/00041840_00001312_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision11/DIMUON.DST/00041840/0000/00041840_00001887_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision11/DIMUON.DST/00041840/0000/00041840_00001915_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision11/DIMUON.DST/00041840/0000/00041840_00001957_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision11/DIMUON.DST/00041840/0000/00041840_00002078_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision11/DIMUON.DST/00041840/0000/00041840_00002160_1.dimuon.dst",
   ], clear =  True )
