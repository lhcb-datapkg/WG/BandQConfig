## produced by the script get-files-from-BK:
## get-files-from-BK '/LHCb/Collision12/Beam4000GeV-VeloClosed-MagDown/Real Data/Reco14/Stripping21/90000000/DIMUON.DST' -S CERN-SWTEST -m 40 -a DV
from GaudiConf import IOHelper
IOHelper().inputFiles([
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00041836/0000/00041836_00000007_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00041836/0000/00041836_00000035_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00041836/0000/00041836_00000049_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00041836/0000/00041836_00000122_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00041836/0000/00041836_00000171_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00041836/0000/00041836_00000207_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00041836/0000/00041836_00000238_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00041836/0000/00041836_00000260_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00041836/0000/00041836_00000272_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00041836/0000/00041836_00000284_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00041836/0000/00041836_00000340_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00041836/0000/00041836_00000354_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00041836/0000/00041836_00000356_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00041836/0000/00041836_00000407_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00041836/0000/00041836_00000410_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00041836/0000/00041836_00000432_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00041836/0000/00041836_00000466_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00041836/0000/00041836_00000500_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00041836/0000/00041836_00000508_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00041836/0000/00041836_00000549_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00041836/0000/00041836_00002609_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision12/DIMUON.DST/00041836/0000/00041836_00006224_1.dimuon.dst",
   ], clear =  True )

