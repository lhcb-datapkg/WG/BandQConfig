## produced by the script get-files-from-BK:
## get-files-from-BK '/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Turbo04/94000000/LEPTONS.MDST' -S CERN-SWTEST -m 40 -a DV
from GaudiConf import IOHelper
IOHelper().inputFiles([
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/LEPTONS.MDST/00064413/0000/00064413_00000005_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/LEPTONS.MDST/00064413/0000/00064413_00000010_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/LEPTONS.MDST/00064413/0000/00064413_00000020_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/LEPTONS.MDST/00064413/0000/00064413_00000030_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/LEPTONS.MDST/00064413/0000/00064413_00000035_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/LEPTONS.MDST/00064413/0000/00064413_00000046_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/LEPTONS.MDST/00064413/0000/00064413_00000054_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/LEPTONS.MDST/00064413/0000/00064413_00000068_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/LEPTONS.MDST/00064413/0000/00064413_00000167_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/LEPTONS.MDST/00064413/0000/00064413_00000172_1.leptons.mdst",
   ], clear =  True )
