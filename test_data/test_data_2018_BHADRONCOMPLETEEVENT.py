## produced by the script get-files-from-BK:
## get-files-from-BK /LHCb/Collision18/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco18/Stripping34/90000000/BHADRONCOMPLETEEVENT.DST -S CERN-SWTEST -m 20 -a DV
from GaudiConf import IOHelper
IOHelper().inputFiles([
   "PFN:root://x509up_u26482@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00075555/0000/00075555_00000010_1.bhadroncompleteevent.dst",
   "PFN:root://x509up_u26482@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00075555/0000/00075555_00000018_1.bhadroncompleteevent.dst",
   "PFN:root://x509up_u26482@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00075555/0000/00075555_00000042_1.bhadroncompleteevent.dst",
   "PFN:root://x509up_u26482@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00075555/0000/00075555_00000087_1.bhadroncompleteevent.dst",
   "PFN:root://x509up_u26482@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00075555/0000/00075555_00000108_1.bhadroncompleteevent.dst",
   "PFN:root://x509up_u26482@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00075555/0000/00075555_00000114_1.bhadroncompleteevent.dst",
   "PFN:root://x509up_u26482@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00075555/0000/00075555_00000137_1.bhadroncompleteevent.dst",
   "PFN:root://x509up_u26482@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00075555/0000/00075555_00000138_1.bhadroncompleteevent.dst",
   "PFN:root://x509up_u26482@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00075555/0000/00075555_00000149_1.bhadroncompleteevent.dst",
   "PFN:root://x509up_u26482@eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision18/BHADRONCOMPLETEEVENT.DST/00075555/0000/00075555_00000162_1.bhadroncompleteevent.dst",
   ], clear =  True )
