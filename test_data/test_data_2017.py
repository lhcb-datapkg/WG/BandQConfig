## produced by the script get-files-from-BK:
## get-files-from-BK '/LHCb/Collision17/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco17/Stripping29r2/90000000/DIMUON.DST' -S CERN-SWTEST -m 40 -a DV
from GaudiConf import IOHelper
IOHelper().inputFiles([
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/DIMUON.DST/00071501/0000/00071501_00000161_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/DIMUON.DST/00071501/0000/00071501_00000205_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/DIMUON.DST/00071501/0000/00071501_00000244_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/DIMUON.DST/00071501/0000/00071501_00000257_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/DIMUON.DST/00071501/0000/00071501_00000281_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/DIMUON.DST/00071501/0000/00071501_00000324_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/DIMUON.DST/00071501/0000/00071501_00000337_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/DIMUON.DST/00071501/0000/00071501_00000351_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/DIMUON.DST/00071501/0000/00071501_00000374_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/DIMUON.DST/00071501/0000/00071501_00000393_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/DIMUON.DST/00071501/0000/00071501_00000426_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/DIMUON.DST/00071501/0000/00071501_00000430_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/DIMUON.DST/00071501/0000/00071501_00000459_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/DIMUON.DST/00071501/0000/00071501_00000482_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/DIMUON.DST/00071501/0000/00071501_00000518_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/DIMUON.DST/00071501/0000/00071501_00000535_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/DIMUON.DST/00071501/0000/00071501_00000541_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/DIMUON.DST/00071501/0000/00071501_00000591_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/DIMUON.DST/00071501/0000/00071501_00000598_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision17/DIMUON.DST/00071501/0000/00071501_00000632_1.dimuon.dst",
   ], clear =  True )
