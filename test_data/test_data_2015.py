## produced by the script get-files-from-BK:
## get-files-from-BK '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco15a/Stripping24r1/90000000/DIMUON.DST' -S CERN-SWTEST -m 40 -a DV
from GaudiConf import IOHelper
IOHelper().inputFiles([
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/DIMUON.DST/00068487/0000/00068487_00000006_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/DIMUON.DST/00068487/0000/00068487_00000090_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/DIMUON.DST/00068487/0000/00068487_00000164_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/DIMUON.DST/00068487/0000/00068487_00000226_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/DIMUON.DST/00068487/0000/00068487_00000447_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/DIMUON.DST/00068487/0000/00068487_00000482_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/DIMUON.DST/00068487/0000/00068487_00000533_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/DIMUON.DST/00068487/0000/00068487_00000603_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/DIMUON.DST/00068487/0000/00068487_00000613_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/DIMUON.DST/00068487/0000/00068487_00000628_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/DIMUON.DST/00068487/0000/00068487_00000680_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/DIMUON.DST/00068487/0000/00068487_00000712_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/DIMUON.DST/00068487/0000/00068487_00000765_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/DIMUON.DST/00068487/0000/00068487_00000802_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/DIMUON.DST/00068487/0000/00068487_00000837_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/DIMUON.DST/00068487/0000/00068487_00000849_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/DIMUON.DST/00068487/0000/00068487_00000966_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/DIMUON.DST/00068487/0000/00068487_00001232_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/DIMUON.DST/00068487/0000/00068487_00001236_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/DIMUON.DST/00068487/0000/00068487_00001250_1.dimuon.dst",
   ], clear =  True )
