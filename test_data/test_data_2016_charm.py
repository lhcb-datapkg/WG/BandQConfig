## produced by the script get-files-from-BK:
## get-files-from-BK '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagUp/Real Data/Reco16/Stripping28r1/90000000/CHARM.MDST' -m 20 -S CERN-SWTEST -a DV
from GaudiConf import IOHelper
IOHelper().inputFiles([
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/CHARM.MDST/00069593/0000/00069593_00000072_1.charm.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/CHARM.MDST/00069593/0000/00069593_00000137_1.charm.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/CHARM.MDST/00069593/0000/00069593_00000233_1.charm.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/CHARM.MDST/00069593/0000/00069593_00000290_1.charm.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/CHARM.MDST/00069593/0000/00069593_00000504_1.charm.mdst",
   ], clear =  True )

