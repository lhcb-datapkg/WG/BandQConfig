## produced by the script get-files-from-BK:
## get-files-from-BK '/LHCb/Collision15/Beam6500GeV-VeloClosed-MagDown/Real Data/Turbo02/RawRemoved/94000000/TURBO.MDST' -S CERN-SWTEST -m 40 -a DV
from GaudiConf import IOHelper
IOHelper().inputFiles([
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/TURBO.MDST/00051290/0000/00051290_00000001_1.turbo.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/TURBO.MDST/00051290/0000/00051290_00000002_1.turbo.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/TURBO.MDST/00051290/0000/00051290_00000003_1.turbo.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/TURBO.MDST/00051290/0000/00051290_00000004_1.turbo.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/TURBO.MDST/00051290/0000/00051290_00000005_1.turbo.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/TURBO.MDST/00051290/0000/00051290_00000006_1.turbo.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/TURBO.MDST/00051290/0000/00051290_00000007_1.turbo.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/TURBO.MDST/00051290/0000/00051290_00000008_1.turbo.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/TURBO.MDST/00051290/0000/00051290_00000009_1.turbo.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision15/TURBO.MDST/00051290/0000/00051290_00000010_1.turbo.mdst",
   ], clear =  True )
