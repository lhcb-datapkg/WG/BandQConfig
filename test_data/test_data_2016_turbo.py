## produced by the script get-files-from-BK:
## get-files-from-BK /LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Turbo03a/94000000/LEPTONS.MDST -m 100 -S CERN-SWTEST -a DV
from GaudiConf import IOHelper
IOHelper().inputFiles([
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/LEPTONS.MDST/00076510/0000/00076510_00000014_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/LEPTONS.MDST/00076510/0000/00076510_00000039_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/LEPTONS.MDST/00076510/0000/00076510_00000072_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/LEPTONS.MDST/00076510/0000/00076510_00000130_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/LEPTONS.MDST/00076510/0000/00076510_00000147_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/LEPTONS.MDST/00076510/0000/00076510_00000169_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/LEPTONS.MDST/00076510/0000/00076510_00000201_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/LEPTONS.MDST/00076510/0000/00076510_00000289_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/LEPTONS.MDST/00076510/0000/00076510_00000314_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/LEPTONS.MDST/00076510/0000/00076510_00000375_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/LEPTONS.MDST/00076510/0000/00076510_00000425_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/LEPTONS.MDST/00076510/0000/00076510_00000426_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/LEPTONS.MDST/00076510/0000/00076510_00000453_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/LEPTONS.MDST/00076510/0000/00076510_00000483_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/LEPTONS.MDST/00076510/0000/00076510_00000494_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/LEPTONS.MDST/00076510/0000/00076510_00000516_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/LEPTONS.MDST/00076510/0000/00076510_00000547_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/LEPTONS.MDST/00076510/0000/00076510_00000567_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/LEPTONS.MDST/00076510/0000/00076510_00000604_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/LEPTONS.MDST/00076510/0000/00076510_00000605_1.leptons.mdst",
   ], clear =  True )
