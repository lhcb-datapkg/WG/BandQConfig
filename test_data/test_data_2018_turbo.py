## produced by the script get-files-from-BK:
## get-files-from-BK /LHCb/Collision18/Beam6500GeV-VeloClosed-MagUp/Real Data/Turbo05/94000000/LEPTONS.MDST -S CERN-SWTEST -m 40 -a DV
from GaudiConf import IOHelper
IOHelper().inputFiles([
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision18/LEPTONS.MDST/00075136/0000/00075136_00000007_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision18/LEPTONS.MDST/00075136/0000/00075136_00000014_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision18/LEPTONS.MDST/00075136/0000/00075136_00000028_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision18/LEPTONS.MDST/00075136/0000/00075136_00000035_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision18/LEPTONS.MDST/00075136/0000/00075136_00000042_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision18/LEPTONS.MDST/00075136/0000/00075136_00000056_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision18/LEPTONS.MDST/00075136/0000/00075136_00000077_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision18/LEPTONS.MDST/00075136/0000/00075136_00000084_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision18/LEPTONS.MDST/00075136/0000/00075136_00000091_1.leptons.mdst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision18/LEPTONS.MDST/00075136/0000/00075136_00000105_1.leptons.mdst",
   ], clear =  True )
