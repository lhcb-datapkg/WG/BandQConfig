
## produced by the script get-files-from-BK:
## get-files-from-BK '/LHCb/Collision16/Beam6500GeV-VeloClosed-MagDown/Real Data/Reco16/Stripping28r1/90000000/DIMUON.DST' -S CERN-SWTEST -m 40 -a DV
from GaudiConf import IOHelper
IOHelper().inputFiles([
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/DIMUON.DST/00069527/0000/00069527_00000215_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/DIMUON.DST/00069527/0000/00069527_00000300_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/DIMUON.DST/00069527/0000/00069527_00000411_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/DIMUON.DST/00069527/0000/00069527_00000499_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/DIMUON.DST/00069527/0000/00069527_00000581_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/DIMUON.DST/00069527/0000/00069527_00000608_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/DIMUON.DST/00069527/0000/00069527_00000692_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/DIMUON.DST/00069527/0000/00069527_00000775_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/DIMUON.DST/00069527/0000/00069527_00000812_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/DIMUON.DST/00069527/0000/00069527_00000833_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/DIMUON.DST/00069527/0000/00069527_00000906_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/DIMUON.DST/00069527/0000/00069527_00000959_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/DIMUON.DST/00069527/0000/00069527_00001008_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/DIMUON.DST/00069527/0000/00069527_00001018_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/DIMUON.DST/00069527/0000/00069527_00001098_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/DIMUON.DST/00069527/0000/00069527_00001117_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/DIMUON.DST/00069527/0000/00069527_00001177_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/DIMUON.DST/00069527/0000/00069527_00001187_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/DIMUON.DST/00069527/0000/00069527_00001283_1.dimuon.dst",
   "PFN:root://eoslhcb.cern.ch//eos/lhcb/grid/prod/lhcb/swtest/lhcb/LHCb/Collision16/DIMUON.DST/00069527/0000/00069527_00001294_1.dimuon.dst",
   ], clear =  True )
