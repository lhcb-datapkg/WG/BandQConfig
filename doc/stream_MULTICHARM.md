# `MUTICHARM` stream 


`MULTICHARM.MDST` is created from input `CHARM.MDST` using 
```
gaudirun.py MultiCharm.py ./DataType-XXX.py input_data
```

The lines:

  1. `Phys/DiCharmForPromptCharm/Particles`          
  1. `Phys/TriCharmForPromptCharm/Particles`         
  1. `Phys/DiMuonAndCharmForPromptCharm/Particles`   
  1. `Phys/DiMuonAndDiCharmForPromptCharm/Particles` 
