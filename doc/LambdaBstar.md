# `LambdaBstar` module  [`LambdaBstar.py`](../scripts/LambdaBstar.py)

The module  is used for processing of `BCOMPLETEEVENT.DST` stripping  stream 

| Output location                         | Selection       |   Content                                       |
|:----------------------------------------|:----------------|:------------------------------------------------|
| `Phys/LambdabWithBPV/Particles`  | | $`\Lambda_b^- \rightarrow ( \Lambda_c^+ \rigtharrow p K^- \pi^+) \pi^-`$ | 
| `Phys/LambdabStar/Particles`     | | $`\Lambda_b(5929)^0  \rightarrow \Lambda_b^0 \pi^+ \pi^-`$, $`\Lambda_b(5929)^0  \rightarrow \Lambda_b^0 \pi^+ \pi^+`$, $`\Lambda_b(5929)^0  \rightarrow \Lambda_b^0 \pi^- \pi^-`$ | 

$`\Lambda_b^- \rightarrow ( \Lambda_c^+ \rigtharrow p K^- \pi^+) \pi^-`$ is selected  by  
`Lb2LcPiNoIPLc2PKPiBeauty2CharmLine` from `BhadronCompleteEvent` stripping   stream 