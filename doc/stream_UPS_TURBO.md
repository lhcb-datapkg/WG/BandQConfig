# `UPS_TURBO` stream 

`UPS_TURBO.MDST` is created from input `LEPTONS.MDST` using 
```
gaudirun.py ./DataType-YEAR-TurboXXX.py ./UPS_turbo.py input_data
```


The stream contains:

| Output location                         |   Content		                 	             |
|:--------  ------------------------------|:---------------------------------------------------------|	 
| `Phys/Y&pi/Particles`                   | $`X\rightarrow \Upsilon(1S)\pi^+`$                       | 
| `Phys/Y&KK/Particles`                   | $`X\rghtarrow \Upsilon(1S) (\phi \rigtharrow K^+ K^-) `$ | 
| `Phys/UPSILON/Particles`                |                                                          | 
  
