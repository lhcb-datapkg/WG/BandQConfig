# `MultiCharm` module  [`MultiCharm.py`](../scripts/MultiCharm.py)

The module  is used for processing of `CHARM.MDST` stripping  stream 

| Output location                         | Selection       |   Content                                       |
|:----------------------------------------|:----------------|:------------------------------------------------|
| `Phys/DiCharmForPromptCharm/Particles`             |    |   all (prompt) di-charm combniations | 
| `Phys/TriCharmForPromptCharm/Particles`            |    |   all (prompt) tri-charm combniations | 
| `Phys/DiMuonAndCharmForPromptCharm/Particles`      |    |   all (prompt) $`\mu^+\mu^-`$ and charm combinations | 
| `Phys/DiMuonAndDiCharmForPromptCharm/Particles`    |    |   all (prompt) $`\mu^+\mu^-`$ and di-charm combinations | 


For details see `PromptCharm` stripping module
