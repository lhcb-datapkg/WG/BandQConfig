# `PSIX` stream 

`PSIX.MDST` stream is created from input `DIMUON.DST` using 
```
gaudirun.py ./BandQ.py ./DataType-XXX.py input_data
```

The stream contains:

  *  All lines  from [`WG_PSIX`](WG_PSIX.md) module 
  *  Four lines from [`WG_Combinations` module](WG_Combinartions.md)
    
      1. `Phys/SelB&B/Particles`                    
      1. `Phys/SelB&C/Particles`                 
      1. `Phys/SelB&2Mu/Particles`               
      1. `Phys/SelB&W/Particles`                  

  *  Set of lines from [`WG_PSIX0`](WG_PSIX0.md) module 

      1. `Phys/SelBc2PsiRhoForPsiX0/Particles`   
      1. `Phys/SelBu2PsiKstarForPsiX0/Particles`  
      1. `Phys/SelB2ChicKForPsiX0/Particles`     
      1. `Phys/SelB2Chic2KForPsiX0/Particles`     
      1. `Phys/SelB2Chic3KForPsiX0/Particles`    
      1. `Phys/SelB2Chic2KPiForPsiX0/Particles`  
      1. `Phys/SelB2Chic3KPiForPsiX0/Particles`  
      1. `Phys/SelB2Chic2PiForPsiX0/Particles`    
      1. `Phys/SelB2Chic3PiForPsiX0/Particles`   
      1. `Phys/SelBc2ChicPiForPsiX0/Particles`   
      1. `Phys/SelLb2ChicPKForPsiX0/Particles`   
      1. `Phys/SelLb2ChicPPiForPsiX0/Particles`  
      1. `Phys/SelB2X3872KForPsiX0/Particles`    
      1. `Phys/SelB2X3872KPiForPsiX0/Particles`  
      1. `Phys/SelLb2X3872PKForPsiX0/Particles`  


