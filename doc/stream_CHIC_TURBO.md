# `CHIC_TURBO` stream 

`CHIC_TURBO.MDST` is created from input `LEPTONS.MDST` using 
```
gaudirun.py ./DataType-YEAR-TurboXXX.py ./CHIC_turbo.py input_data
```

The stream contains:

| Output location                         |   Content					         |
|:--------  ------------------------------|:------------------------------------------------|	 
| `Phys/ChicToJpsiMuMu/Particles`         | $`\chi_{c1,2}\rightarrow (J/\psi\rightarrow \mu^+\mu^-) \mu ^+ \mu^-`$ | 
| `Phys/Psi2SToJpsiPiPi/Particles`        | $`\psi(2S)\rightarrow (J/\psi\rightarrow \mu^+\mu^-) \pi^+ \p i^-`$ | 
| `Phys/ChicTurbo/Particles`              | $`\chi_{c1,2}\rightarrow (J/\psi\rightarrow \mu^+\mu^-) \mu ^+ \mu^-`$ | 
  
