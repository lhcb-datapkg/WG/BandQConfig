# `BOTTOM` stream 

`BOTTOM.MDST` is created from input `DIMUON.DST` using 
```
gaudirun.py ./BandQ.py ./DataType-XXX.py input_data
```


The stream contains set of lines from  from [`WG_UpsilonExotic`](WG_UpsilonExotic.md) module 

  1. `Phys/SelUpsilonForUpsilonExotica/Particles`
  1. `Phys/SelY&KKForUpsilonExotica/Particles`
  1. `Phys/SelY&etaForUpsilonExotica/Particles`
  1. `Phys/SelY&eta_primeForUpsilonExotica/Particles`
  1. `Phys/SelY&omegaForUpsilonExotica/Particles`
  1. `Phys/SelY&pForUpsilonExotica/Particles`
  1. `Phys/SelY&ppForUpsilonExotica/Particles`
  1. `Phys/SelY&pKForUpsilonExotica/Particles`
  1. `Phys/SelY&mumuForUpsilonExotica/Particles`


And a set of lines from  from [`WG_Combinations`](WG_Combinations.md) module 

  1. `Phys/Y&Charm/Particles`
  1. `Phys/Y&Beauty/Particles`   (from v17r4)
  1. `Phys/Y&psi/Particles`
  1. `Phys/Y&psiB/Particles`
  1. `Phys/2xY/Particles`
  1. `Phys/2xB/Particles`
  1. `Phys/Psi&P/Particles`
  1. `Phys/Psi&PP/Particles`

And lines from  from [`WG_CHIC`](WG_CHIC.md) module 

  1. `Phys/ChicToJpsiMiMu/Particles`



