# `BBandBD` module  [`BandD.py`](../scripts/BandD.py)

The module  is used for processing of `BHADRONCOMPLETEEVENTS.DST` stripping  stream 

Input stripping lines (common suffix is `Beauty2CharmLine`)
 - `Lb2LcPiNoIPLc2PKPi`
 - `Lb2LcPiPiPiLc2PKPi`     
 - `Lb2XicPiNoIPXic2PKPi`  
 - `Xib2Xic0PiNoIPXic02PKKPi` 
 - `Xib2Xic0PiPiPiXic02PKKPi` 
 - `B2D0PiD2HH`              
 - `B2D0PiD2HHHHTIGHT`     
 - `B2D0PiPiPiD2HHTIGHT` 
 - `B02DPiD2HHH`        
 - `B02DPiPiPiD2HHHPID`

| Output location                         | Selection       |   Content                                       |
|:----------------------------------------|:----------------|:------------------------------------------------|
| `Phys/Hb&Hb/Particles`                  | `BB`            | all BB~ and BB combinations                     |
| `Phys/Hc&Hc/Particles`                  | `BC`            | all beauty + charm combinations                 |
