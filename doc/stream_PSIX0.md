# `PSIX0` stream 

`PSIX0.MDST` is created from input `DIMUON.DST` using 
```
gaudirun.py ./BandQ.py ./DataType-XXX.py input_data
```

The stream contains set of lines from  from [`WG_PSIX0`](WG_PSIX0.md) module 

    1. `Phys/SelB2PsiEtaForPsiX0/Particles`
    1. `Phys/SelB2PsiEtaPrimeForPsiX0/Particles`
    1. `Phys/SelB2PsiOmegaForPsiX0/Particles`
    1. `Phys/SelB2PsiKEtaForPsiX0/Particles`
    1. `Phys/SelB2PsiKEtaPrimeForPsiX0/Particles`
    1. `Phys/SelB2PsiKOmegaForPsiX0/Particles`
    1. `Phys/SelB2PsiKKEtaForPsiX0/Particles`
    1. `Phys/SelB2PsiKKEtaPrimeForPsiX0/Particles`
    1. `Phys/SelB2PsiKKOmegaForPsiX0/Particles`
    1. `Phys/SelB2PsiPiEtaForPsiX0/Particles`
    1. `Phys/SelB2PsiPiEtaPrimeForPsiX0/Particles`
    1. `Phys/SelB2PsiPiOmegaForPsiX0/Particles`
    1. `Phys/SelB2PsiPiPiEtaForPsiX0/Particles`
    1. `Phys/SelB2PsiPiPiEtaPrimeForPsiX0/Particles`
    1. `Phys/SelB2PsiPiPiOmegaForPsiX0/Particles`
    1. `Phys/SelB2PsiKPiEtaForPsiX0/Particles`
    1. `Phys/SelB2PsiKPiEtaPrimeForPsiX0/Particles`
    1. `Phys/SelB2PsiKPiOmegaForPsiX0/Particles`

Note that some lines from from [`WG_PSIX0`](WG_PSIX0.md) module go to  [`PSIX`](stream_PSIX.md) stream 




