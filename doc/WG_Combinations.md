# `WG_Combinations` module [`WG_Combinations.py`](../scripts/WG_Combinations.py)

The module  is used for processing of `DIMUON.DST` stripping  stream 

| Output location                         | Selection       |   Content                                       |
|:----------------------------------------|:----------------|:------------------------------------------------|
| `Phys/SelB&B/Particles`         | `bb_sel` | $`\chi_{b0}(2P) \rightarrow B^0\bar{B}^0, B^+B^-, B^0_s \bar{B}^0_s, B^0B^0, B^0B^+, B^0B^0_s, B^0 B^-, B^0\bar{B}^0_s, B^+B^+, B^+B^0_s, B^+\bar{B}^0_s`$ |        
| `Phys/SelB&C/Particles`         | `bc_sel`     | $`\chi_{b0}(2P)\rightarrow B^0 D^0, B^0 D^+, B^0 D^{*+}, B^0 D_s^+, B^0 \Lambda_c^+`$, $`\chi_{b0}\rightarrow B^0 \bar{D}^0, B^0 D^-, B^0 D^{*-}, B^0 D_s^-, B^0 \bar{\Lambda}_c^-`$, $`\chi_{b0}\rightarrow B^+ D^0, B^+ D^+, B^+ D^{*+}, B^+ D_s^+, B^+ \Lambda_c^+, B^+ \bar{D}^0`$, $`\chi_{b0}\rightarrow B^+ D^-, B^+ D^{*-}, B^+ D_s^-, B^+ \bar{\Lambda}_c^-, B^0_s D^0`$, $`\chi_{b0}\rightarrow B^0_s D^+, B^0_s D^{*+}, B^0_s D_s^+, B^0_s \Lambda_c^+`$, $`\chi_{b0}\rightarrow B^0_s \bar{D}^0, B^0_s D^-, B^0_s D^{*-},B^0_s D_s^-, B^0_s \bar{\Lambda}_c^-`$ |
| `Phys/SelB&2Mu/Particles`       | `bm_sel`     | $`\chi_{b0}(2P) \rightarrow B^0 (\mu\mu), B^+ (\mu\mu), B^0_s (\mu\mu)`$ | 
| `Phys/SelB&W/Particles`         | `bw_sel`     | $`\chi_{b0}(2P) \rightarrow B^0 \mu_{W}, B^+ \mu_{W}, B^0_s \mu_{W}`$ | 
| `Phys/Y&Charm/Particles`        | `sel_Ycharm` | $`\chi_{b1}(1P) \rightarrow\Upsilon(1S)D^0,\Upsilon(1S)D^+,\Upsilon(1S)D^{*+},\Upsilon(1S)D^{+}_s,\Upsilon(1S)\Lambda_c^+`$ | 
| `Phys/Y&psi/Particles`          | `sel_YPsi`   | $`\chi_{b1}(1P) \rightarrow\Upsilon(1S) (\mu\mu)`$ | 
| `Phys/Y&psiB/Particles`         | `sel_YPsiB`  | $`\chi_{b2}(1P) \rightarrow\Upsilon(1S) \psi_{b}`$ | 
| `Phys/2xY/Particles`            | `sel_2xY`    | $`\chi_{b0}(1P) \rightarrow\Upsilon(1S)\Upsilon(1S)`$ | 
| `Phys/2xB/Particles`            | `sel_2xPsiB` | $`\chi_{b2}(1P) \rightarrow\psi_{b}\psi_{b}`$ | 
| `Phys/Psi&P/Particles`          | `sel_psi_P`  | $`\chi_{b1}(1P) \rightarrow J/\psi p `$ | 
| `Phys/Psi&PP/Particles`         | `sel_psi_PP` | $`\chi_{b1}(1P) \rightarrow J/\psi p \bar{p}`$ | 

For all cases:

 *  $`B^+`$   is $`B^+    \rightarrow \psi K^+`$  and $B^+\rightarrow \psi K^+ K^- \K^+$, $B^+\rightarrow \psi K^+ \pi^+ \pi^-$, from [`WG_PSIX`](WG_PSIX.md) module 
 *  $`B^0`$   is $`B^0    \rightarrow \psi K^- \pi^+`$ from [`WG_PSIX`](WG_PSIX.md) module 
 *  $`B^0_s`$ is $`B^0_s  \rightarrow \psi K^- K^+`$   from [`WG_PSIX`](WG_PSIX.md) module 
 *  charm hadrons are reconstucted via `PromptCharm` module 
 *  $`\Upsilon(1S)`$ are reconstucted via  [`WG_UpsilonExotic`](WG_UpsilonExotic.md) module 
 *  $`\psi_{b}$ denotes detached $`J/\psi\rightarrow \mu+\mu-`$ and $`\psi(2S)\rightarrow \mu+\mu-`$ from [`WG_PSIX`](WG_PSIX.md) module 
 *  $`(\mu\mu)`$ denotes prompt dimuon  
 *  $`\mu_{W}`$ denotes high-pt muon from $`W`$~decay




