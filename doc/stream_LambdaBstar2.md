# `LAMBDABSTAR2` stream 

`LAMBDABSTAR2.MDST` is created from input `BCOMPLETEEVENT.DST` using 
```
gaudirun.py LambdabStar2.py ./DataType-XXX.py input_data
```

The lines from [`LambdaBstar2`](LambdaBstar2.md) module :

   1. `Phys/SelPsiPKForPsiX/Particles`
   1. `Phys/LambdabStar2/Particles`
