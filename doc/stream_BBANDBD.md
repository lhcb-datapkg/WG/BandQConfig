# `BBANDBD` stream 

`BBANDBD.MDST` stream is created from input `BHADRONCOMPLETEEVENTS.DST` using 
```
gaudirun.py ./BandB.py ./DataType-XXX.py input_data
```

Input stripping lines (common suffix is `Beauty2CharmLine`)
 - `Lb2LcPiNoIPLc2PKPi`
 - `Lb2LcPiPiPiLc2PKPi`      
 - `Lb2XicPiNoIPXic2PKPi`  
 - `Xib2Xic0PiNoIPXic02PKKPi` 
 - `Xib2Xic0PiPiPiXic02PKKPi` 
 - `B2D0PiD2HH`              
 - `B2D0PiD2HHHHTIGHT`     
 - `B2D0PiPiPiD2HHTIGHT` 
 - `B02DPiD2HHH`        
 - `B02DPiPiPiD2HHHPID`

The stream contains two lines 

      1. `Phys/Hb&Hb/Particles`                    
      1. `Phys/Hb&Hc/Particles`                 
