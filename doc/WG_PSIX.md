# `WG_PSIX` module  [`WG_PSIX.py`](../scripts/WG_PSIX.py)

The module  is used for processing of `DIMUON.DST` stripping  stream 

| Output location                         | Selection       |   Content                                       |
|:----------------------------------------|:----------------|:------------------------------------------------|
| `Phys/SelPsiKForPsiX/Particles`         | `psi_k`         | $`B^+\rightarrow \psi K^+`$                     |
| `Phys/SelPsiPiForPsiX/Particles`        | `psi_pi`        | $`B^+\rightarrow \psi \pi^+`$                   |
| `Phys/SelPsi2KForPsiX/Particles`        | `psi_2K`        | $`B^0_{(s)}\rightarrow \psi K^+K^-`$            |
| `Phys/SelPsi2KPiForPsiX/Particles`      | `psi_2Kpi`      | $`B^0_{(s)}\rightarrow \psi K^-\pi^+`$          |
| `Phys/SelPsi2PiForPsiX/Particles`       | `psi_2pi`       | $`B^0_{(s)}\rightarrow \psi \pi^-\pi^+`$        |
| `Phys/SelPsi3KForPsiX/Particles`        | `psi_3K`        | $`B^+\rightarrow \psi K^+K^+K^-`$               |
| `Phys/SelPsi3KPiForPsiX/Particles`      | `psi_3Kpi`      | $`B^+\rightarrow \psi K^+\pi^+\pi^-`$, $`B^+\rightarrow \psi K^+K^-\pi^+`$,  $`B^+\rightarrow \psi K^-\pi^+\pi^+`$ | 
| `Phys/SelPsi3PiForPsiX/Particles`       | `psi_3pi`       | $`B^+\rightarrow \psi \pi^+\pi^+\pi^-`$ | 
| `Phys/SelPsi4kForPsiX/Particles`        | `psi_4K`        | $`B^0_s\rightarrow \psi K^+K^+K^-K^-`$  |
| `Phys/SelPsi4KPiForPsiX/Particles`      | `psi_4Kpi`      | $`B^0\rightarrow \psi K^+\pi^+\pi^-\pi^-`$, $`B^0\rightarrow \psi K^+\pi^+K^-\pi^-`$, $`B^0\rightarrow \psi K^+K^+K^-\pi^-`$ | 
| `Phys/SelPsi4PiForPsiX/Particles`       | `psi_4pi`       | $`B^0_s\rightarrow \psi \pi^+\pi^+\pi^-\pi^-`$  |
| `Phys/SelPsi5KForPsiX/Particles`        | `psi_5K`        | $`B^+_c\rightarrow \psi K^+K^+K^+K^-K^-`$       |
| `Phys/SelPsi5KPiForPsiX/Particles`      | `psi_5Kpi`      | $`B^+\rightarrow \psi K^+\pi^+\pi^+\pi^-\pi-`$, $`B^+\rightarrow \psi K^+\pi^+\pi^+K^-\pi-`$, $`B^+\rightarrow \psi K^+K^+\pi^+K^-\pi-`$, $`B^+\rightarrow \psi K^+\pi^+\pi^+K^-K-`$ | 
| `Phys/SelPsi5PiForPsiX/Particles`       | `psi_5pi`       | $`B^+\rightarrow \psi 5\pi^{\pm}`$ | 
| `Phys/SelPsi6KPiForPsiX/Particles`      | `psi_6Kpi`      | $`B^0\rightarrow \psi K^+5\pi`$, $`B^+\rightarrow \psi K^+K-^+4\pi`$, $`B^+\rightarrow \psi K^+K^+K^-3\pi`$, $`B^+\rightarrow \psi K^+K^+K^-K^-2\pi`$ | 
| `Phys/SelPsi6PiForPsiX/Particles`       | `psi_6pi`       | $`B^0_s\rightarrow \psi 6\pi`$  |
| `Phys/SelPsi7KPiForPsiX/Particles`      | `psi_7Kpi`      | $`B^+c\rightarrow \psi nK m\pi`$ , n=1,6; n + m = 7 | 
| `Phys/SelPsi7PiForPsiX/Particles`       | `psi_7pi`       | $`B^+\rightarrow \psi 7\pi^{\pm}`$ | 
| `Phys/SelPsiPKForPsiX/Particles`        | `psi_pK`        | $`\Lambda_b \rightarrow \psi pK^-`$ | 
| `Phys/SelPsiPPiForPsiX/Particles`       | `psi_ppi`       | $`\Lambda_b \rightarrow \psi p \pi^-`$ | 
| `Phys/SelPsiPKpipiForPsiX/Particles`    | `psi_pKpipi`    | $`\Lambda_b \rightarrow \psi pK^- \pi^+\pi^-`$ | 
| `Phys/SelPsiPpipipiForPsiX/Particles`   | `psi_ppipipi`   | $`\Lambda_b \rightarrow \psi p \pi^+ \pi^-\pi^-`$ | 
| `Phys/SelPsiPKKForPsiX/Particles`       | `psi_pKK`       | $`\Xi_b^- \rightarrow \psi pK^-K^-`$ ,  (ws:$`\Xi_b^- \rightarrow \psi pK^-K^+`$) | 
| `Phys/SelPsiPKKpiForPsiX/Particles`     | `psi_pKKpi`     | $`\Xi_b^-\rightarrow \psi p K^-K^-\pi+`$, (ws:$`\Xi_b^-\rightarrow \psi p K^-K^+\pi+`$, $`\Xi_b^-\rightarrow \psi p K^-K^+\pi-`$) |      
| `Phys/SelPsiPKKKpiForPsiX/Particles`    | `psi_pKKKpi`    | $`\Omega_b^-\rightarrow \psi p K^-K^-K^-\pi+`$, (ws:$`\Omega_b^-\rightarrow \psi p K^-K^-K^+\pi-`$$) |      
| `Phys/SelPsiPPForPsiX/Particles`        | `psi_pp`        | $`B^0_s \rightarrow \psi p\bar{p}`$  |  
| `Phys/SelPsiPPpiForPsiX/Particles`      | `psi_pppi`      | $`B^+ \rightarrow \psi p\bar{p} \pi^+`$  |  
| `Phys/SelPsiPPKForPsiX/Particles`       | `psi_ppK`       | $`B^+ \rightarrow \psi p\bar{p} K^+`$  |  
| `Phys/SelPsiPPpipiForPsiX/Particles`    | `psi_pppipi`    | $`B^0_s \rightarrow \psi p\bar{p} \pi^+\pi^-`$  |  
| `Phys/SelPsiPPKpipiForPsiX/Particles`   | `psi_ppKpipi`   | $`B^+ \rightarrow \psi p\bar{p}  K^+\pi+\pi^-`$  |  
| `Phys/SelPsiPPpipipiForPsiX/Particles`  | `psi_pppipipi`  | $`B^+ \rightarrow \psi p\bar{p} \pi^+\pi^+\pi^-`$  |  
| `Phys/SelHbPsiPPKForPsiX/Particles`     | `Hb_psi_ppK`    | $`\Sigma_b^+ \rightarrow \psi p p K^-`$ |  
| `Phys/SelHbPsiPPKPiForPsiX/Particles`   | `Hb_psi_ppKpi`  | $`\Sigma_b^0 \rightarrow \psi p p K^- \pi^-`$ | 
| `Phys/SelPsiKS0ForPsiX/Particles`       | `psi_k0s`       | $`B^0 \rightarrow  \psi K_S^0`$ | 
| `Phys/SelPsiKS0PiForPsiX/Particles`     | `psi_k0spi`     | $`B^+ \rightarrow  \psi K_S^0 \pi^+`$ | 
| `Phys/SelPsiKS02PiForPsiX/Particles`    | `psi_k0s2pi`    | $`B^0 \rightarrow  \psi K_S^0 \pi^+\pi^-`$ | 
| `Phys/SelPsiKS03PiForPsiX/Particles`    | `psi_k0s3pi`    | $`B^+ \rightarrow  \psi K_S^0 \pi^+\pi^+\pi^-`$ | 

For all cases :
  * $`\psi`$ denotes $`J/\psi\rightarrow \mu^+\mu^-`$  or  $`\psi(2S)\rightarrow\mu^+\mu^-`$
  * $`K_S^0`$ denotes both LL and DD  