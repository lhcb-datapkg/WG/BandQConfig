# `UPS_turbo` module  [`UPS_turbo.py`](../scripts/UPS_turbo.py)

The module  is used for processing of `LEPTONS.MDST` turbo stream 


The stream contains:

| Output location                         |   Content		                 	             |
|:--------  ------------------------------|:---------------------------------------------------------|	 
| `Phys/Y&pi/Particles`                   | $`X\rightarrow \Upsilon(1S)\pi^+`$                       | 
| `Phys/Y&KK/Particles`                   | $`X\rghtarrow \Upsilon(1S) (\phi \rigtharrow K^+ K^-) `$ | 
| `Phys/UPSILON/Particles`                |                                                          | 
  
