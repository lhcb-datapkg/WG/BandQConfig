# `LAMBDABSTAR` stream 

`LAMBDABSTAR.MDST` is created from input `BCOMPLETEEVENT.DST` using 
```
gaudirun.py LambdabStar.py ./DataType-XXX.py input_data
```

The lines from [`LambdaBstar`](LambdaBstar.md) module :

  1. `Phys/LambdabWithBPV/Particles`
  1. `Phys/LambdabStar/Particles`

