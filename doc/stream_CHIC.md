# `CHIC` stream 

`CHIC.MDST` is created from input `DIMUON.DST` using 
```
gaudirun.py ./BandQ.py ./DataType-XXX.py input_data
```

The stream contains:

  *  All lines  from [`WG_CHIC`](WG_CHIC.md) module 
