# `WG_PSIX` module  [`WG_PSIX.py`](../scripts/WG_PSIX.py)

The module  is used for processing of `DIMUON.DST` stripping stream 

| Output location                         | Selection       |   Content                                       |
|:----------------------------------------|:----------------|:------------------------------------------------|
| `Phys/ChicToJpsiMiMu/Particles`         | `wg_chic`       | $`\chi_{c1,2}\rightarrow (J/\psi\rightarrow \mu^+\mu^-) \mu^+ \mu^-`$ | 
| `Phys/Psi2SToJpsiPiPi/Particles`        | `wg_psi2s`      | $`\psi(2S)\rightarrow (J/\psi\rightarrow \mu^+\mu^-) \pi^+ \pi^-`$ | 
