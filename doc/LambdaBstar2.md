# `LambdaBstar2` module  [`LambdaBstar2.py`](../scripts/LambdaBstar2.py)

The module  is used for processing of `DUIMUON.DST` stripping  stream 

| Output location                         | Selection       |   Content                                       |
|:----------------------------------------|:----------------|:------------------------------------------------|
| `Phys/SelPsiPKForPsiX/Particles` | | $`\Lambda_b^0 \rigtharrow ( J/\psi \rigtharrow \mu^+ \mu^- )p K^-`$ | 
| `Phys/LambdabStar2/Particles`    | | $`\Lambda_b(5929)^0  \rightarrow \Lambda_b^0 \pi^+ \pi^-`$, $`\Lambda_b(5929)^0  \rightarrow \Lambda_b^0 \pi^+ \pi^+`$, $`\Lambda_b(5929)^0  \rightarrow \Lambda_b^0 \pi^- \pi^-`$ | 

$`\Lambda_b^- \rightarrow J/\psi p K^-`$ is selected  by  [`WG_PSI`](WG_PSIX.md) module 
