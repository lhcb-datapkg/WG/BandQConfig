# `WG_PSIX0` module  [`WG_PSIX0.py`](../scripts/WG_PSIX0.py)

The module  is used for processing of `DIMUON.DST` stripping  stream 

| Output location                         | Selection       |   Content                                       |
|:----------------------------------------|:----------------|:------------------------------------------------|
| `Phys/SelB2PsiEtaForPsiX0/Particles`             | `b2eta`       | $`B^0_s \rightarrow \psi  \eta`$           | 
| `Phys/SelB2PsiEtaPrimeForPsiX0/Particles`        | `b2etap`      | $`B^0_s \rightarrow \psi  \eta^{\prime}`$  | 
| `Phys/SelB2PsiOmegaForPsiX0/Particles`           | `b2omega`     | $`B^0   \rightarrow \psi  \omega`$         | 
| `Phys/SelB2PsiKEtaForPsiX0/Particles`            | `b2Keta`      | $`B^+   \rightarrow \psi  K^+ \eta`$            |
| `Phys/SelB2PsiKEtaPrimeForPsiX0/Particles`       | `b2Ketap`     | $`B^+   \rightarrow \psi  K^+ \eta^{\prime}`$   |
| `Phys/SelB2PsiKOmegaForPsiX0/Particles`          | `b2Komega`    | $`B^+   \rightarrow \psi  K^+ \omega`$          |
| `Phys/SelB2PsiKKEtaForPsiX0/Particles`           | `b2KKeta`     | $`B^0_s \rightarrow \psi  K^+ K^- \eta`$          | 
| `Phys/SelB2PsiKKEtaPrimeForPsiX0/Particles`      | `b2KKetap`    | $`B^0_s \rightarrow \psi  K^+ K^- \eta^{\prime}`$ | 
| `Phys/SelB2PsiKKOmegaForPsiX0/Particles`         | `b2KKomega`   | $`B^0_s \rightarrow \psi  K^+ K^- \omega`$        | 
| `Phys/SelB2PsiPiEtaForPsiX0/Particles`           | `b2pieta`     | $`B^+   \rightarrow \psi  \pi^+ \eta`$            |
| `Phys/SelB2PsiPiEtaPrimeForPsiX0/Particles`      | `b2pietap`    | $`B^+   \rightarrow \psi  \pi^+ \eta^{\prime}`$   |
| `Phys/SelB2PsiPiOmegaForPsiX0/Particles`         | `b2piomega`   | $`B^+   \rightarrow \psi  \pi^+ \omega`$          |
| `Phys/SelB2PsiPiPiEtaForPsiX0/Particles`         | `b2pipieta`   | $`B^0_s \rightarrow \psi  \pi^+ \pi^- \eta`$          | 
| `Phys/SelB2PsiPiPiEtaPrimeForPsiX0/Particles`    | `b2pipietap`  | $`B^0_s \rightarrow \psi  \pi^+ \pi^- \eta^{\prime}`$ | 
| `Phys/SelB2PsiPiPiOmegaForPsiX0/Particles`       | `b2pipiomega` | $`B^0_s \rightarrow \psi  \pi^+ \pi^- \omega`$        | 
| `Phys/SelB2PsiKPiEtaForPsiX0/Particles`          | `b2Kpieta`    | $`B^0   \rightarrow \psi   K^+ \pi^- \eta`$          |  
| `Phys/SelB2PsiKPiEtaPrimeForPsiX0/Particles`     | `b2Kpietap`   | $`B^0   \rightarrow \psi   K^+ \pi^- \eta^{\prime}`$ |  
| `Phys/SelB2PsiKPiOmegaForPsiX0/Particles`        | `b2Kpiomega`  | $`B^0   \rightarrow \psi   K^+ \pi^- \omega`$        |  
| `Phys/SelBc2PsiRhoForPsiX0/Particles`            | `bc2rho`      | $`B_c^+ \rightarrow \psi   \pi^+ \pi^0`$ | 
| `Phys/SelBu2PsiKstarForPsiX0/Particles`          | `bu2Kstar`    | $`B^+   \rightarrow \psi    K^+  \pi^0`$ | 
| `Phys/SelB2ChicKForPsiX0/Particles`              | `b2chicK`     | $`B^+   \rightarrow \chi_{c1} K^+`$ | 
| `Phys/SelB2Chic2KForPsiX0/Particles`             | `b2chicKK`    | $`B^0_s \rightarrow \chi_{c1} K^+ K^-`$ | 
| `Phys/SelB2Chic3KForPsiX0/Particles`             | `b2chicKKK`   | $`B^+   \rightarrow \chi_{c1} K^+ K^+ K^-`$ | 
| `Phys/SelB2Chic2KPiForPsiX0/Particles`           | `b2chic2Kpi`  | $`B^0 \rightarrow \chi_{c1} K^- \pi^+`$ | 
| `Phys/SelB2Chic3KPiForPsiX0/Particles`           | `b2chic3Kpi`  | $`B^+ \rightarrow \chi_{c1} K^+ \pi^+ \pi^-`$, $`B^+ \rightarrow \chi_{c1} K^+ K^- \pi^+`$, $`B^+ \rightarrow \chi_{c1} K^-  \pi^+ \pi^-`$ | 
| `Phys/SelB2Chic2PiForPsiX0/Particles`            | `b2chic2pi`   | $`B^0_s \rightarrow \chi_{c1} \pi^+ \pi^-`$ |   
| `Phys/SelB2Chic3PiForPsiX0/Particles`            | `b2chic3pi`   | $`B^+   \rightarrow \chi_{c1} \pi^+ \pi^+ \pi^-`$ | 
| `Phys/SelBc2ChicPiForPsiX0/Particles`            | `bc2chicpi`   | $`B_c^+ \rightarrow \chi_{c1} \pi^+`$ |    
| `Phys/SelLb2ChicPKForPsiX0/Particles`            | `lb2chicpK`   | $`\Lambda_b^0 \rightarrow \chi_{c1} p K^-`$ |           
| `Phys/SelLb2ChicPPiForPsiX0/Particles`           | `lb2chicppi`  | $`\Lambda_b^0 \rightarrow \chi_{c1} p \pi^-`$ |           
| `Phys/SelB2X3872KForPsiX0/Particles`             | `b2x3872K`    | $`B^+   \rightarrow ( X(3872) \rightarrow \psi \gamma ) K^+`$ | 
| `Phys/SelB2X3872KPiForPsiX0/Particles`           | `b2x3872Kpi`  | $`B^0   \rightarrow ( X(3872) \rightarrow \psi \gamma ) K^-\pi+`$ | 
| `Phys/SelLb2X3872PKForPsiX0/Particles`           | `lb2x3872pK`  | $`\Lambda_b^0 \rightarrow ( X(3872) \rightarrow \psi \gamma ) p K^-`$ | 

For all cases :
  * $`\psi`$ denotes $`J/\psi\rightarrow \mu^+\mu^-`$  or  $`\psi(2S)\rightarrow\mu^+\mu^-`$
  * $`\eta`$ is reconstructed as $`\eta \rightarrow \gamma\gamma`$  and  $`\eta \rightarrow \pi^+\pi^-\pi0`$    
  * $`\eta^{\prime}`$ is reconstructed as $`\eta^{\prime} \rightarrow \pi^+\pi^-\gamma`$  and  $`\eta^{\prime} \rightarrow \pi^+\pi^- (\eta\rightarrow\gamma\gamma) `$
  * $`\omega`$ is reconstructed as $`\omega \rightarrow \pi^+\pi^-\pi^0`$ 
  * $`\chi_{c1}`$ is reconstructed as $`\chi_{c1}\rightarrow J/\psi \gamma`$ and  $`\chi_{c1}\rightarrow J/\psi \mu^+ \mu^-`$  
