# `WG_UpsilonExotic` module [`WG_Combinations.py`](../scripts/WG_UpsilonExotic.py)

The module  is used for processing of `DIMUON.DST` stripping  stream 

| Output location                         | Selection       |   Content                                       |
|:----------------------------------------|:----------------|:------------------------------------------------|
| `Phys/SelUpsilonForUpsilonExotica/Particles`      | `upsilons`           | $`\Upsilon(1S)\rightarrow \mu+\mu-`$ | 
| `Phys/SelY&KKForUpsilonExotica/Particles`         | `upslion_phi`        | $`\Upsilon(4S)\rightarrow \Upsilon(1S) \left(K+ K-\right)_{m_{KK}<1.050GeV/c^2}`$ | 
| `Phys/SelY&PiPiForUpsilonExotica/Particles`       | `upslion_rho`        | $`\Upsilon(4S)\rightarrow \Upsilon(1S) \left(\pi+ \pi-\right)_{m_{\pi\pi}<1.050GeV/c^2}`$ | 
| `Phys/SelY&etaForUpsilonExotica/Particles`        | `upsilon_eta`        | $`\chi_{b0}   \rightarrow \Upsilon(1S) \eta`$ | 
| `Phys/SelY&eta_primeForUpsilonExotica/Particles`  | `upsilon_eta_prime`  | $`\chi_{b0}   \rightarrow \Upsilon(1S) \eta^{\prime}`$ | 
| `Phys/SelY&omegaForUpsilonExotica/Particles`      | `upsilon_omega`      | $`\chi_{b0}   \rightarrow \Upsilon(1S) \omega`$ |  
| `Phys/SelY&pForUpsilonExotica/Particles`          | `upsilon_proton`     | $`\Upsilon(4S)\rightarrow \Upsilon(1S) p`$ | 
| `Phys/SelY&ppForUpsilonExotica/Particles`         | `upsilon_pp`         | $`\Upsilon(4S)\rightarrow \Upsilon(1S) p\bar{p}`$ | 
| `Phys/SelY&pKForUpsilonExotica/Particles`         | `upsilon_pK`         | $`\Upsilon(4S)\rightarrow \Upsilon(1S) pK^-`$ | 
| `Phys/SelY&mumuForUpsilonExotica/Particles`       | `upsilon_2mu`        | $`\Upsilon(4S)\rightarrow \Upsilon(1S) \mu^+\mu^-`$,   $`\Upsilon(4S)\rightarrow \Upsilon(1S) \mu^+\mu^+`$ |

For all cases :

 *  $`Upsilon(1S)`$ is  a heavy dimuon with $`m(\mu^+\mu^-)>6GeV/c^2`$
 *  $`\eta`$ is $`\eta\rightarrow \gamma\gamma`$ and $`\eta\rightarrow \pi^+\pi^-\pi0`$ 
 *  $`\eta^{\prime}`$ is $`\eta^{\prime}\rightarrow \pi^+\pi^-\gamma`$ and $`\eta^{\prime}\rightarrow \pi^+\pi^- ( \eta\rightarrow \gamma\gamma)`$ 
 *  $`\omega`$ is $`\omega\rightarrow \pi^+\pi^-\pi^0`$ 




