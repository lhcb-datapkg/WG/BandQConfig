#!/usr/bin/env gaudirun.py
# -*- coding: utf-8 -*-
# =============================================================================
## @file UPS_turbo.py
#  Script to produce Y from LEPTONS TURBO stream 
#  @attention DataType and CondDB are not specified!
#  Usage:
#  @code
#  gaudirun.py $BANDQCONFIGROOT/scripts/DataType-2016-Turbo03a.py $BANDQCONFIGROOT/scripts/UPS_turbo.py the_file_with_input_data.py
#  @endcode
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2017-07-10
# =============================================================================
""" Script to produce Y uDST from TURBO stream

Usage:

> gaudirun.py $BANDQCONFIGROOT/scripts/DataType-2018-Turbo05.py $BANDQCONFIGROOT/scripts/UPS_turbo.py the_file_with_input_data.py

"""
# =============================================================================
__author__ = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__   = "2021-02-16"
# =============================================================================
from Configurables import LHCbApp
LHCbApp().XMLSummary = 'summary.xml'

ups_line = 'Hlt2DiMuonUpsilonTurbo'

from PhysConf.Selections import AutomaticData
upsilons = AutomaticData( ups_line + '/Particles' )


# from PhysConf.Selections import PrintSelection
# upsilons = PrintSelection ( upsilons ) 

## from GaudiConfUtils.ConfigurableGenerators import SubstitutePID
## from PhysConf.Selections                   import SimpleSelection
## upsilons = SimpleSelection(
##     'Upsilon'     ,
##     SubstitutePID ,
##     upsilons      ,
##     Code =         " DECTREE(' Meson  -> mu+ mu-') & ( 7 * GeV < M ) ",
##     Substitutions = { 'Meson -> mu+ mu-' : "Upsilon(1S)" }
##     )

## from PhysConf.Selections import FilterSelection
## upsilons = FilterSelection ( "Y"           ,
##                              upsilons      , 
##                              Code = """
##                              ( M > 7 * GeV ) & 
##                              DECTREE   ('Meson -> mu+ mu-'  )                      &
##                              ( MINTREE ( 'mu+' == ABSID , PT ) > 1 * GeV         ) &
##                              ( MAXTREE ( ISBASIC & HASTRACK , TRCHI2DOF ) < 4    ) & 
##                              ( MINTREE ( ISBASIC & HASTRACK , CLONEDIST ) > 5000 ) & 
##                              ( VFASPF  ( VPCHI2 ) > 0.5/100 ) 
##                              """ , 
##                              CloneFilteredParticles  = True   ## NB VITAL FOR MURBO.MDST 
##                              )
# 
from StandardParticles   import StdAllNoPIDsPions as pions
from StandardParticles   import StdAllNoPIDsKaons as kaons
from PhysConf.Selections import RebuildSelection 

pions = RebuildSelection ( pions )
kaons = RebuildSelection ( kaons )

from PhysConf.Selections import FilterSelection
pions = FilterSelection  ( "PIONS" ,
                           pions    ,
                           Code = "(TRCHI2DOF < 3) & ( PROBNNpi > 0.1 )" ,
                           CloneFilteredParticles  = True   ) ## NB VITAL FOR MURBO.MDST 

kaons = FilterSelection  ( "KAONS" ,
                           kaons   ,
                           Code = "(TRCHI2DOF < 3) & ( PROBNNK > 0.2  )" ,
                           CloneFilteredParticles  = True  ) ## NB VITAL FOR MURBO.MDST 

from PhysConf.Selections import CombineSelection
ups_pi = CombineSelection      ( "Y&pi" , [ upsilons , pions] ,
                                 DecayDescriptor  = " [ B_c+ -> J/psi(1S) pi+]cc" ,
                                 DaughtersCuts    = { 'J/psi(1S)' : "in_range ( 7 * GeV , M ,  12 * GeV )" } ,
                                 CombinationCut   = """
                                 ( AM                        < 15   * GeV ) &
                                 ( ( AM - AM1 - 139 * MeV )  < 3    * GeV ) &
                                 ( ACHI2DOCA(1,2)            < 12         ) 
                                 """ ,
                                 MotherCut      = " CHI2VXNDOF < 10 " ,
                                 ReFitPVs       = True
                                 ) 

from PhysConf.Selections import Combine3BodySelection
ups_KK = Combine3BodySelection ( "Y&KK" , [ upsilons , kaons ] ,                            
                                 ## algorithm properties 
                                 DecayDescriptor  = " Upsilon(4S) -> J/psi(1S) K+ K-" ,
                                 DaughtersCuts    = { 'J/psi(1S)' : "in_range ( 7 * GeV , M ,  15 * GeV ) " } ,
                                 Combination12Cut = """
                                 ( AM < 51 * GeV       ) &
                                 ( ACHI2DOCA(1,2) < 16 )
                                 """ ,
                                 CombinationCut = """
                                 ( AM                  < 45   * GeV ) &
                                 ( AM23                < 1075 * MeV ) & 
                                 ( ACHI2DOCA(1,3)      < 16         ) &
                                 ( ACHI2DOCA(2,3)      < 16         )
                                 """ ,
                                 MotherCut      = " CHI2VXNDOF < 10" ,
                                 ReFitPVs       = True                
                                 )

from PhysConf.Filters import LoKi_Filters
fltrs = LoKi_Filters (
    HLT2_Code = """
    HLT_PASS_RE ( 'Hlt2DiMuonUpsilonTurboDecision' )
    """
    )

from Configurables import DaVinci
davinci = DaVinci (
    EventPreFilters = fltrs.filters ('Filters') ,
    InputType       = 'MDST'              ,
    Simulation      = False,
    Lumi            = True,
    Turbo           = True,
    EvtMax          = -1 ##TODO
    )

assert davinci.isPropertySet('DataType'), 'DataType is not defined yet!'
data_type = davinci.DataType

assert data_type in  ( '2017' , '2018' ),\
       "Invalid Datatype:%s" % data_type 

if   data_type in ( '2015' , '2016' ) : RootInTES = '/Event/Leptons'
elif data_type in ( '2017' , '2018' ) : RootInTES = '/Event/Leptons/Turbo'  


davinci.RootInTES = RootInTES

from Configurables import CondDB 
CondDB( LatestGlobalTagByDataType = data_type )


## II.1 specific for persist reco
## from Configurables import DstConf, TurboConf
## DstConf   () .Turbo       = True
## TurboConf () .PersistReco = True
## TurboConf () .DataType    = data_type 


davinci.UserAlgorithms.append ( ups_pi )
davinci.UserAlgorithms.append ( ups_KK )


# ============================================================================
## VI. Configure uDST writer/copier  
# ============================================================================
## due to technical reasons for production it needs to be done late...

def _configure_output_ () :

    outputfile = 'ups_turbo.mdst'
    ##
    from Gaudi.Configuration import allConfigurables
    fakew  = allConfigurables.get('MyDSTWriter',None)
    if fakew and 'Sel' != fakew.OutputFileSuffix : 
        outputfile = fakew.OutputFileSuffix + '.' + outputfile 
        
    ##
    from GaudiConf import IOHelper
    ioh    = IOHelper        ( 'ROOT'     , 'ROOT' ) 
    oalgs  = ioh.outputAlgs  ( outputfile , 'InputCopyStream/COPYTURBO' )
    
    writer = oalgs[0]
    writer.AcceptAlgs = [ ups_pi .algorithm().name () ,
                          ups_KK .algorithm().name () ]
    
    from Configurables import DaVinci
    rit = DaVinci().RootInTES
    dt  = DaVinci().DataType 
    
    items = [ 'Phys/Y&pi#99'  ,
              'Phys/Y&KK#99'  ,
              'Phys/PIONS#99' ,
              'Phys/KAONS#99' ,
              ]        
    
    writer.OptItemList   += [  '%s/%s' % ( rit , i )  for i in  items ] 

    ## writer.OutputLevel = 2

    from Configurables import GaudiSequencer
    oseq  = GaudiSequencer ( 'OUTPUTCOPY', Members = oalgs ) 
    
    from Configurables import ApplicationMgr
    AM    = ApplicationMgr ()
    
    if not AM.OutStream :
        AM.OutStream =[]
        
        AM.OutStream.append ( oseq )
        
        
from Gaudi.Configuration import appendPostConfigAction
appendPostConfigAction ( _configure_output_ )



# =============================================================================
##                                                                      The END 
# =============================================================================
