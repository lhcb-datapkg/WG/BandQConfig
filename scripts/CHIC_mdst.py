#!/usr/bin/env gaudirun.py
# -*- coding: utf-8 -*-
# =============================================================================
## @file CHIC_mdst.py
#  Script to produde chi_c -> J/psi mu+ mu- uDST from FULL stream
#  @attention DataType and CondDB are not specified!
#  Usage:
#  @code
#  gaudirun.py $BANDQCONFIGROOT/scripts/CHIC_mdst.py $BANDQCONFIGROOT/scripts/DataType-2012.py  the_file_with_input_data.py
#  @endcode
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2017-07-10
# =============================================================================
""" Script to produde chi_c -> J/psi mu+ mu- uDST from TURBO stream

Usage:

> gaudirun.py $BANDQCONFIGROOT/scripts/CHIC_mdst.py $BANDQCONFIGROOT/scripts/DataType-2012.py  the_file_with_input_data.py

"""
# =============================================================================
## logging
# =============================================================================
try : 
    from AnalysisPython.Logger import getLogger
    _name = __name__
    if 0 == _name.find ( '__'   ) : _name = 'WG/B&Q'
    logger = getLogger (  _name )
except:
    from Gaudi.Configuration import log as logger

# =============================================================================
## I. Build the selection
# =============================================================================

try :
    from WG_CHIC import  wg_chic, wg_psi2s 
except ImportError :
    import sys,  os 
    opath = [ os.path.normpath ( os.path.abspath ( i ) ) for i in  sys.path ]
    npaths = set() 
    for s in sys.argv[1:] :
        if os.path.exists ( s ) :
            ap      = os.path.abspath  ( s  )
            np      = os.path.normpath ( ap )        
            dirname = os.path.dirname  ( np ) 
            if dirname not in opath : npaths.add ( dirname )
            a , b = os.path.split ( dirname )
            dirname = os.path.join ( a , 'scripts' )
            dirname = os.path.abspath  ( dirname )
            dirname = os.path.normpath ( dirname )
            if dirname not in opath : npaths.add ( dirname )
                                   
    logger.info ( 'Path is exended with: %s' % list ( npaths ) )
    for n in npaths :
        if n not in sys.path :
            sys.path = [ n ] + sys.path
 
    from WG_CHIC import  wg_chic, wg_psi2s 
    sys.path = opath


from PhysConf.Selections import SelectionSequence
from PhysConf.Selections import SelectionSequence, MultiSelectionSequence 
the_seq = MultiSelectionSequence (
    'CHIC' ,
    Sequences = [
    SelectionSequence ( 'CHIC2PSIMUMU' , wg_chic  ) ,
    SelectionSequence ( 'PSI2SPSIPIPI' , wg_psi2s ) ] 
    )

# =============================================================================
## II. Configure muDST writer 
# =============================================================================

from DSTWriters.Configuration import ( SelDSTWriter            ,
                                       stripMicroDSTStreamConf ,
                                       stripMicroDSTElements   )

# Configuration of SelDSTWriter
SelDSTWriterConf     = { 'default' : stripMicroDSTStreamConf ( pack = False ) }
SelDSTWriterElements = { 'default' : stripMicroDSTElements   ( pack = False , refit = True ) }

uDstWriter = SelDSTWriter(
    "MyDSTWriter"                               ,
    StreamConf         =  SelDSTWriterConf      ,
    MicroDSTElements   =  SelDSTWriterElements  ,
    OutputFileSuffix   = 'BandQ'                ,  ## output PRE-fix! 
    SelectionSequences = [ the_seq ]
    )


# ============================================================================
## IV. Configure DaVinci
# ============================================================================

from Configurables import LHCbApp
LHCbApp().XMLSummary = 'summary.xml'

from PhysConf.Filters import LoKi_Filters
fltrs   = LoKi_Filters (
    STRIP_Code = "HLT_PASS_RE('.*DiMuonJpsi2MuMuTOS.*')"
    ) 

from Configurables import DaVinci
davinci = DaVinci (
    #
    EventPreFilters = fltrs.filters('Fltrs') ,
    InputType       = 'DST'            ,
    PrintFreq       = 5000             ,
    EvtMax          = -1               ,
    #
    Lumi            = True ,
    ##
    UserAlgorithms  = [ uDstWriter.sequence() ] 
    )

# =============================================================================
##                                                                      The END 
# =============================================================================
