#!/usr/bin/env gaudirun.py 
# =============================================================================
# @file
#
# WG-production for
#
#  - B -> psi(') n*pi m*K     decays, 1<=(n+m)<= 6 
#  - B -> psi(') eta('),omega decays
#  - B -> psi(') K(K) eta('),omega decays
#  - B  -> psi C                                      (included into X )
#  - B+ -> psi (K*+ -> K+ pi0 ) for photon efficiency (included into X0) 
#  - B+ -> psi pp H
#  - B -> psi(') KS0 l*pi decays, 0 <=l<= 3
#  - Upsilon -> mumu
#  - chi_b   -> ( Upsilon gamma ) 
#
# Input :  DIMUON.DST
#
# Outputs :
#
#  - BandQ.PSIX.mdst
#  - BandQ.PSIX0.mdst
#  - BandQ.BOTTOM.mdst
#
# @author Vanya BELYAEV Ivan.Belyaev@itep.ru
# @date   2012-08-17
#
# @attention DataType and CondDB are not specified!
#
# For 2k+11-data:
#
# @code
#
#  gaudirun.py  $BANDQCONFIGROOT/scripts/BandQ.py $BANDQCONFIGROOT/scripts/DataType-2011.py  the_file_with_input_data.py
#
# @endcode
#
# For 2k+12-data:
#
# @code
# 
# gaudirun.py  $BANDQCONFIGROOT/scripts/BandQ.py $BANDQCONFIGROOT/scripts/DataType-2012.py  the_file_with_input_data.py
#
# @endcode
# =============================================================================
"""WG-production for

  - B  -> psi(') n*pi m*K     decays, 1<=(n+m)<= 6 
  - B  -> psi(') eta('),omega decays
  - B  -> psi(') K(K) eta(') , omega decays
  - B  -> psi C                                      (included into X )
  - B+ -> psi (K*+ -> K+ pi0 ) for photon efficiency (included into X0) 
  - B+ -> psi pp H
  - B -> psi(') KS0 l*pi decays, 0 <=l<= 3

  - Upsilon -> mumu
  - chi_b   -> ( Upsilon gamma ) 

Input :  DIMUON.DST

Outputs :

  - BandQ.PSIX.mdst
  - BandQ.PSIX0.mdst
  - BandQ.BOTTOM.mdst
    
Attention: DataType and CondDB are not specified!

> gaudirun.py  BandQ.py $BANDQCONFIGROOT/scripts/DataType-2011.py   data.py
> gaudirun.py  BandQ.py $BANDQCONFIGROOT/scripts/DataType-2012.py   data.py
   
"""
# =============================================================================
__author__   = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__     = "2012-12-18"
__version__  = "$Revision: 150278 $"
# =============================================================================
## logging
# =============================================================================
try : # =======================================================================
    # =========================================================================
    from AnalysisPython.Logger import getLogger
    _name = __name__
    if 0 == _name.find ( '__'   ) : _name = 'WG/B&Q'
    logger = getLogger (  _name )
    # =========================================================================
except: # =====================================================================
    # =========================================================================
    from Gaudi.Configuration import log as logger
    # =========================================================================
    
# =============================================================================
from GaudiKernel.SystemOfUnits import GeV 

## the_year = "2012"

import os
logger.info ('Current directory %s' % os.getcwd () )
# =============================================================================
# 0. import basic elements
# =============================================================================
# import StrippingSelections.StrippingBandQ.StrippingUpsilonExotic  as BOTTOM
# import StrippingSelections.StrippingBandQ.StrippingPsiX0          as PSIX0  
# import StrippingSelections.StrippingBandQ.StrippingPsiXForBandQ   as PSIX
# =============================================================================
try : # =======================================================================
    # =========================================================================
    import WG_UpsilonExotic  as     BOTTOM
    import WG_PsiX_Prompt    as     PSIX_PROMPT
    import WG_PsiX           as     PSIX
    import WG_PsiX0          as     PSIX0  
    import WG_Combinations   as     WGCOMB
    from   WG_CHIC           import wg_chic, wg_psimm 
    # =========================================================================
except ImportError : # ========================================================
    # =========================================================================
    logger.info ( 'make a try to modify the path' )
    import sys,  os 
    opath = [ os.path.normpath ( os.path.abspath ( i ) ) for i in  sys.path ]
    npaths = set() 
    for s in sys.argv[1:] :
        if os.path.exists ( s ) :
            ap      = os.path.abspath  ( s  )
            np      = os.path.normpath ( ap )        
            dirname = os.path.dirname  ( np ) 
            if dirname not in opath : npaths.add ( dirname )
            a , b = os.path.split ( dirname )
            dirname = os.path.join ( a , 'scripts' )
            dirname = os.path.abspath  ( dirname )
            dirname = os.path.normpath ( dirname )
            if dirname not in opath : npaths.add ( dirname )
                        
    logger.info ( 'Path is exended with: %s' % list ( npaths ) ) 
    for n in npaths :
        if n not in sys.path :
            sys.path = [ n ] + sys.path
            
    ## try to import them now
    import WG_UpsilonExotic  as BOTTOM
    import WG_PsiX_Prompt    as PSIX_PROMPT
    import WG_PsiX0          as PSIX0  
    import WG_PsiX           as PSIX
    import WG_Combinations   as WGCOMB
    from   WG_CHIC           import wg_chic, wg_psimm
    
    sys.path = opath

import StrippingSelections.StrippingCharm.StrippingPromptCharm    as PC  

# =============================================================================
# 1. 
# =============================================================================
#
## location of dimuon candidates in TES:
#
jpsi_name        = 'FullDSTDiMuonJpsi2MuMuDetachedLine'
psi2_name        = 'FullDSTDiMuonPsi2MuMuDetachedLine'
ups_name         = 'FullDSTDiMuonDiMuonHighMassLine'

jpsi_line        = '/Event/Dimuon/Phys/%s/Particles' % jpsi_name 
psi2s_line       = '/Event/Dimuon/Phys/%s/Particles' % psi2_name 
ups_line         = '/Event/Dimuon/Phys/%s/Particles' %  ups_name 


bottom      = BOTTOM.UpsilonExoticConf ( 'UpsilonExotica' ,
                                         config = { 'DIMUONLINE'  : ups_line                   } )
psix_prompt = PSIX_PROMPT.PsiXPromptConf ( 'PsiX_Prompt' , config = { 'DIMUONLINE'  : None  } )
psix0       = PSIX0.PsiX0Conf          ( 'PsiX0'          ,
                                         config = { 'DIMUONLINES' : [ jpsi_line , psi2s_line ] } )


# ==================
## v18r0
# ==================
pion_cut = """
( PT          > 150 * MeV ) & 
( CLONEDIST   > 5000      ) & 
( TRGHOSTPROB < 0.5       ) &
( TRCHI2DOF   < 4         ) & 
in_range ( 2          , ETA , 5         ) &
in_range ( 3.2 * GeV  , P   , 150 * GeV ) &
HASRICH                     &
( MIPCHI2DV()  > 4        )
"""
logger.info ( "PSIX: Use only J/psi, no psi(2S)!")
logger.info ( "PSIX: redefine pion cuts!!")
psix        = PSIX.PsiX_BQ_Conf (
    'PsiX'           ,
    config = { 'DIMUONLINES' : [ jpsi_line ]     ,
               'PionCut'     : pion_cut          ,
               'PionPIDCut'  : "PROBNNpi > 0.1 " }
)




# ==============================================================================
## high pt prompt J/psi line 
# ==============================================================================
jpsiPT_location  = 'FullDSTDiMuonJpsi2MuMuTOSLine' ## high pt prompt J/psi 
jpsiPT_line      = '/Event/Dimuon/Phys/%s/Particles' % jpsiPT_location 
from PhysSelPython.Wrappers import AutomaticData 
jpsi_PT_0          = AutomaticData ( jpsiPT_line ) 
# ==============================================================================\
# 
pc      = PC.StrippingPromptCharmConf('PromptCharm', {
'pT(D0)'           :  0.95 * GeV ,    ## pt-cut for  prompt   D0
'pT(D+)'           :  0.95 * GeV ,    ## pt-cut for  prompt   D+
'pT(Ds+)'          :  0.95 * GeV ,    ## pt-cut for  prompt   Ds+
'pT(Lc+)'          :  0.95 * GeV ,    ## pt-cut for  prompt   Lc+
} )

prompt  = pc.PromptCharm ()  ## prompt charm 
w       = pc.W           ()  ## W+/- 
dimu_pr = pc.DiMuon      ()  ## prompt dimuons 
beauty  = psix.beauty    ()

##  various interesting combinations 
comb     = WGCOMB.Combine( bottom , psix , pc )  
             
# ==============================================================================
## B&W 
# ==============================================================================
## B&B
# ==============================================================================
## B&C
# ==============================================================================
## B&2mu
# ==============================================================================

# =============================================================================
## Y + prompt charm 
# =============================================================================
## Y + prompt J/psi 
# =============================================================================
## Y + detached J/psi 
# =============================================================================
## Y + Y
# =============================================================================
## pair of detached J/psi 
# =============================================================================
## Y -> J/psi p+ p~-
# =============================================================================

from PhysSelPython.Wrappers import MultiSelectionSequence
from PhysSelPython.Wrappers import      SelectionSequence

 
# =============================================================================
## PSIX0 stream 
# =============================================================================
psix0_seq = MultiSelectionSequence (
    "PSIX0",
    Sequences = [
        #
        SelectionSequence ( 'ETA'           , psix0 . b2eta       () ) ,
        SelectionSequence ( 'ETAPRIME'      , psix0 . b2etap      () ) ,
        SelectionSequence ( 'OMEGA'         , psix0 . b2omega     () ) ,
        #
        SelectionSequence ( 'KETA'          , psix0 . b2Keta      () ) ,
        SelectionSequence ( 'KETAPRIME'     , psix0 . b2Ketap     () ) ,
        SelectionSequence ( 'KOMEGA'        , psix0 . b2Komega    () ) ,
        #
        SelectionSequence ( 'KKETA'         , psix0 . b2KKeta     () ) ,
        SelectionSequence ( 'KKETAPRIME'    , psix0 . b2KKetap    () ) ,
        SelectionSequence ( 'KKOMEGA'       , psix0 . b2KKomega   () ) ,
        #
        SelectionSequence ( 'PIETA'         , psix0 . b2pieta     () ) ,
        SelectionSequence ( 'PIETAPRIME'    , psix0 . b2pietap    () ) ,
        SelectionSequence ( 'PIOMEGA'       , psix0 . b2piomega   () ) ,
        #
        SelectionSequence ( 'PIPIETA'       , psix0 . b2pipieta   () ) ,
        SelectionSequence ( 'PIPIETAPRIME'  , psix0 . b2pipietap  () ) ,
        SelectionSequence ( 'PIPIOMEGA'     , psix0 . b2pipiomega () ) ,
        #
        SelectionSequence ( 'KPIETA'        , psix0 . b2Kpieta    () ) ,
        SelectionSequence ( 'KPIETAPRIME'   , psix0 . b2Kpietap   () ) ,
        SelectionSequence ( 'KPIOMEGA'      , psix0 . b2Kpiomega  () ) , 
        SelectionSequence ( 'KPIPI'         , psix0 . b2Kpipi     () ) ,
        #
    ] 
)
 
# =============================================================================
## PSIX stream 
# =============================================================================
b2psi3Kpi_seq = SelectionSequence ( 'B2Psi3KPi'   , psix . psi_3Kpi () )
psix_seq = MultiSelectionSequence (
    "PSIX"      ,
    Sequences = [
    #
    SelectionSequence ( 'B2PsiK'      , psix . psi_K    () ) ,
    SelectionSequence ( 'B2PsiPi'     , psix . psi_pi   () ) ,
    #
    SelectionSequence ( 'B2Psi2K'     , psix . psi_2K   () ) ,
    SelectionSequence ( 'B2Psi2KPi'   , psix . psi_2Kpi () ) ,
    SelectionSequence ( 'B2Psi2Pi'    , psix . psi_2pi  () ) ,
    #
    SelectionSequence ( 'B2Psi3K'     , psix . psi_3K   () ) ,
    SelectionSequence ( 'B2Psi3Pi'    , psix . psi_3pi  () ) ,
    b2psi3Kpi_seq  , 
    #
    SelectionSequence ( 'B2Psi4K'     , psix . psi_4K   () ) ,
    SelectionSequence ( 'B2Psi4KPi'   , psix . psi_4Kpi () ) ,
    SelectionSequence ( 'B2Psi4Pi'    , psix . psi_4pi  () ) ,
    #
    SelectionSequence ( 'B2Psi5K'     , psix . psi_5K   () ) ,
    SelectionSequence ( 'B2Psi5KPi'   , psix . psi_5Kpi () ) ,
    SelectionSequence ( 'B2Psi5Pi'    , psix . psi_5pi  () ) ,
    #
    SelectionSequence ( 'B2Psi6KPi'   , psix . psi_6Kpi () ) ,
    SelectionSequence ( 'B2Psi6Pi'    , psix . psi_6pi  () ) ,
    #
    SelectionSequence ( 'B2Psi7KPi'   , psix . psi_7Kpi () ) ,
    SelectionSequence ( 'B2Psi7Pi'    , psix . psi_7pi  () ) ,
    #
    SelectionSequence ( 'B2PsiKS0'    , psix . psi_ks0    () ) ,
    SelectionSequence ( 'B2PsiKS0Pi'  , psix . psi_ks0pi  () ) ,
    SelectionSequence ( 'B2PsiKS02Pi' , psix . psi_ks02pi () ) ,
    SelectionSequence ( 'B2PsiKS03Pi' , psix . psi_ks03pi () ) ,
    #
    ## Lb
    #
    SelectionSequence ( 'Lb2PSIPK'      , psix . psi_pK      () ) ,
    SelectionSequence ( 'Lb2PSIPPi'     , psix . psi_ppi     () ) ,
    SelectionSequence ( 'Lb2PSIPKPiPi'  , psix . psi_pKpipi  () ) ,
    SelectionSequence ( 'Lb2PSIPPiPiPi' , psix . psi_ppipipi () ) ,
    SelectionSequence ( 'Xb2PSIPKK'     , psix . psi_pKK     () ) ,
    SelectionSequence ( 'Xb2PSIPKKPi'   , psix . psi_pKKpi   () ) ,
    SelectionSequence ( 'Ob2PSIPKKKPi'  , psix . psi_pKKKpi  () ) ,
    #
    # 2 protons
    #
    SelectionSequence ( 'B2PSIPP'       , psix . psi_pp       () ) ,
    SelectionSequence ( 'B2PSIPPPi'     , psix . psi_pppi     () ) ,
    SelectionSequence ( 'B2PSIPPK'      , psix . psi_ppK      () ) ,
    SelectionSequence ( 'B2PSIPPPiPi'   , psix . psi_pppipi   () ) ,
    SelectionSequence ( 'B2PSIPP3KPi'   , psix . psi_pp3Kpi   () ) ,
    SelectionSequence ( 'B2PSIPP3Pi'    , psix . psi_pp3pi    () ) ,
    #
    # HexaB
    #
    SelectionSequence ( 'Hb2PSIPPK'     , psix . Hb_psi_ppK  () ) ,
    SelectionSequence ( 'Hb2PSIPPKPi'   , psix . Hb_psi_ppKpi() ) ,
    #
    ## B + X
    #
    SelectionSequence ( 'B&B'           , comb.bb_sel  )  ,
    SelectionSequence ( 'B&C'           , comb.bc_sel  )  ,    
    SelectionSequence ( 'B&2Mu'         , comb.bm_sel  )  ,    
    SelectionSequence ( 'B&W'           , comb.bw_sel  )  ,
    #
    ##  Bc -> psi rho+          ## new 
    # 
    SelectionSequence ( 'BC2RHO'          , psix0 . bc2rho     () ) ,
    #
    ##  Bc -> J/psi pi+ pi+ pi0 pi0          ## new
    #
    SelectionSequence ( 'BC2PSI4PI'       , psix0 . bc2psi4pi  () ) ,
    #
    ## for the photon efficiency: ## new
    #
    SelectionSequence ( 'KSTARPLUS'       , psix0 . bu2Kstar   () ) ,
    ## SelectionSequence ( 'KSTARPLUSMERGED' , psix0 . bu2KstarM  () ) , 
    #
    #
    ## channels with chic
    # 
    SelectionSequence ( 'B2CHICK'     , psix0 . b2chicK     () ) ,
    SelectionSequence ( 'B2CHIC2K'    , psix0 . b2chic2K    () ) ,
    SelectionSequence ( 'B2CHIC3K'    , psix0 . b2chic3K    () ) ,
    SelectionSequence ( 'B2CHIC2KPi'  , psix0 . b2chic2Kpi  () ) ,
    SelectionSequence ( 'B2CHIC3KPi'  , psix0 . b2chic3Kpi  () ) ,    
    SelectionSequence ( 'B2CHIC2Pi'   , psix0 . b2chic2pi   () ) ,
    SelectionSequence ( 'B2CHIC3Pi'   , psix0 . b2chic3pi   () ) ,
    #
    SelectionSequence ( 'BC2CHICPi'   , psix0 . bc2chicpi   () ) ,
    SelectionSequence ( 'Lb2CHICPK'   , psix0 . lb2chicpK   () ) ,
    SelectionSequence ( 'Lb2CHICPPi'  , psix0 . lb2chicppi  () ) ,
    #
    SelectionSequence ( 'B2X3872K'    , psix0 . b2X3872K    () ) ,
    SelectionSequence ( 'B2X3872KPi'  , psix0 . b2X3872Kpi  () ) ,
    SelectionSequence ( 'Lb2X3872KPi' , psix0 . lb2X3872pK  () ) ,
    #
    ## dedicated Bc lines (no IP hadrons)
    #
    #SelectionSequence ( "BC2PSIPI"      , psix.bc_psi_pi       () ) ,
    #SelectionSequence ( "BC2PSI3PI"     , psix.bc_psi_3pi      () ) ,
    #SelectionSequence ( "BC2PSI3KPI"    , psix.bc_psi_3Kpi     () ) ,
    #SelectionSequence ( "BC2PSI3K"      , psix.bc_psi_3K       () ) ,
    # 
    #SelectionSequence ( "BC2PSI5PI"     , psix.bc_psi_5pi      () ) ,
    #SelectionSequence ( "BC2PSI5KPI"    , psix.bc_psi_5Kpi     () ) ,
    #SelectionSequence ( "BC2PSI5K"      , psix.bc_psi_5K       () ) ,
    # 
    #SelectionSequence ( "BC2PSIPPPI"    , psix.bc_psi_pppi     () ) ,
    #SelectionSequence ( "BC2PSIPPK"     , psix.bc_psi_ppK      () ) ,
    #SelectionSequence ( "BC2PSIPP3PI"   , psix.bc_psi_pp3pi    () ) ,
    #SelectionSequence ( "BC2PSIPP3KPI"  , psix.bc_psi_pp3Kpi   () ) ,
    ##
    #SelectionSequence ( "BC2PSIKSK"     , psix.bc_psi_ks0K     () ) ,
    #SelectionSequence ( "BC2PSIKSKPIPI" , psix.bc_psi_ks0Kpipi () ) ,     
    ]
)

# =============================================================================
## UPSILON stream 
# =============================================================================
bottom_seq = MultiSelectionSequence (
    'BOTTOM' ,
    Sequences = [
    #
    SelectionSequence ( 'Ups'        , bottom.upsilons()  ) ,
    SelectionSequence ( 'Y&Pi'       , bottom.upsilon_pi           () ) ,
    ## SelectionSequence ( 'Y&KK'       , bottom.upsilon_phi       () ) ,
    ## SelectionSequence ( 'Y&PiPi'  , bottom.upsilon_rho       () ) ,
    ## SelectionSequence ( 'Y&eta'     , bottom.upsilon_eta       () ) ,
    ## SelectionSequence ( 'Y&etapr'   , bottom.upsilon_eta_prime () ) ,
    ## SelectionSequence ( 'Y&omega'   , bottom.upsilon_omega     () ) ,
    ## SelectionSequence ( 'Y&p'       , bottom.upsilon_proton    () ) ,
    SelectionSequence ( 'Y&pp'       , bottom.upsilon_pp        () ) ,
    ## SelectionSequence ( 'Y&pK'      , bottom.upsilon_pK        () ) ,
    SelectionSequence ( 'Y&mumu'     , bottom.upsilon_2mu       () ) ,
    #
    SelectionSequence ( 'Y&C'        , comb.sel_Ycharm ) ,
    SelectionSequence ( 'Y&B'        , comb.sel_YB     ) ,
    SelectionSequence ( 'Y&Psi'      , comb.sel_YPsi   ) ,
    SelectionSequence ( 'Y&PsiB'     , comb.sel_YPsiB  ) ,
    SelectionSequence ( 'Y&Y'        , comb.sel_2xY    ) ,
    ## 
    ## SelectionSequence ( '2xPsiB'     , comb.sel_2xPsiB ) ,
    ## SelectionSequence ( 'Psi&p'      , comb.sel_psi_P  ) , 
    ## SelectionSequence ( 'Psi&pp'     , comb.sel_psi_PP ) ,
    ##
    ## SelectionSequence ( 'Jpsi&mumu'  , wg_chic        ) , 
    ]
    )

tetra_seq = MultiSelectionSequence (
    'TETRA' ,
    Sequences = [
        ## SelectionSequence ( 'Jpsi&mumu'  , wg_chic        ) , 
        #SelectionSequence ( 'Jpsi&mumu_HM'  , wg_psimm      ) , 
        #
        SelectionSequence ( 'Jpsi&mumu'  , psix_prompt.jpsi_mumu()   ) , ## duplicate
        SelectionSequence ( 'Jpsi&KK'    , psix_prompt.jpsi_KK()     ) , 
        SelectionSequence ( 'Jpsi&pp'    , psix_prompt.jpsi_pp()     ) , 
        SelectionSequence ( 'Jpsi&pipi'  , psix_prompt.jpsi_pipi()   ) , 
        #
        SelectionSequence ( 'Jpsi&2mu2pi' , psix_prompt.jpsi_2mu2pi()  ) , 
        SelectionSequence ( 'Jpsi&2mu2K'  , psix_prompt.jpsi_2mu2K()   ) , 
        #
        SelectionSequence ( 'Jpsi&2K2pi'  , psix_prompt.jpsi_2K2pi ()  ) , 
        SelectionSequence ( 'Jpsi&KsKpi'  , psix_prompt.jpsi_KsKpi ()  ) , 
        SelectionSequence ( 'Jpsi&2p2pi'  , psix_prompt.jpsi_2p2pi()   ) , 
        SelectionSequence ( 'Jpsi&4K'     , psix_prompt.jpsi_4K()      ) , 
        #
        SelectionSequence ( 'Y&mumu_TETRA'     , bottom.upsilon_2mu       () ) ,
        SelectionSequence ( 'Y&pp_TETRA'       , bottom.upsilon_pp        () ) ,
        ]
)


# =============================================================================
## V18r0:
# X(3872) -> J/pspi pi pi & psi' -> J/psi pi pi 
# =============================================================================
jpsi  = psix.psi   ()
pions = psix.pions ()  

from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays 
PSI2S = psix.make_selection (
    ## unique tag 
    'Psi2S'    ,
    ## algorithm type to be used
    DaVinci__N3BodyDecays         ,
    ## input selections 
    [ jpsi , pions ] ,
    # 
    ## algorithm configuration:
    #
    DecayDescriptor  = " psi(2S) -> J/psi(1S) pi+ pi- " ,
    Combination12Cut = " ( AM < 4.0 * GeV ) & ( ACHI2DOCA(1,2) < 20 ) " ,
    CombinationCut   = " ( AM < 4.0 * GeV ) & ( ACHI2DOCA(1,3) < 20 ) & ( ACHI2DOCA(2,3) < 20 ) " , 
    MotherCut        = """
    in_range ( 3.635 * GeV  , M - M1 + 3.096 * GeV , 3.740 * GeV )  &  ( CHI2VXNDF < 10 )
    """
)
# =============================================================================
X3872 = psix.make_selection (
    ## unique tag 
    'X3872'    ,
    ## algorithm type to be used
    DaVinci__N3BodyDecays         ,
    ## input selections 
    [ jpsi , pions ] ,
    ## algorithm configuration:
    DecayDescriptor  = " X_1(3872) -> J/psi(1S) pi+ pi- " ,
    Combination12Cut = " ( AM < 4.0 * GeV ) & ( ACHI2DOCA(1,2) < 20 ) " ,
    CombinationCut   = " ( AM < 4.0 * GeV ) & ( ACHI2DOCA(1,3) < 20 ) & ( ACHI2DOCA(2,3) < 20 ) " , 
    MotherCut        = """
    in_range ( 3.825 * GeV  , M - M1 + 3.096 * GeV , 3.925 * GeV )  &  ( CHI2VXNDF < 10 )
    """
)

jpsipipi_seq = MultiSelectionSequence (
    'JPSIPIPI' ,
    Sequences = [
        ## SelectionSequence ( 'B2Psi3KPi'   , psix . psi_3Kpi () ) , ## exlcuive
        b2psi3Kpi_seq  , 
        SelectionSequence ( 'PSI2S'  , PSI2S ) , ## inclusive psi'
        SelectionSequence ( 'X3872'  , X3872 ) , ## inclusive x(3872) 
    ]
)




# =============================================================================
## micro-DST writer
# =============================================================================
from DSTWriters.Configuration import ( SelDSTWriter            ,
                                       stripMicroDSTStreamConf ,
                                       stripMicroDSTElements   )

# Configuration of SelDSTWriter
SelDSTWriterConf     = { 'default' : stripMicroDSTStreamConf ( pack = False ) }
SelDSTWriterElements = { 'default' : stripMicroDSTElements   ( pack = False , refit = True ) }

uDstWriter = SelDSTWriter(
    "MyDSTWriter"                          ,
    StreamConf         =  SelDSTWriterConf      ,
    MicroDSTElements   =  SelDSTWriterElements  ,
    OutputFileSuffix   = 'BandQ'                ,  ## output PRE-fix! 
    SelectionSequences = [
        ## bottom_seq ,
        ## psix_seq   ,
        ## psix0_seq  ,
        ## tetra_seq  , 
        jpsipipi_seq  ,
    ]
    )

#
## Read only fired events to speed up
#
from PhysConf.Filters import LoKi_Filters
fltrs = LoKi_Filters (
    STRIP_Code = """
    HLT_PASS_RE ( 'Stripping.*DiMuonJpsi2MuMuTOS.*'      ) |
    HLT_PASS_RE ( 'Stripping.*DiMuonPsi2MuMuTOS.*'       ) |
    HLT_PASS_RE ( 'Stripping.*DiMuonJpsi2MuMuDetached.*' ) |
    HLT_PASS_RE ( 'Stripping.*DiMuonPsi2MuMuDetached.*'  ) |
    HLT_PASS_RE ( 'Stripping.*DiMuonDiMuonHighMass.*'    )     
    """ 
    )

fltrs = LoKi_Filters (
    STRIP_Code = """
    HLT_PASS_RE ( 'Stripping.*DiMuonJpsi2MuMuDetached.*' )
    """ 
)

# ==============================================================================
# The last step: DaVinci & DB 
# ==============================================================================

from Configurables import DaVinci 
davinci = DaVinci ( 
    #
    ## DataType        = the_year ,                ## ATTENTION !!
    #
    EventPreFilters = fltrs.filters ('Filters') , 
    InputType       = "DST"    , 
    ## EvtMax          =  10000 , #  -1    ,  
    ## PrintFreq       =  100    ,
    Lumi            =  True    ,  
    )

davinci.appendToMainSequence( [ uDstWriter.sequence() ] )

## from Configurables import CondDB
## CondDB ( LatestGlobalTagByDataType = the_year )  ## ATTENTION !!

# =============================================================================
## suppress some  extensive printouts 
# =============================================================================
from Configurables import MessageSvc, PrintDuplicates,PrintDecayTreeTool
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"
#for s in ( 'BOTTOM' , 'PSIX' , 'PSIX0', 'TETRA' ) :
for s in ( 'BOTTOM' , 'PSIX' , 'PSIX0' ) :
    c = PrintDuplicates ( 'FindDuplicates_' + s , OutputLevel = 5 ) 
PrintDecayTreeTool ( 'PrintDuplicateDecays'     , OutputLevel = 4 ) 

    
# =============================================================================
if '__main__' == __name__ :
    
    print ( 100*'*' ) 
    print ( __doc__ ) 
    print ( 100*'*' ) 
    print ( ' Author  : %s ' % __author__  ) 
    print ( ' Version : %s ' % __version__ ) 
    print ( ' Date    : %s ' % __date__    )
    print ( 100*'*' ) 

# =============================================================================
##                                                                      The END 
# =============================================================================
