#!/usr/bin/env gaudirun.py
# -*- coding: utf-8 -*-
# =============================================================================
## @file LambdabStar2.py
#  Script to produce Lambda_b* -> Lambda_b pi+ pi- uDST from FULL stream
#  @attention DataType and CondDB are not specified!
#  Usage:
#  @code
#  gaudirun.py $BANDQCONFIGROOT/scripts/LambdabStar2.py $BANDQCONFIGROOT/scripts/DataType-2012.py  the_file_with_input_data.py
#  @endcode
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2019-04-14
# =============================================================================
""" Script to produce Lambda_b* -> Lambda_b0 pi+ pi- uDST

Usage:

> gaudirun.py $BANDQCONFIGROOT/scripts/LambdabStar2.py $BANDQCONFIGROOT/scripts/DataType-2012.py  the_file_with_input_data.py

"""
# =============================================================================
## logging
# =============================================================================
try : 
    from AnalysisPython.Logger import getLogger
    _name = __name__
    if 0 == _name.find ( '__'   ) : _name = 'WG/B&Q'
    logger = getLogger (  _name )
except:
    from Gaudi.Configuration import log as logger
    
# =============================================================================
from GaudiKernel.SystemOfUnits import GeV 

import os 
logger.info ('Current directory %s' % os.getcwd () )


try :
    import WG_PsiX           as     PSIX
except ImportError :

    logger.info ( 'make a try to modify the path' )
    import sys,  os 
    opath = [ os.path.normpath ( os.path.abspath ( i ) ) for i in  sys.path ]
    npaths = set() 
    for s in sys.argv[1:] :
        if os.path.exists ( s ) :
            ap      = os.path.abspath  ( s  )
            np      = os.path.normpath ( ap )        
            dirname = os.path.dirname  ( np ) 
            if dirname not in opath : npaths.add ( dirname )
            a , b = os.path.split ( dirname )
            dirname = os.path.join ( a , 'scripts' )
            dirname = os.path.abspath  ( dirname )
            dirname = os.path.normpath ( dirname )
            if dirname not in opath : npaths.add ( dirname )
                        
            
    logger.info ( 'Path is exended with: %s' % list ( npaths ) ) 
    for n in npaths :
        if n not in sys.path :
            sys.path = [ n ] + sys.path
            
    ## try to import them now
    import WG_PsiX           as PSIX
    sys.path = opath


    
jpsi_name        = 'FullDSTDiMuonJpsi2MuMuDetachedLine'
jpsi_line        = '/Event/Dimuon/Phys/%s/Particles' % jpsi_name

psix   = PSIX.PsiX_BQ_Conf        ( 'PsiX'           ,
                                    config = { 'DIMUONLINES' : [ jpsi_line ] } )

## get Lambda_b -> J/psi p K-

lambda_b = psix . psi_pK () 

# =============================================================================
## I. Build the Lambda_b* selection
# =============================================================================

from StandardParticles     import StdAllNoPIDsPions as pions

from PhysConf.Selections import CombineSelection 
lambda_star = CombineSelection (
    'PreLambdabStar'     ,
    [ lambda_b , pions ] ,  
    DecayDescriptors = [ '[Lambda_b(5920)0 -> Lambda_b0 pi+ pi-]cc' ,
                         '[Lambda_b(5920)0 -> Lambda_b0 pi+ pi+]cc' ,
                         '[Lambda_b(5920)0 -> Lambda_b0 pi- pi-]cc' ] ,
    CombinationCut  = """
    AM - AM1 < ( 2 * 140 + 560 ) * MeV   
    """,
    MotherCut = """
    ( CHI2VX < 16 ) 
    & ( M - M1 < ( 2 * 140 + 550 ) * MeV  ) 
    """ )

from PhysConf.Selections import FilterSelection
lambda_star = FilterSelection ( 'LambdabStar2' ,
                                lambda_star   ,
                                Code  = """
                                BPVVALID() & 
                                in_range ( -0.1 , DTF_CHI2NDOF ( True ) , 5.1 )
                                """ )

from PhysConf.Selections import SelectionSequence, MultiSelectionSequence
lambda_seq = MultiSelectionSequence (
    'LAMBDABSTAR2' ,
    Sequences = [
    SelectionSequence ( 'LB'     , lambda_b    ) ,
    SelectionSequence ( 'LBSTAR' , lambda_star ) ] 
    )

# =============================================================================
## II. Configure muDST writer 
# =============================================================================

from DSTWriters.Configuration import ( SelDSTWriter            ,
                                       stripMicroDSTStreamConf ,
                                       stripMicroDSTElements   )

# Configuration of SelDSTWriter
SelDSTWriterConf     = { 'default' : stripMicroDSTStreamConf ( pack = False ) }
SelDSTWriterElements = { 'default' : stripMicroDSTElements   ( pack = False , refit = True ) }

uDstWriter = SelDSTWriter(
    "MyDSTWriter"                               ,
    StreamConf         =  SelDSTWriterConf      ,
    MicroDSTElements   =  SelDSTWriterElements  ,
    OutputFileSuffix   = 'BandQ'                ,  ## output PRE-fix! 
    SelectionSequences = [ lambda_seq ]
    )


# ============================================================================
## IV. Configure DaVinci
# ============================================================================

from Configurables import LHCbApp
LHCbApp().XMLSummary = 'summary.xml'

from PhysConf.Filters import LoKi_Filters
fltrs = LoKi_Filters (
    STRIP_Code = """
    HLT_PASS_RE ( 'Stripping.*DiMuonJpsi2MuMuDetached.*' )
    """
    )

from Configurables import DaVinci
davinci = DaVinci (
    #
    EventPreFilters = fltrs.filters('Fltrs') ,
    InputType       = 'DST'            ,
    PrintFreq       = 5000             ,
    EvtMax          = -1               ,
    #
    Lumi            = True ,
    ##
    UserAlgorithms  = [ uDstWriter.sequence() ] 
    )


# =============================================================================
# The END 
# =============================================================================
