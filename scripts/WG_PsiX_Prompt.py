##!/usr/bin/env python
# =============================================================================
## @file
# 
#  The attempt for coherent stripping for X -> Jpsi X0 
#
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru ; Ivan Polyakov Ivan.Polyakov@cern.ch
#  @date   2021-07-19
#
# =============================================================================
""" The attempt for coherent stripping for X -> Jpsi X0 

- phi 
- eho 
- eta 
- eta'
- omega 
- proton
- proton + anti-proton
- proton + kaon 
"""
# =============================================================================
__author__  = 'Vanya BELYAEV Ivan.Belyaev@itep.ru & Ivan Polyakov Ivan.Polyakov@cern.ch'
__date__    = '2021-07-19'
__version__ = '$Revision:$'
# =============================================================================
__all__ = (
    'PsiXPromptConf' ,
    'default_config'    , 
    )
# =============================================================================
from GaudiKernel.SystemOfUnits             import GeV, MeV, mm, micrometer 
from StrippingUtils.Utils                  import LineBuilder
# =============================================================================
## logging
# =============================================================================
import logging
logger = logging.getLogger(__name__)
if not logger.handlers : logging.basicConfig()
logger.setLevel(logging.INFO)
# =============================================================================
## Define the default configuration 
_default_configuration_ = {
    #    
    'NOPIDHADRONS'   : False ,  ## USE FOR SIMULATION "True"
    ## use for B&Q wg production
    'DIMUONLINE'     : None  ,  ## USE FOR B&Q WG-selection  
    #
    ## muon selection for soft muons
    #
    'MuonCut'   : """
    ISMUON & switch ( PROBNNmu < 0 , PIDmu > 1 , PROBNNmu>0.10 )    
    """ ,
    #
    ## pions 
    # 
    'PionCut'   : """
    ( PT          > 300 * MeV ) & 
    ( CLONEDIST   > 5000      ) & 
    ( TRGHOSTPROB < 0.5       ) &
    ( TRCHI2DOF   < 4         ) & 
    in_range ( 2          , ETA , 5         ) &
    in_range ( 3.2 * GeV  , P   , 150 * GeV ) &
    HASRICH                     
    """ ,
    #
    ## kaons
    # 
    'KaonCut'   : """
    ( PT          > 300 * MeV ) & 
    ( CLONEDIST   > 5000      ) & 
    ( TRGHOSTPROB < 0.5       ) &
    ( TRCHI2DOF   < 4         ) & 
    in_range ( 2          , ETA , 5         ) &
    in_range ( 3.2 * GeV  , P   , 150 * GeV ) &
    HASRICH                     
    """ ,
    #
    ## protons 
    #
    'ProtonCut'   : """
    ( PT           > 300 * MeV ) & 
    ( CLONEDIST    > 5000      ) & 
    ( TRCHI2DOF    < 4         ) & 
    ( TRGHOSTPROB  < 0.5       ) & 
    in_range ( 2         , ETA , 5         ) &
    in_range ( 10 * GeV  , P   , 150 * GeV ) &
    HASRICH                      
    """ ,
    ## K0S cuts: 
    #
    'KS0Cut_LL'   : """
    ( PT          > 250 * MeV )
    """ ,
    'KS0Cut_DD'   : """
    ( PT          > 250 * MeV )
    """ ,
    #
    ####
    #
    'JpsiCut'   : """
    in_range ( 2.990 * GeV , M , 3.210 * GeV)
    & CHILDCUT ( 1 , ISMUON & switch ( PROBNNmu < 0 , PIDmu > 1 , PROBNNmu>0.10 ) ) 
    & CHILDCUT ( 2 , ISMUON & switch ( PROBNNmu < 0 , PIDmu > 1 , PROBNNmu>0.10 ) )  
    """ ,
    ##
    'MuFromGamma' : '( AM23 < 1.5 * GeV )' ,
    ##
    'Combination12Cut' : """
    ( AM < 15*GeV ) &
    ( ACHI2DOCA(1,2) <  20 )
    """ ,
    'Combination123Cut' : """
    ( AM < 15*GeV )  &
    ( ACHI2DOCA(1,3) <  20 ) &
    ( ACHI2DOCA(2,3) <  20 )
    """ ,
    'Combination1234Cut' : """
    ( AM < 15*GeV ) &
    ( ACHI2DOCA(1,4) <  20 ) &
    ( ACHI2DOCA(2,4) <  20 ) &
    ( ACHI2DOCA(3,4) <  20 )
    """ ,
    ###
    '3Body_CombinationCut' : """
    in_range( 3.8*GeV,  AM, 15*GeV ) &
    ( ACHI2DOCA(1,3)      < 16         ) &
    ( ACHI2DOCA(2,3)      < 16         )
    """ ,
    '3Body_MotherCut' : """ (chi2vxndf<10)
    & ( MINTREE( 'mu+' == ABSID, PT ) > ((1.2*M-3) /3 /4) )
    & ( MINTREE( 'pi+' == ABSID, PT ) > ((1.2*M-3) /3 /2) )
    & ( MINTREE( 'p+' == ABSID , PT ) > ((1.2*M-3) /3 /2) )
    & ( MINTREE( 'K+' == ABSID , PT ) > ((1.2*M-3) /3 /2) )
    & ( PT > M*0.7 )
    """ ,
    ###
    '4Body_CombinationCut' : """
    in_range( 3.8*GeV,  AM, 15*GeV ) &
    ( ACHI2DOCA(1,4) <  20 ) &
    ( ACHI2DOCA(2,4) <  20 ) &
    ( ACHI2DOCA(3,4) <  20 ) &
    """,
    #
    '4Body_MotherCut' : """ (chi2vxndf<10)
    & ( MINTREE( 'mu+' == ABSID , PT ) > ((1.2*M-3) /4 /4) )
    & ( MINTREE( 'pi+' == ABSID , PT ) > ((1.2*M-3) /4 /2) )
    & ( MINTREE( 'p+'  == ABSID , PT ) > ((1.2*M-3) /4 /2) )
    & ( MINTREE( 'K+'  == ABSID , PT ) > ((1.2*M-3) /4 /2) )
    & ( PT > M*0.7 )
    """ ,
    ###
    '5Body_CombinationCut' : """
    in_range( 3.8*GeV,  AM, 15*GeV ) &
    ( ACHI2DOCA(1,5) <  20 ) &
    ( ACHI2DOCA(2,5) <  20 ) &
    ( ACHI2DOCA(3,5) <  20 ) &
    ( ACHI2DOCA(4,5) <  20 ) 
    """,
    '5Body_MotherCut' : """ (chi2vxndf<10)
    & ( MINTREE( 'mu+' == ABSID , PT ) > (1.2*M-3) /5 /4 )
    & ( MINTREE( 'pi+' == ABSID , PT ) > (1.2*M-3) /5 /2 )
    & ( MINTREE( 'p+'  == ABSID , PT ) > (1.2*M-3) /5 /2 )
    & ( MINTREE( 'K+'  == ABSID , PT ) > (1.2*M-3) /5 /2 )
    & ( PT > M*0.7 )
    """ ,
    #
    
    #
    ## PID-cuts for hadrons 
    #
    'PionPIDCut'   : " (PROBNNpi > 0.4)  & (PROBNNk  < 0.7) " ,
    'KaonPIDCut'   : " (PROBNNk  > 0.25) & (PROBNNpi < 0.7) " ,
    'ProtonPIDCut' : " (PROBNNp  > 0.25) & (PROBNNk  < 0.7) " ,
    #
    ## useful shortcuts:
    #
    'Preambulo' : [
    ## shortcut for chi2 of vertex fit 
    'chi2vx    = CHI2VX'           , 
    'chi2vxNDF = CHI2VXNDOF'       , 
    'chi2vxndf = CHI2VXNDOF'       ,
    'vrho2     = VX**2 + VY**2'    , 
    ] ,
    }
## ============================================================================
## the mandatory element for stripping framework 
default_config = {
    #
    'NAME'        :   'PsiXPrompt'        ,
    'WGs'         : [ 'BandQ' ]              ,
    'CONFIG'      : _default_configuration_  , 
    'BUILDERTYPE' :   'PsiXPromptConf'    ,
    'STREAMS'     : { 'Dimuon'    : [] }
    }
# =============================================================================
class PsiXPromptConf(LineBuilder) :
    """Helper class to configure 'PsiXPrompt'-lines
    """
    __configuration_keys__ = default_config['CONFIG'].keys()
    
    ## get the default configuration 
    @staticmethod
    def defaultConfiguration( key = None ) :
        """Get the default/recommended configurtaion
        
        >>> conf = PsiX0.defaultConfiguration()
        
        """
        from copy import deepcopy
        _config = deepcopy ( _default_configuration_ )
        if key : return _config[ key ]
        return _config
    
    ## constructor
    def __init__ ( self , name , config ) :
        """Constructor
        """
        # check the names 
        if 'PsiXPrompt' != name :
            logger.warning ( 'The non-default name is specified "%s"' % name  ) 
            
        from copy import deepcopy
        _config = deepcopy ( _default_configuration_ )

        if isinstance ( config , dict ):
            _config.update ( config )
            LineBuilder.__init__( self , name , _config )
        else :
            LineBuilder.__init__( self , name ,  config )

        ## private set of selections 
        self.__selections_ = {}


        if not self.name() in self.__selections_ :
            self.__selections_[ self.name() ] = {}
            
        self.__selections_[ self.name() ]['CONFIG'] = deepcopy ( _config ) 
        
        keys = _config.keys()
        for key in keys :
            
            if not key in _default_configuration_ :
                raise KeyError("Invalid key is specified: '%s'" % key )
            
            val = _config[key]
            if val != _default_configuration_ [ key ] : 
                logger.debug ('new configuration: %-16s : %s ' % ( key , _config[key] ) )
                
        self._name         = name
        
        for line in self._lines_PsiXPrompt () :
            self.registerLine(line)
            logger.debug ( "Register line: %s" %  line.name () ) 
            
    ## get the selection, associated with some nickname name 
    def _selection ( self, nick ) :
        """Get the selection, associated with some nickname name
        """
        
        if not self.name() in self.__selections_ :
            self.__selections_[ self.name() ] = {} 
            
        return self.__selections_[ self.name() ].get( nick , None ) 
    
    ## add the selection, associated with some nickname name 
    def _add_selection ( self , nick , sel ) :
        """add the selection, associated with some nickname name
        """
        if not self.name() in self.__selections_ :
            self.__selections_[ self.name() ] = {} 
        
        if nick in self.__selections_[ self.name()]  :
            raise AttributeError ( "Selection '%s'already exists " % nick ) 
        
        self.__selections_[ self.name() ][ nick ] = sel
        
        return sel 
    
    ## empty, no real stripping lines here 
    def _lines_PsiXPrompt ( self ) : return [] 

    # =========================================================================
    ## pure technical method for creation of selections
    # =========================================================================
    def make_selection ( self      ,
                         tag       , 
                         algotype  ,
                         inputs    , 
                         *args     ,
                         **kwargs  ) :
        """Technical method for creation of 1-step selections 
        """
        sel_tag  = '%s_Selection' % tag
        sel_name = 'Sel%sFor%s'   % ( tag , self.name() )
        #
        ## check existing selection
        #
        sel      = self._selection ( sel_tag )
        if sel : return sel 

        #
        ## adjust a bit the arguments
        if not 'Preambulo' in kwargs :
            kwargs ['Preambulo'        ] = self['Preambulo']

        if not 'ParticleCombiners' in kwargs :
            kwargs ['ParticleCombiners'] = { '' : 'LoKi::VertexFitter:PUBLIC' } 
            
        # 
        ## use "simple-selection"
        #
        from PhysSelPython.Wrappers import SimpleSelection
        sel = SimpleSelection (
            sel_name ,
            algotype ,
            inputs   , 
            *args    ,
            **kwargs )
        # 
        return self._add_selection( sel_tag , sel ) 

    ## get the selections
    def _selections_private ( self ) :
        
        sel = self._selection ( 'Selections' )
        if sel : return sel
        
        sel =  [
            #
            self.jpsis          () ,
            #
            self.jpsi_mumu       () ,
            self.jpsi_pipi       () ,
            self.jpsi_pp       () ,
            self.jpsi_KK       () ,
            #
            self.jpsi_2mu2pi       () ,
            self.jpsi_2mu2K       () ,
            self.jpsi_2K2pi       () ,
            self.jpsi_KsKpi       () ,
            self.jpsi_2p2pi       () ,
            self.jpsi_4K       () ,
            ]
        
        return self._add_selection ( 'Selections' , sel )

    # ========================================================================
    ## muons 
    # ========================================================================
    def muons    ( self ) :
        """Soft muons for  X -> J/psi mu+ mu-
        """
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from StandardParticles import StdAllNoPIDsMuons  as inpts 
        ##
        return self.make_selection (
            'Muons'                 ,
            FilterDesktop           ,
            [ inpts ]               ,
            Code = self['MuonCut']  ,
            )
    

    # ========================================================================
    ## pions :
    # ========================================================================
    def pions    ( self ) :
        """Pions for  X -> Jpsi X lines 
        """
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        ##
        if self['NOPIDHADRONS'] :
            from StandardParticles import   StdAllNoPIDsPions as inpts
            pioncut = self['PionCut']
        else                    :
            from StandardParticles import StdAllLooseANNPions as inpts
            pioncut = "(%s)&(%s)" % ( self['PionCut'] , self['PionPIDCut'] ) 
        ##
        return self.make_selection (
            'Pion'                 ,
            FilterDesktop          ,
            [ inpts ]              ,
            Code = pioncut         ,
            )
    
    # ========================================================================
    ## kaons :
    # ========================================================================
    def kaons     ( self ) :
        """Kaons for X -> Jpsi X 
        """
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        ## 
        if self['NOPIDHADRONS'] :
            from StandardParticles import   StdAllNoPIDsKaons as inpts
            kaoncut = self['KaonCut']
        else                    :
            from StandardParticles import StdAllLooseANNKaons as inpts 
            kaoncut = "(%s)&(%s)" % ( self['KaonCut'] , self['KaonPIDCut'] ) 
        #
        ##
        return self.make_selection (
            'Kaon'                 ,
            FilterDesktop          ,
            [ inpts ]              ,
            Code = kaoncut         ,
            )

    # ========================================================================
    ## protons 
    # ========================================================================
    def protons     ( self ) :
        """Protons for X -> Jpsi P 
        """
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        ## 
        if self['NOPIDHADRONS'] :
            from StandardParticles import   StdAllNoPIDsProtons as inpts
            protoncut = self['ProtonCut']
        else                    :
            from StandardParticles import StdAllLooseANNProtons as inpts 
            protoncut = "(%s)&(%s)" % ( self['ProtonCut'] , self['ProtonPIDCut'] ) 
        #
        ##
        return self.make_selection (
            'Proton'               ,
            FilterDesktop          ,
            [ inpts ]              ,
            Code = protoncut       ,
            )
    
    # ========================================================================
    ## KS0 :
    # ========================================================================
    def ks0     ( self ) :
        """
        KS0 for   X -> Jpsi P 
        """
        tag      = 'KsALL'
        sel_tag  = '%s_Selection' % tag
        ksALL    = self._selection ( sel_tag )
        if ksALL : return ksALL
        ##
        from StandardParticles import StdLooseKsLL as inptsLL
        from StandardParticles import StdLooseKsDD as inptsDD
        ##
        from PhysConf.Selections import FilterSelection, MergedSelection
        ## 
        ksLL  = FilterSelection ( 'KsLL' , inptsLL , Code = self['KS0Cut_LL'] )
        ksDD  = FilterSelection ( 'KsDD' , inptsDD , Code = self['KS0Cut_DD'] )
        ##
        ksALL = MergedSelection ( 'KsALL' , [ ksLL , ksDD ] )
        ##
        return self._add_selection ( sel_tag , ksALL )  


    # ========================================================================
    ## Jpsi -> mu+ mu-
    # ========================================================================
    def jpsis    ( self ) :
        """ Jpsi -> mu+ mu-
        """
        from GaudiConfUtils.ConfigurableGenerators import FilterDesktop
        from PhysConf.Selections import AutomaticData
        jpsi_inpts = AutomaticData ( '/Event/Dimuon/Phys/FullDSTDiMuonJpsi2MuMuTOSLine/Particles' )
        ##
        return self.make_selection (
            'Jpsis'                 ,
            FilterDesktop           ,
            [ jpsi_inpts ]               ,
            Code = self['JpsiCut']  ,
            )

    # =========================================================================
    # X -> Jpsi mu+mu-
    # =========================================================================
    def  jpsi_mumu( self ) :
        """X -> Jpsi mu+ mu-
        """
        from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays 
        #
        return self.make_selection  (
            'Jpsi&mumu'                ,
            DaVinci__N3BodyDecays ,
            [ self.jpsis() , self.muons() ] ,
            ## algorithm properties 
            DecayDescriptors  = [ 
                "Upsilon(4S) -> J/psi(1S) mu+ mu-" ,
                "[Upsilon(4S) -> J/psi(1S) mu+ mu+]cc" ,
            ],
            Combination12Cut = self['Combination12Cut'] ,
            CombinationCut = self['3Body_CombinationCut'] + " & " + self["MuFromGamma"],
            MotherCut      = self['3Body_MotherCut'],
            ReFitPVs       = True                
            )

    # =========================================================================
    # X -> Jpsi pi+pi-
    # =========================================================================
    def  jpsi_pipi( self ) :
        """X -> Jpsi pi+ pi-
        """
        from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays 
        #
        return self.make_selection  (
            'Jpsi&pipi'                ,
            DaVinci__N3BodyDecays ,
            [ self.jpsis() , self.pions() ] ,
            ## algorithm properties 
            DecayDescriptors  = [ 
                "Upsilon(4S) -> J/psi(1S) pi+ pi-" ,
                "[Upsilon(4S) -> J/psi(1S) pi+ pi+]cc" ,
            ],
            Combination12Cut = self['Combination12Cut'] ,
            CombinationCut = self['3Body_CombinationCut'] ,
            MotherCut      = self['3Body_MotherCut'] ,
            ReFitPVs       = True                
            )
    
    # =========================================================================
    # X -> Jpsi K+K-
    # =========================================================================
    def  jpsi_KK( self ) :
        """X -> Jpsi K+ K-
        """
        from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays 
        #
        return self.make_selection  (
            'Jpsi&KK'                ,
            DaVinci__N3BodyDecays ,
            [ self.jpsis() , self.kaons() ] ,
            ## algorithm properties 
            DecayDescriptors  = [ 
                "Upsilon(4S) -> J/psi(1S) K+ K-" ,
                "[Upsilon(4S) -> J/psi(1S) K+ K+]cc" ,
            ],
            Combination12Cut = self['Combination12Cut'] ,
            CombinationCut = self['3Body_CombinationCut'] ,
            MotherCut      = self['3Body_MotherCut'] ,
            ReFitPVs       = True                
            )
    
    # =========================================================================
    # X -> Jpsi proton antiproton 
    # =========================================================================
    def  jpsi_pp ( self ) :
        """X -> Jpsi pp 
        """
        from GaudiConfUtils.ConfigurableGenerators import DaVinci__N3BodyDecays
        #
        return self.make_selection  (
            'Jpsi&pp'                  ,
            DaVinci__N3BodyDecays   ,
            [ self.jpsis() , self.protons()] ,
            ## algorithm properties 
            DecayDescriptors  = [ 
                "Upsilon(4S) -> J/psi(1S) p+ p~-" ,
                "[Upsilon(4S) -> J/psi(1S) p+ p+]cc" ,
            ],
            Combination12Cut = self['Combination12Cut'] ,
            CombinationCut = self['3Body_CombinationCut'] ,
            MotherCut      = self['3Body_MotherCut'] ,
            ReFitPVs       = True                
            )            

    # =========================================================================
    # X -> Jpsi mu+mu- pi+ pi-
    # =========================================================================
    def  jpsi_2mu2pi( self ) :
        """X -> Jpsi mu+ mu- pi+ pi-
        """
        from GaudiConfUtils.ConfigurableGenerators import DaVinci__N5BodyDecays 
        #
        return self.make_selection  (
            'Jpsi&2mu2pi'                ,
            DaVinci__N5BodyDecays ,
            [ self.jpsis() , self.muons(), self.pions() ] ,
            ## algorithm properties 
            DecayDescriptors  = [ 
                "Upsilon(4S) -> J/psi(1S) mu+ mu- pi+ pi-" ,
                "[Upsilon(4S) -> J/psi(1S) mu+ mu+ pi+ pi-]cc" ,
                "[Upsilon(4S) -> J/psi(1S) mu+ mu- pi+ pi+]cc" ,
                #"[Upsilon(4S) -> J/psi(1S) mu+ mu+ pi- pi-]cc" ,
                #"[Upsilon(4S) -> J/psi(1S) mu+ mu+ pi+ pi+]cc" ,
            ],
            Combination12Cut = self['Combination12Cut'] ,
            Combination123Cut = self['Combination123Cut'] ,
            Combination1234Cut = self['Combination1234Cut'],
            CombinationCut = self['5Body_CombinationCut'] + " & " + self["MuFromGamma"] ,
            MotherCut      = self['5Body_MotherCut'] ,
            ReFitPVs       = True                
            )

    # =========================================================================
    # X -> Jpsi mu+mu- K+ K-
    # =========================================================================
    def  jpsi_2mu2K( self ) :
        """X -> Jpsi mu+ mu- K+ K-
        """
        from GaudiConfUtils.ConfigurableGenerators import DaVinci__N5BodyDecays 
        #
        return self.make_selection  (
            'Jpsi&2mu2K'                ,
            DaVinci__N5BodyDecays ,
            [ self.jpsis() , self.muons(), self.kaons() ] ,
            ## algorithm properties 
            DecayDescriptors  = [ 
                "Upsilon(4S) -> J/psi(1S) mu+ mu- K+ K-" ,
                "[Upsilon(4S) -> J/psi(1S) mu+ mu+ K+ K-]cc" ,
                "[Upsilon(4S) -> J/psi(1S) mu+ mu- K+ K+]cc" ,
                #"[Upsilon(4S) -> J/psi(1S) mu+ mu+ K- K-]cc" ,
                #"[Upsilon(4S) -> J/psi(1S) mu+ mu+ K+ K+]cc" ,
            ],
            Combination12Cut = self['Combination12Cut'] ,
            Combination123Cut = self['Combination123Cut'] ,
            Combination1234Cut = self['Combination1234Cut'],
            CombinationCut = self['5Body_CombinationCut']  + " & " + self["MuFromGamma"],
            MotherCut      = self['5Body_MotherCut'] ,
            ReFitPVs       = True                
            )

    # =========================================================================
    # X -> Jpsi K+ K- pi+ pi-
    # =========================================================================
    def  jpsi_2K2pi( self ) :
        """X -> Jpsi K+ K- pi+ pi-
        """
        from GaudiConfUtils.ConfigurableGenerators import DaVinci__N5BodyDecays 
        #
        return self.make_selection  (
            'Jpsi&2K2pi'                ,
            DaVinci__N5BodyDecays ,
            [ self.jpsis() , self.kaons(), self.pions() ] ,
            ## algorithm properties 
            DecayDescriptors  = [ 
                "Upsilon(4S) -> J/psi(1S) K+ K- pi+ pi-" ,
                "[Upsilon(4S) -> J/psi(1S) K+ K+ pi+ pi-]cc" ,
                "[Upsilon(4S) -> J/psi(1S) K+ K- pi+ pi+]cc" ,
                #"[Upsilon(4S) -> J/psi(1S) K+ K+ pi- pi-]cc" ,
                #"[Upsilon(4S) -> J/psi(1S) K+ K+ pi+ pi+]cc" ,
            ],
            Combination12Cut = self['Combination12Cut'] ,
            Combination123Cut = self['Combination123Cut'] ,
            Combination1234Cut = self['Combination1234Cut'],
            CombinationCut = self['5Body_CombinationCut'] ,
            MotherCut      = self['5Body_MotherCut'] ,
            ReFitPVs       = True                
            )

    # =========================================================================
    # X -> Jpsi KS0 K- pi+
    # =========================================================================
    def  jpsi_KsKpi( self ) :
        """X -> Jpsi Ks0 K- pi+
        """
        from GaudiConfUtils.ConfigurableGenerators import DaVinci__N5BodyDecays 
        #
        return self.make_selection  (
            'Jpsi&2K2pi'                ,
            DaVinci__N5BodyDecays ,
            [ self.jpsis() , self.ks0, self.kaons(), self.pions() ] ,
            ## algorithm properties 
            DecayDescriptors  = [ 
                "[Upsilon(4S) -> J/psi(1S) KS0 K- pi+]cc" ,
                "[Upsilon(4S) -> J/psi(1S) KS0 K+ pi+]cc" ,
            ],
            Combination12Cut = self['Combination12Cut'] ,
            Combination123Cut = self['Combination123Cut'] ,
            CombinationCut = self['4Body_CombinationCut'] ,
            MotherCut      = self['4Body_MotherCut'] ,
            ReFitPVs       = True                
            )

    # =========================================================================
    # X -> Jpsi p+ p~- pi+ pi-
    # =========================================================================
    def  jpsi_2p2pi( self ) :
        """X -> Jpsi p+ p~- pi+ pi-
        """
        from GaudiConfUtils.ConfigurableGenerators import DaVinci__N5BodyDecays 
        #
        return self.make_selection  (
            'Jpsi&2p2pi'                ,
            DaVinci__N5BodyDecays ,
            [ self.jpsis() , self.protons(), self.pions() ] ,
            ## algorithm properties 
            DecayDescriptors  = [ 
                "Upsilon(4S) -> J/psi(1S) p+ p~- pi+ pi-" ,
                "[Upsilon(4S) -> J/psi(1S) p+ p+ pi+ pi-]cc" ,
                "[Upsilon(4S) -> J/psi(1S) p+ p~- pi+ pi+]cc" ,
                #"[Upsilon(4S) -> J/psi(1S) p+ p+ pi- pi-]cc" ,
                #"[Upsilon(4S) -> J/psi(1S) p+ p+ pi+ pi+]cc" ,
            ],
            Combination12Cut = self['Combination12Cut'] ,
            Combination123Cut = self['Combination123Cut'] ,
            Combination1234Cut = self['Combination1234Cut'],
            CombinationCut = self['5Body_CombinationCut'] ,
            MotherCut      = self['5Body_MotherCut'] ,
            ReFitPVs       = True                
            )

    # =========================================================================
    # X -> Jpsi K+ K- K+ K-
    # =========================================================================
    def  jpsi_4K( self ) :
        """X -> Jpsi K+ K- K+ K-
        """
        from GaudiConfUtils.ConfigurableGenerators import DaVinci__N5BodyDecays 
        #
        return self.make_selection  (
            'Jpsi&4K'                ,
            DaVinci__N5BodyDecays ,
            [ self.jpsis() , self.kaons() ] ,
            ## algorithm properties 
            DecayDescriptors  = [ 
                "Upsilon(4S) -> J/psi(1S) K+ K- K+ K-" ,
                "[Upsilon(4S) -> J/psi(1S) K+ K- K+ K+]cc" ,
            ],
            Combination12Cut = self['Combination12Cut'] ,
            Combination123Cut = self['Combination123Cut'] ,
            Combination1234Cut = self['Combination1234Cut'],
            CombinationCut = self['5Body_CombinationCut'] ,
            MotherCut      = self['5Body_MotherCut'] ,
            ReFitPVs       = True                
            )

    
# =============================================================================
if '__main__' == __name__ :
        
    logger.info ( 80*'*'  ) 
    logger.info (  __doc__ ) 
    logger.info ( ' Author :  %s' % __author__ ) 
    logger.info ( ' Date   :  %s' % __date__   )
    ##
    clines = set() 
    logger.info ( ' Lines declared in default_config["STREAMS"] are' )
    for stream in default_config['STREAMS'] :
        lines = default_config['STREAMS'][stream] 
        for l in lines :
            logger.info ( ' %-15s : %-50s ' % ( stream , l ) )
            clines.add ( l )
    ##
    logger.info ( ' The output locations for the default configuration: ' )
    ##
    _conf = PsiXPromptConf ( 'PsiXPrompt' , 
                                config = default_config['CONFIG']  )
    ##
    _ln   = ' ' + 61*'-' + '+' + 30*'-'
    logger.info ( _ln ) 
    logger.info ( '  %-60s| %-30s  ' % ( 'Output location', 'Stripping line name' ) ) 
    logger.info ( _ln )
    for l in _conf.lines() :
        lout  = l.outputLocation()
        lname = l.name() 
        logger.info ( '  %-60s| %-30s  ' % ( lout, lname ) )
        if not lname in clines :
            raise AttributeError ('Unknown Line %s' % lname )
        clines.remove ( lname )
    logger.info ( _ln ) 
    logger.info ( 80*'*'  ) 
    if clines :
        raise AttributeError('Undeclared lines: %s' % clines )

    ## ## make dot-graphs 
    ## try:    
    ##     selections = _conf._selections_private() 
    ##     for s in selections :
    ##         from SelPy.graph import graph
    ##         o = graph ( s , format = 'png' )
    ##         if o : logger.info  ( "Generate DOT-graph: %s"          % o        )
    ##         else : logger.error ( "Can't produce DOT=-graph for %s" % s.name() )
            
    ## except : pass
   
# =============================================================================
##                                                                      The END 
# =============================================================================
 
