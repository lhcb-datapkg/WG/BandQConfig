#!/usr/bin/env gaudirun.py
# -*- coding: utf-8 -*-
# =============================================================================
## @file CHIC_turbo.py
#  Script to produce chi_c -> J/psi mu+ mu- uDST from TURBO stream
#  @attention DataType and CondDB are not specified!
#  Usage:
#  @code
#  gaudirun.py $BANDQCONFIGROOT/scripts/DataType-2016-Turbo03a.py $BANDQCONFIGROOT/scripts/CHIC_turbo.py the_file_with_input_data.py
#  @endcode
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2017-07-10
# =============================================================================
""" Script to produce chi_c -> J/psi mu+ mu- uDST from TURBO stream

Usage:

> gaudirun.py $BANDQCONFIGROOT/scripts/DataType-2016-Turbo03a.py $BANDQCONFIGROOT/scripts/CHIC_turbo.py the_file_with_input_data.py

"""
# =============================================================================
from Configurables import LHCbApp
LHCbApp().XMLSummary = 'summary.xml'

from PhysConf.Filters import LoKi_Filters
fltrs = LoKi_Filters (
    HLT2_Code = """
    HLT_PASS_RE('.*Hlt2DiMuonJPsiTurbo.*' ) |
    HLT_PASS_RE('.*Hlt2DiMuonChicXTurbo.*')     
    """
    )

from Configurables import DaVinci
davinci = DaVinci (
    #
    EventPreFilters = fltrs.filters('Fltrs') ,
    InputType       = 'MDST'           ,
    Turbo           = True             , 
    PrintFreq       = 5000             ,
    EvtMax          = -1               ,
    #
    Lumi            = True ,
    ##
    )

assert davinci.isPropertySet('DataType'), 'DataType is not defined yet!'
data_type = davinci.DataType

assert data_type in  ( '2015' , '2016' , '2017' , '2018' ),\
       "Invalid Datatype:%s" % data_type 

if   data_type in ( '2015' , '2016' ) : RootInTES = '/Event/Leptons'
elif data_type in ( '2017' , '2018' ) : RootInTES = '/Event/Leptons/Turbo'  

davinci.RootInTES = RootInTES


## II.1 specific for persist reco
from Configurables import DstConf, TurboConf
DstConf   () .Turbo       = True
TurboConf () .PersistReco = True
TurboConf () .DataType    = data_type 

## ## II.2 fix for persist reco, not needed if "plain" Turbo is used 
## #          I would suggest to merge it into a single place, e.g. in TurboConf
## from Configurables import DataOnDemandSvc
## dod = DataOnDemandSvc( Dump = True )
## from Configurables import Gaudi__DataLink as Link
## for  name , target , what  in [
##     ( 'LinkHlt2Tracks' , RootInTES + '/Hlt2/TrackFitted/Long' , '/Event/Hlt2/TrackFitted/Long'     ) , 
##     ( 'LinkDAQ'        , RootInTES + '/DAQ'                   , '/Event/DAQ'                       ) ,
##     ( 'LinkPPs'        , RootInTES + '/Rec/ProtoP/Charged'    , '/Event/Turbo/Hlt2/Protos/Charged' ) 
##     ] :
##     dod.AlgMap [ target ] = Link ( name , Target = target , What = what , RootInTES = '' ) 
    
# ============================================================================
## I. Build the selection 
# ============================================================================
config = {
    'MotherCut'            : ' ( CHI2VXNDF < 10 ) & BPVVALID() '       ,
    'CheckOverlapTool'     : 'LoKi::CheckOverlap/CHECKOVERLAP:PUBLIC'  ,
    'ReFitPVs'             : True 
    }


if data_type in ( '2015' , '2016' ) :
    config.update ( { 'InputPrimaryVertices' : 'Primary'     } ) 
else :
    config.update ( { 'InputPrimaryVertices' : 'Vertex/PV3D' } ) 


## I.1  Starting point: TURBO-line
from PhysConf.Selections import AutomaticData
psi        = AutomaticData ( 'Hlt2DiMuonJPsiTurbo/Particles' )
from PhysConf.Selections import Hlt2Selection
hlt2_jpsi  = Hlt2Selection ( 'HLT2:Hlt2JPsiTurbo' , "HLT_PASS_RE('.*Hlt2DiMuonJPsiTurbo.*')" )
## I.2  Filter good J/psi candidates
from PhysConf.Selections import FilterSelection
psi = FilterSelection (
    'JPSI'   ,
    [ hlt2_jpsi , psi ] ,
    Code  = """
    in_range ( 2.990 * GeV , M , 3.210 * GeV)
    & CHILDCUT ( 1 , ISMUON & switch ( PROBNNmu < 0 , PIDmu > 1 , PROBNNmu>0.10 ) ) 
    & CHILDCUT ( 2 , ISMUON & switch ( PROBNNmu < 0 , PIDmu > 1 , PROBNNmu>0.10 ) ) 
    """ ,
    CloneFilteredParticles  = True  ## NB! 
    )

## I.3 Prepare good muon candidates
from StandardParticles   import StdAllNoPIDsMuons as loose_muons

## I.3.1   IMPOTANT: need to rebuild it!!
from PhysConf.Selections import RebuildSelection
loose_muons = RebuildSelection ( loose_muons ) 

## from PhysConf.Selections import PrintSelection
## loose_muons = PrintSelection ( loose_muons )

## I.3.1 Filter good muon candidates:
from PhysConf.Selections import FilterSelection 
muons =  FilterSelection (
    'MUONS'              ,
    [ loose_muons ]      ,
    Code = """
    ISMUON & switch ( PROBNNmu < 0 , PIDmu > 1 , PROBNNmu>0.10 )
    """ ,
    CloneFilteredParticles  = True  ## NB!
    )

from PhysConf.Selections import PrintSelection
##muons = PrintSelection ( muons ) 

## I.4  Construct chic->J/psi mumu candidates

## I.4.1 algorithm configuration
chic_config = {}
chic_config.update ( config ) 
chic_config.update ( { 'DecayDescriptor'      : 'chi_c1(1P) -> J/psi(1S) mu+ mu-'               ,
                       'Combination12Cut'     : ' ( AM - AM1 ) < ( 3.63 * GeV - 3.096 * GeV ) ' ,
                       'CombinationCut'       : 'in_range ( 3.44 * GeV , AM , 3.63 * GeV )'     } )

## I.4.2 Combine it!
from PhysConf.Selections import Combine3BodySelection
chic   = Combine3BodySelection (
    'ChicToJpsiMuMu' , ## unique name 
    [ psi, muons ]   , ## required selections
    **chic_config    ) ## algorithm properties

### 
##from PhysConf.Selections import SelectionSequence
##chiseq = SelectionSequence ( 'TURBOCHIC' , chic )


# ============================================================================
## II. Configure TURBO input 
# ============================================================================

    
# ============================================================================
## III. Use Chic - > J/psi mumu Turbo line 
# ============================================================================
from PhysConf.Selections import Hlt2Selection
hlt2_chic  = Hlt2Selection   ( 'HLT2:Hlt2ChicXTurbo' , "HLT_PASS_RE('.*Hlt2DiMuonChicXTurbo.*')" )
chic_turbo = AutomaticData   ( 'Hlt2DiMuonChicXTurbo/Particles' )
chic_hlt2  = FilterSelection (
    'ChicTurbo'  ,
    [ hlt2_chic  , chic_turbo ] ,
    Code  = """
    ( M < 3.63 * GeV ) &
    ( CHI2VXNDF < 10 ) &
    in_range ( 3.096 * GeV - 120 * MeV , M1 , 3.096 * GeV + 120 * MeV )
    """ ,
    CloneFilteredParticles  = True  ## NB! 
    )

# ============================================================================
## add prescaled psi(2S) -> J/psi pi+ pi- selection 
# ============================================================================

## prescaled J/psi
from PhysConf.Selections import PrescaleSelection
psi_prescaled = PrescaleSelection ( psi , 0.01 ) 

## I.3 Prepare good muon candidates
from StandardParticles   import StdAllNoPIDsPions as pions 

## I.3.1   IMPOTANT: need to rebuild it!!
from PhysConf.Selections import RebuildSelection
pions = RebuildSelection ( pions )

from PhysConf.Selections import PrintSelection
pions = PrintSelection   ( pions )

psi2S_config = {}
psi2S_config.update ( config ) 
psi2S_config.update ( {
    'DecayDescriptor'      : 'psi(2S) -> J/psi(1S) pi+ pi-'                   ,
    'Combination12Cut'     : ' ( AM - AM1 ) < ( 3.725 * GeV - 3.096 * GeV ) ' ,
    'CombinationCut'       : 'in_range ( 3.644 * GeV , AM - AM1 + 3.096 * GeV , 3.716 * GeV )' } ) 
    
from StandardParticles   import StdAllNoPIDsPions  as pions 
psi2S_ = Combine3BodySelection ( '__Psi2SToJpsiPiPi'         , ## unique name 
                                 [ psi_prescaled , pions ]   , ## required selections
                                 **psi2S_config              ) ## algorithm properties 

from PhysConf.Selections import SimpleSelection
from GaudiConfUtils.ConfigurableGenerators import FilterInTrees as _FINT
PIONS  = SimpleSelection ( 'PIONS'    , _FINT  , [ psi2S_ ]  , 
                           Code = '"pi+"==ABSID'             ,
                           CloneFilteredParticles  = True    ) ## NB!

psi2S  = Combine3BodySelection ( 'Psi2SToJpsiPiPi'           , ## unique name 
                                 [ psi_prescaled , PIONS ]   , ## required selections
                                 **psi2S_config              ) ## algorithm properties 

# ============================================================================
## V. Configure DaVinci
# ============================================================================

davinci.UserAlgorithms.append ( chic      )
davinci.UserAlgorithms.append ( chic_hlt2 )
davinci.UserAlgorithms.append ( psi2S     )

# ============================================================================
## VI. Configure uDST writer/copier  
# ============================================================================
## due to technical reasons for production it needs to be done late...

def _configure_output_ () :

    outputfile = 'chi_turbo.mdst'
    ##
    from Gaudi.Configuration import allConfigurables
    fakew  = allConfigurables.get('MyDSTWriter',None)
    if fakew and 'Sel' != fakew.OutputFileSuffix : 
        outputfile = fakew.OutputFileSuffix + '.' + outputfile 

    ##
    from GaudiConf import IOHelper
    ioh    = IOHelper        ( 'ROOT'     , 'ROOT' ) 
    oalgs  = ioh.outputAlgs  ( outputfile , 'InputCopyStream/COPYTURBO' )
    
    writer = oalgs[0]
    writer.AcceptAlgs = [ chic  .algorithm().name () ,
                          psi2S .algorithm().name () ]
    
    from Configurables import DaVinci
    rit = DaVinci().RootInTES
    dt  = DaVinci().DataType 
    
    items = [ 'Phys/ChicToJpsiMuMu#99'  ,
              'Phys/Psi2SToJpsiPiPi#99' ,
              'Phys/JPSI#99'            ,        
              'Phys/MUONS#99'           ,
              'Phys/PIONS#99'           ]        
    
    if dt in ( '2017' , '2018' ) : 
        items             += [ 'Phys/ChicTurbo#99' ] 
        writer.AcceptAlgs += [ chic_hlt2.algorithm().name() ]
        
    writer.OptItemList   += [  '%s/%s' % ( rit , i )  for i in  items ] 

    ## writer.OutputLevel = 2

    from Configurables import GaudiSequencer
    oseq  = GaudiSequencer ( 'OUTPUTCOPY', Members = oalgs ) 
    
    from Configurables import ApplicationMgr
    AM    = ApplicationMgr ()
    
    if not AM.OutStream :
        AM.OutStream =[]
        
        AM.OutStream.append ( oseq )
        
        
from Gaudi.Configuration import appendPostConfigAction
appendPostConfigAction ( _configure_output_ )


# =============================================================================
# The END 
# =============================================================================
