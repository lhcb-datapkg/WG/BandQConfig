#!/usr/bin/env gaudirun.py 
# =============================================================================
# @file
#
# WG-production for Multi-Charm
#
#  - double charm 
#  - triple charm
#  - ...
#
# Input :  CHARM.DST
#
# Outputs :
#
#  - BandQ.MULTICHARM.mdst
#
# @author Vanya BELYAEV Ivan.Belyaev@itep.ru
# @date   2018-10-24
#
# @attention DataType and CondDB are not specified!
#
# @code
# 
# gaudirun.py  $BANDQCONFIGROOT/scripts/MultiCharm.py $BANDQCONFIGROOT/scripts/DataType-2012.py  the_file_with_input_data.py
#
# @endcode
# =============================================================================
"""WG-production for ``multi-charm'' events 

- double charm 
- triple charm
- ...

Input  :
- CHARM.DST

Output :
- BandQ.MULTICHARM.mdst
    
Attention: DataType and CondDB are not specified!

> gaudirun.py  MultiCharm.py $BANDQCONFIGROOT/scripts/DataType-2012.py   data.py
   
"""
# =============================================================================
__author__   = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__     = "$Date: 2012-12-18 16:28:58 +0100 (Tue, 18 Dec 2012) $"
__version__  = "$Revision: 150278 $"
# =============================================================================
## logging
# =============================================================================
try : 
    from AnalysisPython.Logger import getLogger
    _name = __name__
    if 0 == _name.find ( '__'   ) : _name = 'WG/B&Q'
    logger = getLogger (  _name )
except:
    from Gaudi.Configuration import log as logger
    
# =============================================================================
from GaudiKernel.SystemOfUnits import GeV 


from PhysConf.Selections import AutomaticData

dicharm     = AutomaticData (              'Phys/DiCharmForPromptCharm/Particles' )
tricharm    = AutomaticData (             'Phys/TriCharmForPromptCharm/Particles' )
dimucharm   = AutomaticData (       'Phys/DiMuonAndCharmForPromptCharm/Particles' )
dimucharm2  = AutomaticData (     'Phys/DiMuonAndDiCharmForPromptCharm/Particles' )
dimu2charm  = AutomaticData ( 'Phys/DoubleDiMuonAndCharmForPromptCharm/Particles' )

## from PhysConf.Selections import FilterSelection
## props        = { 'Code' : 'ALL' } 
## dicharm      = FilterSelection ( 'DiCharm'              , dicharm_    , **props )
## tricharm     = FilterSelection ( 'TriCharm'             , tricharm_   , **props )
## dimucharm    = FilterSelection ( 'DiMuonAndCharm'       , dimucharm_  , **props )
## dimucharm2   = FilterSelection ( 'DiMuonAndDiCharm'     , dimucharm2_ , **props )
## dimu2charm   = FilterSelection ( 'DoubleDiMuonAndCharm' , dimu2charm_ , **props )

## 
from PhysSelPython.Wrappers import MultiSelectionSequence
from PhysSelPython.Wrappers import      SelectionSequence
multicharm  = MultiSelectionSequence (
    'MULTICHARM' ,
    Sequences = [ 
    SelectionSequence ( 'DOUBLECHARM'  ,     dicharm  ) ,
    SelectionSequence ( 'TRIPLECHARM'  ,    tricharm  ) ,
    SelectionSequence ( 'DIMUONCHARM'  ,   dimucharm  ) ,
    SelectionSequence ( 'DIMUONCHARM2' ,   dimucharm2 ) ,
    SelectionSequence ( 'DIMUON2CHARM' ,  dimu2charm  ) ]
    )

# =============================================================================
## micro-DST writer
# =============================================================================
from DSTWriters.Configuration import ( SelDSTWriter            ,
                                       stripMicroDSTStreamConf ,
                                       stripMicroDSTElements   )

# Configuration of SelDSTWriter
SelDSTWriterConf     = { 'default' : stripMicroDSTStreamConf ( pack = False ) }
SelDSTWriterElements = { 'default' : stripMicroDSTElements   ( pack = False , refit = True ) }

uDstWriter = SelDSTWriter(
    "MyDSTWriter"                               ,
    StreamConf         =  SelDSTWriterConf      ,
    MicroDSTElements   =  SelDSTWriterElements  ,
    OutputFileSuffix   = 'BandQ'                ,  ## output PRE-fix! 
    SelectionSequences = [ multicharm ]         ,
    RootInTES          = '/Event/Charm'         )

#
## Read only fired events to speed up
#
from PhysConf.Filters import LoKi_Filters
fltrs = LoKi_Filters (
    STRIP_Code = """
    HLT_PASS_RE(              '.*DiCharmForPromptCharm.*') |
    HLT_PASS_RE(             '.*TriCharmForPromptCharm.*') |
    HLT_PASS_RE(       '.*DiMuonAndCharmForPromptCharm.*') |
    HLT_PASS_RE(     '.*DiMuonAndDiCharmForPromptCharm.*') |
    HLT_PASS_RE( '.*DoubleDiMuonAndCharmForPromptCharm.*')         
    """
    )

# ==============================================================================
# The last step: DaVinci & DB 
# ==============================================================================

from Configurables import DaVinci 
davinci = DaVinci ( 
    #
    ## DataType        = the_year ,                ## ATTENTION !!
    #
    EventPreFilters = fltrs.filters ('Filters') ,
    RootInTES       = '/Event/Charm' , 
    InputType       = "MDST"         , 
    EvtMax          =    -1          ,  
    PrintFreq       =  50000         ,
    Lumi            =  True          ,  
    )

davinci.appendToMainSequence( [ uDstWriter.sequence() ] )
davinci.UserAlgorithms = [ multicharm.sequence() ]

## from Configurables import CondDB
## CondDB ( LatestGlobalTagByDataType = the_year )  ## ATTENTION !!

# =============================================================================
## suppress some  extensive printouts 
# =============================================================================
from Configurables import MessageSvc, PrintDuplicates,PrintDecayTreeTool
##MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"
## for s in ( 'BOTTOM' , 'PSIX' , 'PSIX0' ) :
##    c = PrintDuplicates ( 'FindDuplicates_' + s , OutputLevel = 5 ) 
##PrintDecayTreeTool ( 'PrintDuplicateDecays'     , OutputLevel = 4 ) 
    
# =============================================================================
if '__main__' == __name__ :
    
    print 100*'*'
    print __doc__
    print 100*'*'
    print ' Author  : %s ' % __author__
    print ' Version : %s ' % __version__ 
    print ' Date    : %s ' % __date__
    print 100*'*'

# =============================================================================
# The END 
# =============================================================================

