from GaudiConfUtils.ConfigurableGenerators import ( CombineParticles      ,
                                                    DaVinci__N3BodyDecays ,
                                                    SubstitutePID         )
from PhysConf.Selections                   import SimpleSelection, AutomaticData 




class Combine(object) :

    def __init__ ( self , bottom , psix , pc ) :
        
        # =====================================================================
        ## B&W 
        # =====================================================================
        self.bw_sel = SimpleSelection (
            'SelB&W'                    ,
            CombineParticles            ,
            [ pc.W()  , psix.beauty() ] ,
            # 
            ## the decays to be reconstructed
            DecayDescriptors = [ # beauty + W 
            " [ chi_b0(2P) -> B0        mu+ ]cc " ,
            " [ chi_b0(2P) -> B~0       mu+ ]cc " ,
            " [ chi_b0(2P) -> B+        mu+ ]cc " ,
            " [ chi_b0(2P) -> B-        mu+ ]cc " ,
            " [ chi_b0(2P) -> B_s0      mu+ ]cc " ,
            " [ chi_b0(2P) -> B_s0      mu- ]cc " ,
            ] ,
            ## combination cut : accept all
            CombinationCut = " AALL " ,
            ##      mother cut : accept all
            MotherCut      = "  ALL " , 
            #
            ReFitPVs       = True    
            )
        
        # =========================================================================
        ## B&B
        # =========================================================================
        self.bb_sel = SimpleSelection (
            ##
            'SelB&B'          ,
            CombineParticles  , 
            [ psix.beauty() ] ,
            # 
            ## the decays to be reconstructed
            DecayDescriptors = [ # beauty+beauty
            "   chi_b0(2P) -> B0        B~0       " ,
            "   chi_b0(2P) -> B+        B-        " ,
            "   chi_b0(2P) -> B_s0      B_s0      " ,
            #
            " [ chi_b0(2P) -> B0        B0    ]cc " ,
            " [ chi_b0(2P) -> B0        B+    ]cc " ,
            " [ chi_b0(2P) -> B0        B_s0  ]cc " ,
            # 
            " [ chi_b0(2P) -> B0        B-    ]cc " ,
            " [ chi_b0(2P) -> B0        B_s~0 ]cc " ,
            #
            " [ chi_b0(2P) -> B+        B+    ]cc " ,
            " [ chi_b0(2P) -> B+        B_s0  ]cc " ,
            " [ chi_b0(2P) -> B+        B_s~0 ]cc " ,
            #
            ] ,
            ## combination cut : accept all
            CombinationCut = " AALL " ,
            ##      mother cut : accept all
            MotherCut      = "  ALL " , 
            ReFitPVs       = True    
            )
        
        # =========================================================================
        ## B&Charm
        # =========================================================================
        self.bc_sel = SimpleSelection (
            'SelB&C'          ,
            CombineParticles  , 
            [ psix . beauty() , pc . PromptCharm() ] , 
            ## the decays to be reconstructed
            DecayDescriptors = [ # beauty + charm
            #
            " [ chi_b0(2P) -> B0        D0         ]cc " ,
            " [ chi_b0(2P) -> B0        D+         ]cc " ,
            " [ chi_b0(2P) -> B0        D*(2010)+  ]cc " ,
            " [ chi_b0(2P) -> B0        D_s+       ]cc " ,
            " [ chi_b0(2P) -> B0        Lambda_c+  ]cc " ,
            #
            " [ chi_b0(2P) -> B0        D~0        ]cc " ,
            " [ chi_b0(2P) -> B0        D-         ]cc " ,
            " [ chi_b0(2P) -> B0        D*(2010)-  ]cc " ,
            " [ chi_b0(2P) -> B0        D_s-       ]cc " ,
            " [ chi_b0(2P) -> B0        Lambda_c~- ]cc " ,
            #
            " [ chi_b0(2P) -> B+        D0         ]cc " ,
            " [ chi_b0(2P) -> B+        D+         ]cc " ,
            " [ chi_b0(2P) -> B+        D*(2010)+  ]cc " ,
            " [ chi_b0(2P) -> B+        D_s+       ]cc " ,
            " [ chi_b0(2P) -> B+        Lambda_c+  ]cc " ,
            #
            " [ chi_b0(2P) -> B+        D~0        ]cc " ,
            " [ chi_b0(2P) -> B+        D-         ]cc " ,
            " [ chi_b0(2P) -> B+        D*(2010)-  ]cc " ,
            " [ chi_b0(2P) -> B+        D_s-       ]cc " ,
            " [ chi_b0(2P) -> B+        Lambda_c~- ]cc " ,
            #
            #
            " [ chi_b0(2P) -> B_s0      D0         ]cc " ,
            " [ chi_b0(2P) -> B_s0      D+         ]cc " ,
            " [ chi_b0(2P) -> B_s0      D*(2010)+  ]cc " ,
            " [ chi_b0(2P) -> B_s0      D_s+       ]cc " ,
            " [ chi_b0(2P) -> B_s0      Lambda_c+  ]cc " ,
            #
            " [ chi_b0(2P) -> B_s0      D~0        ]cc " ,
            " [ chi_b0(2P) -> B_s0      D-         ]cc " ,
            " [ chi_b0(2P) -> B_s0      D*(2010)-  ]cc " ,
            " [ chi_b0(2P) -> B_s0      D_s-       ]cc " ,
            " [ chi_b0(2P) -> B_s0      Lambda_c~- ]cc " ,
            #
            ] ,
            ## combination cut : accept all
            CombinationCut = " AALL " ,
            ##      mother cut : accept all
            MotherCut      = "  ALL " , 
            )
        # ==============================================================================
        ## B&2mu
        # ==============================================================================
        self.bm_sel = SimpleSelection (
            'SelB&2Mu'         ,
            CombineParticles   , 
            [ psix . beauty () , pc . DiMuon () ] ,
            ## the decays to be reconstructed
            DecayDescriptors = [ # beauty+2mu
            " [ chi_b0(2P) -> B0        J/psi(1S)  ]cc " ,
            " [ chi_b0(2P) -> B+        J/psi(1S)  ]cc " ,
            "   chi_b0(2P) -> B_s0      J/psi(1S)      " ,
            #
            ] ,
            ## combination cut : accept all
            CombinationCut = " AALL " ,
            ##      mother cut : accept all
            MotherCut      = "  ALL " , 
            )
        
        # ==============================================================================
        ## Y&B
        # ==============================================================================
        self.sel_YB = SimpleSelection (
            'Y&Beauty'         ,
            CombineParticles   , 
            [ bottom.upsilons() , psix . beauty ()  ] ,
            ## the decays to be reconstructed
            DecayDescriptors = [ # beauty+2mu
            " [ chi_b0(2P) -> Upsilon(1S) B0   ]cc " ,
            " [ chi_b0(2P) -> Upsilon(1S) B+   ]cc " ,
            "   chi_b0(2P) -> Upsilon(1S) B_s0     " ,
            #
            ] ,
            ## combination cut : accept all
            CombinationCut = " AALL " ,
            ##      mother cut : accept all
            MotherCut      = "  ALL " , 
            ReFitPVs       = True    
            )

        # =========================================================================
        ## Y + prompt charm 
        # =========================================================================
        self.sel_Ycharm = SimpleSelection (
            'Y&Charm'        ,
            CombineParticles , 
            [ bottom.upsilons (), pc.PromptCharm() ] , 
            ## algorithm parameters 
            DecayDescriptors    = [
            "[ chi_b1(1P) -> Upsilon(1S) D0        ]cc" ,
            "[ chi_b1(1P) -> Upsilon(1S) D+        ]cc" ,
            "[ chi_b1(1P) -> Upsilon(1S) D*(2010)+ ]cc" ,
            "[ chi_b1(1P) -> Upsilon(1S) D_s+      ]cc" ,
            "[ chi_b1(1P) -> Upsilon(1S) Lambda_c+ ]cc" ,
            ] , 
            CombinationCut     = " AALL " ,
            MotherCut          = "  ALL " , 
            # 
            ReFitPVs           = True    
            )
        
        # =========================================================================
        ## Y + propmt J/psi 
        # =========================================================================    
        self.sel_YPsi = SimpleSelection (
            'Y&psi'          ,
            CombineParticles , 
            [ bottom.upsilons()  , pc.DiMuon () ] , 
            ## algorithm parameters 
            DecayDescriptor    = " chi_b1(1P) -> Upsilon(1S) J/psi(1S)" ,
            CombinationCut     = " AALL " ,
            MotherCut          = "  ALL " , 
            ReFitPVs           = True    
            )
        # =========================================================================    
        ## Y + detached J/psi 
        # =========================================================================
        self.sel_YPsiB = SimpleSelection (
            'Y&psiB'         ,
            CombineParticles , 
            [ bottom.upsilons()  , psix.psi () ] , 
            ## algorithm parameters 
            DecayDescriptor    = " chi_b2(1P) -> Upsilon(1S) J/psi(1S)" ,
            CombinationCut     = " AALL " ,
            MotherCut          = "  ALL " , 
            )
        # =========================================================================    
        ## Y + Y 
        # =========================================================================
        self.sel_2xY = SimpleSelection (
            '2xY'             ,
            CombineParticles  , 
            [ bottom.upsilons() ] , 
            ## algorithm parameters 
            DecayDescriptor    = " chi_b0(1P) -> Upsilon(1S) Upsilon(1S)" ,
            CombinationCut     = " AALL " ,
            MotherCut          = "  ALL " , 
            ReFitPVs           = True    
            )
        # =========================================================================
        ## pair of detached J/psi 
        # =========================================================================
        self.sel_2xPsiB = SimpleSelection (
            '2xB'             ,   
            CombineParticles  ,
            [ psix.psi ()  ]  ,
            DecayDescriptor   = "chi_b0(1P) -> J/psi(1S) J/psi(1S) " ,
            CombinationCut    = "AALL" ,
            MotherCut         = " ALL" ,
            ReFitPVs          = True    
            )
        
        ## high pt prompt J/psi line
        jpsiPT_location  = 'FullDSTDiMuonJpsi2MuMuTOSLine' ## high pt prompt J/psi 
        jpsiPT_line      = '/Event/Dimuon/Phys/%s/Particles' % jpsiPT_location 
        from PhysConf.Selections import AutomaticData, MergedSelection
        _jpsi_PT         = AutomaticData ( jpsiPT_line )
        
        psi2SPT_location  = 'FullDSTDiMuonPsi2MuMuTOSLine' ## high pt prompt J/psi 
        psi2SPT_line      = '/Event/Dimuon/Phys/%s/Particles' % jpsiPT_location 
        _psi2S_PT         = AutomaticData ( psi2SPT_line )
        
        self.psi_PT = MergedSelection ( 'PsiHighPT' ,
                                        RequiredSelections = [ _jpsi_PT , _psi2S_PT ] )
        
        # =========================================================================
        ## Y -> J/psi p+ p~-
        # =========================================================================
        self.sel_psi_PP = SimpleSelection (
            'Psi&PP'   ,
            DaVinci__N3BodyDecays           ,
            [ self.psi_PT, bottom.protons()  ]  , 
            ## algorithm properties 
            DecayDescriptor  = "Upsilon(1S) -> J/psi(1S) p+ p~-" ,
            DaughtersCuts    = { 'p+' : "PT>1*GeV" },
            Combination12Cut = """
            ( AM < 12 * GeV ) & ( ACHI2DOCA(1,2)  < 16  ) 
            """,    
            CombinationCut = """
            in_range ( 8.4 * GeV , AM , 11.6 * GeV ) &
            ( ACHI2DOCA(1,3)  < 16  ) &
            ( ACHI2DOCA(2,3)  < 16  ) 
            """,
            MotherCut  = """
            VFASPF(VCHI2PDOF) < 8    
            """,
            ReFitPVs          = True    
            )
        # =========================================================================
        ## Pc -> J/psi p+ 
        # =========================================================================
        self.sel_psi_P = SimpleSelection (
            'Psi&P'                ,
            CombineParticles       ,
            [ self.psi_PT , bottom.protons() ]   , 
            ## algorithm properties 
            DecayDescriptor  = "[chi_b1(1P) -> J/psi(1S) p+]cc" ,
            DaughtersCuts    = {
            'p+'        : """
            ( PT>500*MeV        ) &
            ( PROBNNp     > 0.5 ) &
            ( PROBNNpi    < 0.8 ) &
            ( PROBNNK     < 0.8 ) &
            ( BPVIPCHI2() < 16  ) 
            """
            },
            CombinationCut   = """
            ( AM  - AM1 < ( 5.15 - 3.1 ) * GeV ) & 
            ( APT > 3   * GeV ) 
            """,
            MotherCut        = """
            ( PT > 3 *  GeV         ) & 
            ( M  < 5.050 * GeV      ) &
            ( VFASPF(VCHI2PDOF) < 6 )
            """ ,    
            ReFitPVs           = True    
            )
        

# =============================================================================
##                                                                      The END 
# =============================================================================
