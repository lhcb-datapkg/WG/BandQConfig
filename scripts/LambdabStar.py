#!/usr/bin/env gaudirun.py
# -*- coding: utf-8 -*-
# =============================================================================
## @file LambdabStar.py
#  Script to produce Lambda_b* -> Lambda_b pi+ pi- uDST from FULL stream
#  @attention DataType and CondDB are not specified!
#  Usage:
#  @code
#  gaudirun.py $BANDQCONFIGROOT/scripts/LambdabSar.py $BANDQCONFIGROOT/scripts/DataType-2012.py  the_file_with_input_data.py
#  @endcode
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2019-04-13
# =============================================================================
""" Script to produce Lambda_b* -> Lambda_b0 pi+ pi- uDST

Usage:

> gaudirun.py $BANDQCONFIGROOT/scripts/LambdabStar.py $BANDQCONFIGROOT/scripts/DataType-2012.py  the_file_with_input_data.py

"""
# =============================================================================

stripping_line =  'BhadronCompleteEvent/Phys/Lb2LcPiNoIPLc2PKPiBeauty2CharmLine/Particles'


# =============================================================================
## I. Build the selection
# =============================================================================
from PhysConf.Selections import AutomaticData
lambda_b = AutomaticData ( stripping_line )

from PhysConf.Selections import ValidBPVSelection 
lambda_b = ValidBPVSelection ( lambda_b , name = 'LambdabWithBPV' )

from StandardParticles   import StdAllNoPIDsPions as pions

from PhysConf.Selections import CombineSelection 
lambda_star = CombineSelection (
    'PreLambdabStar'     ,
    [ lambda_b , pions ] ,
    DecayDescriptors = [ '[Lambda_b(5920)0 -> Lambda_b0 pi+ pi-]cc' ,
                         '[Lambda_b(5920)0 -> Lambda_b0 pi+ pi+]cc' ,
                         '[Lambda_b(5920)0 -> Lambda_b0 pi- pi-]cc'
                         ] ,
    DaughtersCuts    = {
    'pi+' : "(PROBNNpi>0.05) & (TRGHOSTPROB<0.5)"
    } , 
    CombinationCut  = """
    AM - AM1 < ( 2 * 140 + 560 ) * MeV   
    """,
    MotherCut = """
    ( CHI2VX < 16 ) 
    & ( M - M1 < ( 2 * 140 + 550 ) * MeV  ) 
    """ )

from PhysConf.Selections import FilterSelection
lambda_star = FilterSelection ( 'LambdabStar' ,
                                lambda_star   ,
                                Code  = """
                                BPVVALID() & in_range ( -0.1 , DTF_CHI2NDOF ( True ) , 5.1 )
                                """ )

from PhysConf.Selections import SelectionSequence, MultiSelectionSequence 
lambda_seq = MultiSelectionSequence (
    'LAMBDABSTAR' ,
    Sequences = [
    SelectionSequence ( 'LB'     , lambda_b    ) ,
    SelectionSequence ( 'LBSTAR' , lambda_star ) ] 
    )

# =============================================================================
## II. Configure muDST writer 
# =============================================================================

from DSTWriters.Configuration import ( SelDSTWriter            ,
                                       stripMicroDSTStreamConf ,
                                       stripMicroDSTElements   )

# Configuration of SelDSTWriter
SelDSTWriterConf     = { 'default' : stripMicroDSTStreamConf ( pack = False ) }
SelDSTWriterElements = { 'default' : stripMicroDSTElements   ( pack = False , refit = True ) }

uDstWriter = SelDSTWriter(
    "MyDSTWriter"                               ,
    StreamConf         =  SelDSTWriterConf      ,
    MicroDSTElements   =  SelDSTWriterElements  ,
    OutputFileSuffix   = 'BandQ'                ,  ## output PRE-fix! 
    SelectionSequences = [ lambda_seq ]
    )


# ============================================================================
## IV. Configure DaVinci
# ============================================================================

from Configurables import LHCbApp
LHCbApp().XMLSummary = 'summary.xml'

from PhysConf.Filters import LoKi_Filters
fltrs   = LoKi_Filters (
    STRIP_Code = "HLT_PASS_RE('.*Lb2LcPiNoIPLc.*')"
    )                   

from Configurables import DaVinci
davinci = DaVinci (
    #
    EventPreFilters = fltrs.filters('Fltrs') ,
    InputType       = 'DST'            ,
    PrintFreq       = 5000             ,
    EvtMax          = -1               ,
    #
    Lumi            = True ,
    ##
    UserAlgorithms  = [ uDstWriter.sequence() ] 
    )


# =============================================================================
# The END 
# =============================================================================
