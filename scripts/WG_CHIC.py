#!/usr/bin/env gaudirun.py
# -*- coding: utf-8 -*-
# =============================================================================
## @file WG_CHIC.py
#  Module to make chi_c -> J/psi mu+ mu- 
#  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
#  @date   2017-07-10
# =============================================================================
""" Module to make chi_c -> J/psi mu+ mu-
"""
__all__ = (
    'wg_chic'  , ## selection oof chic    -> J/psi mu+ mu-
    'wg_psi2s' , ## selection oof psi(2S) -> J/psi pi+ pi-
    )
# =============================================================================

from PhysConf.Selections import AutomaticData
jpsi = AutomaticData ( '/Event/Dimuon/Phys/FullDSTDiMuonJpsi2MuMuTOSLine/Particles' )

from PhysConf.Selections import FilterSelection
psi = FilterSelection (
    'JPSI'  ,
    [ jpsi ] ,
    Code  = """
    in_range ( 2.990 * GeV , M , 3.210 * GeV)
    & CHILDCUT ( 1 , ISMUON & switch ( PROBNNmu < 0 , PIDmu > 1 , PROBNNmu>0.10 ) ) 
    & CHILDCUT ( 2 , ISMUON & switch ( PROBNNmu < 0 , PIDmu > 1 , PROBNNmu>0.10 ) ) 
    """
    )

from StandardParticles   import StdAllNoPIDsMuons as muons 
from PhysConf.Selections import FilterSelection 
muons =  FilterSelection (
    'MUONS'   ,
    [ muons ] ,
    Code = """
    ISMUON & switch ( PROBNNmu < 0 , PIDmu > 1 , PROBNNmu>0.10 )
    """
    )

chic_config = { 'DecayDescriptor'  : 'chi_c1(1P) -> J/psi(1S) mu+ mu-'               ,
                'Combination12Cut' : ' ( AM - AM1 ) < ( 4.22 * GeV - 3.096 * GeV ) ' ,
                'CombinationCut'   : 'in_range ( 3.44 * GeV , AM , 4.22 * GeV )'     ,
                'MotherCut'        : ' ( CHI2VXNDF < 10 ) & BPVVALID ()  '           ,
                'ReFitPVs'         : True                                            }

psimm_config = { 'DecayDescriptor'  : 'chi_c1(1P) -> J/psi(1S) mu+ mu-'               ,
                'Combination12Cut' : ' AM < 15*GeV ' ,
                'CombinationCut'   : """ 
                in_range(3.8*GeV, AM, 15*GeV) 
                & (AM23 < 1.5*GeV) 
                """ ,
                'MotherCut'        : """
                ( CHI2VXNDF < 10 ) & BPVVALID ()
                & ( MINTREE( 'mu+' == ABSID, PT ) > (1.2*M-3) /5 /4 )
                """           ,
                'ReFitPVs'         : True                                            }

from PhysConf.Selections import Combine3BodySelection
wg_chic = Combine3BodySelection ( 'ChicToJpsiMuMu' , ## unique name 
                                  [ psi , muons ]  , ## required selections
                                  **chic_config    ) ## algorithm properties 

wg_psimm = Combine3BodySelection ( 'JpsiMuMuHighMass' , ## unique name 
                                  [ psi , muons ]  , ## required selections
                                  **psimm_config    ) ## algorithm properties 


psi2s_config = { 'DecayDescriptor'  : 'psi(2S) -> J/psi(1S) pi+ pi-'                  ,
                 'Combination12Cut' : ' ( AM - AM1 ) < ( 3.725 * GeV - 3.096 * GeV ) ' ,
                 'CombinationCut'   : 'in_range ( 3.644 * GeV , AM - AM1 + 3.096 * GeV , 3.716 * GeV )' ,
                 'MotherCut'        : ' ( CHI2VXNDF < 10 ) & BPVVALID ()  '           ,
                 'ReFitPVs'         : True                                            }


# ==============================================================================
## prescaled J/psi
from PhysConf.Selections import PrescaleSelection
psi_prescaled = PrescaleSelection ( psi , 0.03 ) 

from StandardParticles   import StdAllNoPIDsPions  as pions 
wg_psi2s = Combine3BodySelection ( 'Psi2SToJpsiPiPi'           , ## unique name 
                                   [ psi_prescaled , pions ]   , ## required selections
                                   **psi2s_config              ) ## algorithm properties 


# =============================================================================
##                                                                      The END 
# =============================================================================
