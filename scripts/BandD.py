#!/usr/bin/env gaudirun.py 
# =============================================================================
# @file
#
# WG-production for
#
#  - B&B,
#  - B&C
#  where B is reconstrructed hadronically  from BHADRONCOMPLETEEVENT stream
#
# Input :  BHADRONCOMPLETEEVENT
#
# Outputs :
#
#  - BandQ.BandD.mdst
#
# @author Vanya BELYAEV Ivan.Belyaev@itep.ru
# @date   2021-02-14
#
# @attention DataType and CondDB are not specified!
#
# @code
# 
# gaudirun.py  $BANDQCONFIGROOT/scripts/BandD.py $BANDQCONFIGROOT/scripts/DataType-2012.py  the_file_with_input_data.py
#
# @endcode
# =============================================================================
"""WG-production for

- B&B,
- B&C,
where B is reconstrructed hadronically  from BHADRONCOMPLETEEVENT stream

Input :  DIMUON.DST

Outputs :

  - BandQ.BandD.mdst
  - BandQ.PSIX0.mdst
  - BandQ.BOTTOM.mdst
    
Attention: DataType and CondDB are not specified!

> gaudirun.py  BandD.py $BANDQCONFIGROOT/scripts/DataType-2011.py data.py
> gaudirun.py  BandD.py $BANDQCONFIGROOT/scripts/DataType-2012.py data.py
   
"""

# =============================================================================
__author__   = "Vanya BELYAEV Ivan.Belyaev@itep.ru"
__date__     = "2021-02-16"
__version__  = "$Revision:$"
# =============================================================================
## logging
# =============================================================================
try : 
    from AnalysisPython.Logger import getLogger
    _name = __name__
    if 0 == _name.find ( '__'   ) : _name = 'WG/B&Q'
    logger = getLogger (  _name )
except:
    from Gaudi.Configuration import log as logger
    
# =============================================================================
from   GaudiKernel.SystemOfUnits import MeV, GeV 
from   GaudiConfUtils            import ConfigurableGenerators

import os 
logger.info ('Current directory %s' % os.getcwd () )


location ='BhadronCompleteEvent/Phys/%sBeauty2CharmLine/Particles'


from PhysConf.Selections import AutomaticData
Lb2LcPi    = AutomaticData ( location % 'Lb2LcPiNoIPLc2PKPi'       )
Lb2Lc3Pi   = AutomaticData ( location % 'Lb2LcPiPiPiLc2PKPi'       ) 
Lb2XicPi   = AutomaticData ( location % 'Lb2XicPiNoIPXic2PKPi'     ) 

Xib2XicPi  = AutomaticData ( location % 'Xib2Xic0PiNoIPXic02PKKPi' )
Xib2Xic3Pi = AutomaticData ( location % 'Xib2Xic0PiPiPiXic02PKKPi' )

Bu2D0Pi2   = AutomaticData ( location %  'B2D0PiD2HH'              )
Bu2D0Pi4   = AutomaticData ( location %  'B2D0PiD2HHHHTIGHT'       ) 
Bu2D03Pi   = AutomaticData ( location %  'B2D0PiPiPiD2HHTIGHT'     ) 

B02DPi     = AutomaticData ( location %  'B02DPiD2HHH'             )
B02D3Pi    = AutomaticData ( location %  'B02DPiPiPiD2HHHPID'      )

from PhysConf.Selections import SimpleSelection
Bs2DsPi    = SimpleSelection (
    'Bs2DsPi'  , ConfigurableGenerators.SubstitutePID ,
    inputs        = [ B02DPi ] , 
    ##
    Code          = "DECTREE('[Beauty -> Charm X-]CC')" ,
    MaxChi2PerDoF = -666 , ## switch off refit                                
    Substitutions = { ' Beauty -> ^( Charm & X+ ) X- ' : 'D_s+'  ,
                      ' Beauty -> ^( Charm & X- ) X+ ' : 'D_s-'  ,
                      ' Beauty ->  ( Charm & X+ ) X- '  : 'B_s~0' , 
                      ' Beauty ->  ( Charm & X- ) X+ '  : 'B_s0'  }
    )

Bs2Ds3Pi   = SimpleSelection (
    'Bs2Ds3Pi' , ConfigurableGenerators.SubstitutePID ,
    inputs        = [ B02D3Pi ] , 
    ##
    Code          = "DECTREE('[Beauty --> ( Charm & X+ ) X- X- X+ ]CC')" ,
    MaxChi2PerDoF = -666 , ## switch off refit                                
    Substitutions = { ' Beauty --> ^( Charm & X+ ) X- X- X+ ' : 'D_s+'  ,
                      ' Beauty --> ^( Charm & X- ) X+ X+ X- ' : 'D_s-'  ,
                      ' Beauty -->  ( Charm & X+ ) X- X- X+ ' : 'B_s~0' , 
                      ' Beauty -->  ( Charm & X- ) X+ X+ X- ' : 'B_s0'  }
    ) 


from PhysConf.Selections import MergedSelection 
beauty     = MergedSelection  (
    'BeautyHadrons' , [ Lb2LcPi   , Lb2Lc3Pi   , Lb2XicPi , 
                        Xib2XicPi , Xib2Xic3Pi ,
                        Bu2D0Pi2  , Bu2D0Pi4   , Bu2D03Pi ,
                        B02DPi    , B02D3Pi    ,
                        Bs2DsPi   , Bs2Ds3Pi   ]
    )

# =============================================================================
## Combine two B-hadrons 
# =============================================================================
from PhysConf.Selections import CombineSelection
BB         = CombineSelection  ( 'Hb&Hb' ,
                                 beauty   ,
                                 DecayDescriptors = [
    # beauty+beauty
    "   chi_b0(2P) -> B0        B~0       " ,
    "   chi_b0(2P) -> B+        B-        " ,
    "   chi_b0(2P) -> B_s0      B_s0      " ,
    "   chi_b0(2P) -> Lambda_b0 Lambda_b~0" ,
    "   chi_b0(2P) -> Xi_b-     Xi_b~+    " ,
    "   chi_b0(2P) -> Xi_b0     Xi_b~0    " ,    
    #
    ## B&B
    # 
    " [ chi_b0(2P) -> B0         B0         ]cc " ,
    " [ chi_b0(2P) -> B0         B+         ]cc " ,
    " [ chi_b0(2P) -> B0         B_s0       ]cc " ,
    " [ chi_b0(2P) -> B0         Lambda_b0  ]cc " ,
    " [ chi_b0(2P) -> B0         Xi_b-      ]cc " ,
    " [ chi_b0(2P) -> B0         Xi_b0      ]cc " ,
    #
    " [ chi_b0(2P) -> B+         B+         ]cc " ,
    " [ chi_b0(2P) -> B+         B_s0       ]cc " ,
    " [ chi_b0(2P) -> B+         Lambda_b0  ]cc " ,
    " [ chi_b0(2P) -> B+         Xi_b-      ]cc " ,
    " [ chi_b0(2P) -> B+         Xi_b0      ]cc " ,
    # 
    " [ chi_b0(2P) -> B_s0       B_s0       ]cc " ,
    " [ chi_b0(2P) -> B_s0       Lambda_b0  ]cc " ,
    " [ chi_b0(2P) -> B_s0       Xi_b-      ]cc " ,
    " [ chi_b0(2P) -> B_s0       Xi_b0      ]cc " ,
    # 
    " [ chi_b0(2P) -> Lambda_b0  Lambda_b0  ]cc " ,
    " [ chi_b0(2P) -> Lambda_b0  Xi_b-      ]cc " ,
    " [ chi_b0(2P) -> Lambda_b0  Xi_b0      ]cc " ,
    ##
    " [ chi_b0(2P) -> Xi_b-      Xi_b-      ]cc " ,
    " [ chi_b0(2P) -> Xi_b-      Xi_b0      ]cc " ,
    ##
    " [ chi_b0(2P) -> Xi_b0      Xi_b0      ]cc " ,
    #
    ## B&Bbar 
    # 
    " [ chi_b0(2P) -> B~0        B+         ]cc " ,
    " [ chi_b0(2P) -> B~0        B_s0       ]cc " ,
    " [ chi_b0(2P) -> B~0        Lambda_b0  ]cc " ,
    " [ chi_b0(2P) -> B~0        Xi_b-      ]cc " ,
    " [ chi_b0(2P) -> B~0        Xi_b0      ]cc " ,
    #
    " [ chi_b0(2P) -> B-         B_s0       ]cc " ,
    " [ chi_b0(2P) -> B-         Lambda_b0  ]cc " ,
    " [ chi_b0(2P) -> B-         Xi_b-      ]cc " ,
    " [ chi_b0(2P) -> B-         Xi_b0      ]cc " ,
    # 
    " [ chi_b0(2P) -> B_s~0      Lambda_b0  ]cc " ,
    " [ chi_b0(2P) -> B_s~0      Xi_b-      ]cc " ,
    " [ chi_b0(2P) -> B_s~0      Xi_b0      ]cc " ,
    # 
    " [ chi_b0(2P) -> Lambda_b~0 Xi_b-      ]cc " ,
    " [ chi_b0(2P) -> Lambda_b~0 Xi_b0      ]cc " ,
    ##
    " [ chi_b0(2P) -> Xi_b~+     Xi_b0      ]cc " ,
    ] ,
                                 ## combination cut : accept all
                                 CombinationCut = " AALL " ,
                                 ##      mother cut : accept all
                                 MotherCut      = "  ALL " , 
                                 ReFitPVs       = True     )

## get prompt charm hadrons 
import StrippingSelections.StrippingCharm.StrippingPromptCharm    as PC
pc      = PC.StrippingPromptCharmConf('PromptCharm', {
    'pT(D0)'           :  0.95 * GeV ,    ## pt-cut for  prompt   D0
    'pT(D+)'           :  0.95 * GeV ,    ## pt-cut for  prompt   D+
    'pT(Ds+)'          :  0.95 * GeV ,    ## pt-cut for  prompt   Ds+
    'pT(Lc+)'          :  0.95 * GeV ,    ## pt-cut for  prompt   Lc+
    } )

charm      = pc.PromptCharm()


# =============================================================================
## Combine beauty and charm hadrons 
# =============================================================================
from PhysConf.Selections import CombineSelection
BC         = CombineSelection  ( 'Hb&Hc' ,
                                 [ beauty  , charm ]  ,
                                 DecayDescriptors = [
    ##
    ##  B + C
    ## 
    '[ chi_b1(2P) -> B0        D0        ]cc' ,
    '[ chi_b1(2P) -> B0        D+        ]cc' ,
    '[ chi_b1(2P) -> B0        D*(2010)+ ]cc' ,
    '[ chi_b1(2P) -> B0        D_s+      ]cc' ,
    '[ chi_b1(2P) -> B0        Lambda_c+ ]cc' ,
    '[ chi_b1(2P) -> B0        Sigma_c0  ]cc' ,
    '[ chi_b1(2P) -> B0        Sigma_c++ ]cc' ,
    ##
    '[ chi_b1(2P) -> B+        D0        ]cc' ,
    '[ chi_b1(2P) -> B+        D+        ]cc' ,
    '[ chi_b1(2P) -> B+        D*(2010)+ ]cc' ,
    '[ chi_b1(2P) -> B+        D_s+      ]cc' ,
    '[ chi_b1(2P) -> B+        Lambda_c+ ]cc' ,
    '[ chi_b1(2P) -> B+        Sigma_c0  ]cc' ,
    '[ chi_b1(2P) -> B+        Sigma_c++ ]cc' ,
    ##
    '[ chi_b1(2P) -> B_s0      D0        ]cc' ,
    '[ chi_b1(2P) -> B_s0      D+        ]cc' ,
    '[ chi_b1(2P) -> B_s0      D*(2010)+ ]cc' ,
    '[ chi_b1(2P) -> B_s0      D_s+      ]cc' ,
    '[ chi_b1(2P) -> B_s0      Lambda_c+ ]cc' ,
    '[ chi_b1(2P) -> B_s0      Sigma_c0  ]cc' ,
    '[ chi_b1(2P) -> B_s0      Sigma_c++ ]cc' ,
    ##    
    '[ chi_b1(2P) -> Lambda_b0 D0        ]cc' ,
    '[ chi_b1(2P) -> Lambda_b0 D+        ]cc' ,
    '[ chi_b1(2P) -> Lambda_b0 D*(2010)+ ]cc' ,
    '[ chi_b1(2P) -> Lambda_b0 D_s+      ]cc' ,
    '[ chi_b1(2P) -> Lambda_b0 Lambda_c+ ]cc' ,
    '[ chi_b1(2P) -> Lambda_b0 Sigma_c0  ]cc' ,
    '[ chi_b1(2P) -> Lambda_b0 Sigma_c++ ]cc' ,    
    ##    
    '[ chi_b1(2P) -> Xi_b0     D0        ]cc' ,
    '[ chi_b1(2P) -> Xi_b0     D+        ]cc' ,
    '[ chi_b1(2P) -> Xi_b0     D*(2010)+ ]cc' ,
    '[ chi_b1(2P) -> Xi_b0     D_s+      ]cc' ,
    '[ chi_b1(2P) -> Xi_b0     Lambda_c+ ]cc' ,
    '[ chi_b1(2P) -> Xi_b0     Sigma_c0  ]cc' ,
    '[ chi_b1(2P) -> Xi_b0     Sigma_c++ ]cc' ,    
    ##    
    '[ chi_b1(2P) -> Xi_b-     D0        ]cc' ,
    '[ chi_b1(2P) -> Xi_b-     D+        ]cc' ,
    '[ chi_b1(2P) -> Xi_b-     D*(2010)+ ]cc' ,
    '[ chi_b1(2P) -> Xi_b-     D_s+      ]cc' ,
    '[ chi_b1(2P) -> Xi_b-     Lambda_c+ ]cc' ,
    '[ chi_b1(2P) -> Xi_b-     Sigma_c0  ]cc' ,
    '[ chi_b1(2P) -> Xi_b-     Sigma_c++ ]cc' ,    
    ##
    ##  B~ + C
    ## 
    '[ chi_b1(2P) -> B~0        D0        ]cc' ,
    '[ chi_b1(2P) -> B~0        D+        ]cc' ,
    '[ chi_b1(2P) -> B~0        D*(2010)+ ]cc' ,
    '[ chi_b1(2P) -> B~0        D_s+      ]cc' ,
    '[ chi_b1(2P) -> B~0        Lambda_c+ ]cc' ,
    '[ chi_b1(2P) -> B~0        Sigma_c0  ]cc' ,
    '[ chi_b1(2P) -> B~0        Sigma_c++ ]cc' ,
    ##
    '[ chi_b1(2P) -> B-         D0        ]cc' ,
    '[ chi_b1(2P) -> B-         D+        ]cc' ,
    '[ chi_b1(2P) -> B-         D*(2010)+ ]cc' ,
    '[ chi_b1(2P) -> B-         D_s+      ]cc' ,
    '[ chi_b1(2P) -> B-         Lambda_c+ ]cc' ,
    '[ chi_b1(2P) -> B-         Sigma_c0  ]cc' ,
    '[ chi_b1(2P) -> B-         Sigma_c++ ]cc' ,
    ##
    '[ chi_b1(2P) -> B_s~0      D0        ]cc' ,
    '[ chi_b1(2P) -> B_s~0      D+        ]cc' ,
    '[ chi_b1(2P) -> B_s~0      D*(2010)+ ]cc' ,
    '[ chi_b1(2P) -> B_s~0      D_s+      ]cc' ,
    '[ chi_b1(2P) -> B_s~0      Lambda_c+ ]cc' ,
    '[ chi_b1(2P) -> B_s~0      Sigma_c0  ]cc' ,
    '[ chi_b1(2P) -> B_s~0      Sigma_c++ ]cc' ,
    ##    
    '[ chi_b1(2P) -> Lambda_b~0 D0        ]cc' ,
    '[ chi_b1(2P) -> Lambda_b~0 D+        ]cc' ,
    '[ chi_b1(2P) -> Lambda_b~0 D*(2010)+ ]cc' ,
    '[ chi_b1(2P) -> Lambda_b~0 D_s+      ]cc' ,
    '[ chi_b1(2P) -> Lambda_b~0 Lambda_c+ ]cc' ,
    '[ chi_b1(2P) -> Lambda_b~0 Sigma_c0  ]cc' ,
    '[ chi_b1(2P) -> Lambda_b~0 Sigma_c++ ]cc' ,    
    ##    
    '[ chi_b1(2P) -> Xi_b0      D0        ]cc' ,
    '[ chi_b1(2P) -> Xi_b0      D+        ]cc' ,
    '[ chi_b1(2P) -> Xi_b0      D*(2010)+ ]cc' ,
    '[ chi_b1(2P) -> Xi_b0      D_s+      ]cc' ,
    '[ chi_b1(2P) -> Xi_b0      Lambda_c+ ]cc' ,
    '[ chi_b1(2P) -> Xi_b0      Sigma_c0  ]cc' ,
    '[ chi_b1(2P) -> Xi_b0      Sigma_c++ ]cc' ,    
    ##    
    '[ chi_b1(2P) -> Xi_b-      D0        ]cc' ,
    '[ chi_b1(2P) -> Xi_b-      D+        ]cc' ,
    '[ chi_b1(2P) -> Xi_b-      D*(2010)+ ]cc' ,
    '[ chi_b1(2P) -> Xi_b-      D_s+      ]cc' ,
    '[ chi_b1(2P) -> Xi_b-      Lambda_c+ ]cc' ,
    '[ chi_b1(2P) -> Xi_b-      Sigma_c0  ]cc' ,
    '[ chi_b1(2P) -> Xi_b-      Sigma_c++ ]cc' ,    
    ## 
    ] , 
                                 ## combination cut : accept all
                                 CombinationCut = " AALL " ,
                                 ##      mother cut : accept all
                                 MotherCut      = "  ALL " , 

                                 ReFitPVs       = True     )

from PhysSelPython.Wrappers import MultiSelectionSequence
from PhysSelPython.Wrappers import      SelectionSequence

# =============================================================================
## BANDD stream 
# =============================================================================
bandd = MultiSelectionSequence (
    "BBANDBD" ,
    Sequences = [ SelectionSequence ( 'BB' , BB ) ,
                  SelectionSequence ( 'BC' , BC ) ]
    )
# =============================================================================
## micro-DST writer
# =============================================================================
from DSTWriters.Configuration import ( SelDSTWriter            ,
                                       stripMicroDSTStreamConf ,
                                       stripMicroDSTElements   )

# Configuration of SelDSTWriter
SelDSTWriterConf     = { 'default' : stripMicroDSTStreamConf ( pack = False ) }
SelDSTWriterElements = { 'default' : stripMicroDSTElements   ( pack = False , refit = True ) }

uDstWriter = SelDSTWriter(
    "MyDSTWriter"                          ,
    StreamConf         =  SelDSTWriterConf      ,
    MicroDSTElements   =  SelDSTWriterElements  ,
    OutputFileSuffix   = 'BandQ'                ,  ## output PRE-fix! 
    SelectionSequences = [ bandd ] 
    )

    
from PhysConf.Filters import LoKi_Filters
fltrs   = LoKi_Filters (
    STRIP_Code = """
    HLT_PASS_RE('.*Lb2Lc.*Beauty2CharmLine'          ) |
    HLT_PASS_RE('.*Xib2Xic.*Beauty2CharmLine'        ) |
    HLT_PASS_RE('.*B2D0PiD2HH.*Beauty2CharmLine'     ) |
    HLT_PASS_RE('.*B2D0PiPiPiD2HH.*Beauty2CharmLine' ) |
    HLT_PASS_RE('.*B02DPiD2HH.*Beauty2CharmLine'     ) |
    HLT_PASS_RE('.*B02DPiPiPiD2HH.*Beauty2CharmLine' ) 
    """
    )

# ==============================================================================
# The last step: DaVinci & DB 
# ==============================================================================

from Configurables import DaVinci 
davinci = DaVinci ( 
    #
    ## DataType        = the_year ,                ## ATTENTION !!
    #
    ## EventPreFilters = fltrs.filters ('Filters') , 
    InputType       = "DST"    , 
    EvtMax          =    -1    ,  
    PrintFreq       =  1000    ,
    Lumi            =  True    ,  
    )

davinci.appendToMainSequence( [ uDstWriter.sequence() ] )


# =============================================================================
## suppress some  extensive printouts 
# =============================================================================
from Configurables import MessageSvc, PrintDuplicates,PrintDecayTreeTool
MessageSvc().Format = "% F%60W%S%7W%R%T %0W%M"
for s in ( 'BANDD' , ) :
    c = PrintDuplicates ( 'FindDuplicates_' + s , OutputLevel = 5 ) 
PrintDecayTreeTool ( 'PrintDuplicateDecays'     , OutputLevel = 4 ) 

    
# =============================================================================
if '__main__' == __name__ :
    
    print 100*'*'
    print __doc__
    print 100*'*'
    print ' Author  : %s ' % __author__
    print ' Version : %s ' % __version__ 
    print ' Date    : %s ' % __date__
    print 100*'*'


# =============================================================================
##                                                                      The END 
# =============================================================================


# =============================================================================
##                                                                      The END 
# =============================================================================
