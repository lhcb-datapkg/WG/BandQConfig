import sys, os
sys.path.append ( os.environ['BANDQSELECTIONSROOT'] )

from BandQSelections.JpsiPiPi import JpsiPiPi
JpsiPiPi() . configure ( {
    "jpsiTES"  : '/Event/Dimuon/Phys/FullDSTDiMuonJpsi2MuMuTOSLine/Particles' ,
    "pionTES"  : 'Phys/StdAllNoPIDsPions/Particles' , 
})


