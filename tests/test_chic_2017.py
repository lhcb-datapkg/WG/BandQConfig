#!/usr/bin/env  gaudirun.py 
from Gaudi.Configuration import importOptions

importOptions ('$BANDQCONFIGROOT/scripts/CHIC_mdst.py')
importOptions ('$BANDQCONFIGROOT/scripts/DataType-2017.py')
importOptions ('$BANDQCONFIGROOT/test_data/test_data_2017.py')

from Configurables import DaVinci
DaVinci( EvtMax = 10000 )

