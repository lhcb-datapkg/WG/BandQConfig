#!/usr/bin/env  gaudirun.py 
from Gaudi.Configuration import importOptions

importOptions ('$BANDQCONFIGROOT/scripts/DataType-2018-Turbo05.py')
importOptions ('$BANDQCONFIGROOT/scripts/CHIC_turbo.py')
importOptions ('$BANDQCONFIGROOT/test_data/test_data_2018_turbo.py')

from Configurables import DaVinci
DaVinci( EvtMax = 10000 )


