#!/usr/bin/env  gaudirun.py 
from Gaudi.Configuration import importOptions

importOptions ('$BANDQCONFIGROOT/scripts/DataType-2016-Turbo03a.py')
importOptions ('$BANDQCONFIGROOT/scripts/CHIC_turbo.py')
importOptions ('$BANDQCONFIGROOT/test_data/test_data_2016_turbo.py')

from Configurables import DaVinci
DaVinci( EvtMax = 10000 )


