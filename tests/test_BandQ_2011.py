#!/usr/bin/env  gaudirun.py 
from Gaudi.Configuration import importOptions

importOptions ('$BANDQCONFIGROOT/scripts/BandQ.py')
importOptions ('$BANDQCONFIGROOT/scripts/DataType-2011.py')
importOptions ('$BANDQCONFIGROOT/test_data/test_data_2011.py')

from Configurables import DaVinci
DaVinci( EvtMax = 1000 )

