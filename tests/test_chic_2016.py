#!/usr/bin/env  gaudirun.py 
from Gaudi.Configuration import importOptions

importOptions ('$BANDQCONFIGROOT/scripts/CHIC_mdst.py')
importOptions ('$BANDQCONFIGROOT/scripts/DataType-2016.py')
importOptions ('$BANDQCONFIGROOT/test_data/test_data_2016.py')

from Configurables import DaVinci
DaVinci( EvtMax = 10000 )

