#!/usr/bin/env  gaudirun.py 
from Gaudi.Configuration import importOptions

importOptions ('$BANDQCONFIGROOT/scripts/DataType-2017-Turbo04.py')
importOptions ('$BANDQCONFIGROOT/scripts/CHIC_turbo.py')
importOptions ('$BANDQCONFIGROOT/test_data/test_data_2017_turbo.py')

from Configurables import DaVinci
DaVinci( EvtMax = 10000 )
