#!/usr/bin/env  gaudirun.py 
from Gaudi.Configuration import importOptions

importOptions ('$BANDQCONFIGROOT/scripts/LambdabStar.py')
importOptions ('$BANDQCONFIGROOT/scripts/DataType-2018.py')
importOptions ('$BANDQCONFIGROOT/test_data/test_data_2018_BHADRONCOMPLETEEVENT.py')

from Configurables import DaVinci
DaVinci( EvtMax = 20000 )

