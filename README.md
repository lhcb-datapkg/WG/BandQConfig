## Scripts to run B&Q WG prodictions 

There are several [modules](doc/MODULES.md) that defines the lines: 

  - [WG_PSIX](doc/WG_PSIX.md) 
  - [WG_PSIX0](doc/WG_PSIX0.md)	   
  - [WG_Combinations](doc/WG_Combinations.md) 
  - [WG_UpsilonExotic](doc/WG_UpsilonExotic.md) 
  - [WG_CHIC](doc/WG_CHIC.md) 
  - [MultiCharm](doc/MultiCharm.md) 
  - [LambdaBstar](doc/LambdabStar.md) 
  - [LambdaBstar2](doc/LambdabStar2.md) 

These modules are combined into several [streams](doc/STREAMS.md):

  - [PSIX](doc/stream_PSIX.md)
  - [PSIX0](doc/stream_PSIX0.md)
  - [BOTTOM](doc/stream_BOTTOM.md)
  - [CHIC](doc/stream_CHIC.md)
  - [CHIC_TURBO](doc/stream_CHIC_TURBO.md)
  - [MULTICHARM](doc/stream_MULTICHARM.md)
  - [LAMBDABSTAR](doc/stream_LAMBDABSTAR.md)
  - [LAMBDABSTAR2](doc/stream_LAMBDABSTAR2.md)

